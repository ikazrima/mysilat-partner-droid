﻿using Newtonsoft.Json;
using System;

namespace MySilatPartner.JsonModel
{
    public class JurulatihApprovalModel
    {
        [JsonProperty("buah_id")]
        public string BuahId { get; set; }

        [JsonProperty("jurulatih_id")]
        public int JurulatihId { get; set; }

        [JsonProperty("received")]
        public int Received { get; set; }

        [JsonProperty("received_dt")]
        public DateTime? ReceivedDt { get; set; }

        [JsonProperty("verified_by")]
        public int VerifiedBy { get; set; }

        [JsonProperty("verified_dt")]
        public DateTime? VerifiedDt { get; set; }

        [JsonProperty("user_buah_id")]
        public int UserBuahId { get; set; }

        [JsonProperty("profile_id")]
        public string ProfileId { get; set; }

        [JsonProperty("user_alt_name")]
        public string UserAltName { get; set; }

        [JsonProperty("profile_img_id")]
        public string ProfileImgId { get; set; }

        [JsonProperty("profile_img_name")]
        public string ProfileImgName { get; set; }

        [JsonProperty("profile_img_uri")]
        public string ProfileImgUri { get; set; }

        [JsonProperty("facebook_id")]
        public string FacebookId { get; set; }

        [JsonProperty("peringkat_id")]
        public int PeringkatId { get; set; }

        [JsonProperty("peringkat")]
        public string Peringkat { get; set; }

        [JsonProperty("kelas_id")]
        public int KelasId { get; set; }

        [JsonProperty("kelas_code")]
        public string KelasCode { get; set; }
    }
}