﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class TauliahDetailedModel
    {
        [JsonProperty("profile_id")]
        public string profile_id { get; set; }

        [JsonProperty("identification")]
        public string identification { get; set; }

        [JsonProperty("sex")]
        public string sex { get; set; }

        [JsonProperty("first_name")]
        public string first_name { get; set; }

        [JsonProperty("last_name")]
        public string last_name { get; set; }

        [JsonProperty("alternate_name")]
        public string alternate_name { get; set; }

        [JsonProperty("tauliah_id")]
        public string tauliah_id { get; set; }

        [JsonProperty("code")]
        public string code { get; set; }

        [JsonProperty("jawatan")]
        public string jawatan { get; set; }

        [JsonProperty("facebook_id")]
        public string facebook_id { get; set; }

        [JsonProperty("kelas_id")]
        public string kelas_id { get; set; }

        [JsonProperty("kelas_code")]
        public string kelas_code { get; set; }

        [JsonProperty("kelas_name")]
        public string kelas_name { get; set; }

        [JsonProperty("year")]
        public string year { get; set; }
    }
}