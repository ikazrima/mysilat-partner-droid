﻿using Newtonsoft.Json;
using System;

namespace MySilatPartner.JsonModel
{
    class KelasLatihanModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("kelas_id")]
        public string KelasId { get; set; }

        [JsonProperty("kelas_code")]
        public string KelasCode { get; set; }

        [JsonProperty("kelas_name")]
        public string KelasName { get; set; }

        [JsonProperty("kelas_description")]
        public string KelasDescription { get; set; }

        [JsonProperty("days")]
        public string Days { get; set; }

        [JsonProperty("start_time")]
        public DateTime? StartTime { get; set; }

        [JsonProperty("end_time")]
        public DateTime? EndTime { get; set; }

        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }
    }
}