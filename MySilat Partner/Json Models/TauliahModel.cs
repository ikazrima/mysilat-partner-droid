﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class TauliahModel
    {
        [JsonProperty("tauliah_id")]
        public string TauliahId { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("jawatan")]
        public string Jawatan { get; set; }

        [JsonProperty("kelas_id")]
        public string KelasId { get; set; }

        [JsonProperty("kelas_code")]
        public string KelasCode { get; set; }

        [JsonProperty("kelas_name")]
        public string KelasName { get; set; }

        [JsonProperty("kelas_type")]
        public string KelasType { get; set; }

        [JsonProperty("year")]
        public string Year { get; set; }
    }
}