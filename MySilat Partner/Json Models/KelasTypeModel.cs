﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    class KelasTypeModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }
}