﻿using Newtonsoft.Json;
using System;

namespace MySilatPartner.JsonModel
{
    class KelasDetailsModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("state_id")]
        public int StateId { get; set; }

        [JsonProperty("zone_id")]
        public int ZoneId { get; set; }

        [JsonProperty("active")]
        public int Active { get; set; }

        [JsonProperty("created_dt")]
        public DateTime CreatedDt { get; set; }

        [JsonProperty("modified_dt")]
        public DateTime ModifiedDt { get; set; }
    }
}