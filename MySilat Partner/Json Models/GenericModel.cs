﻿namespace MySilatPartner.JsonModel
{
    public class GenericModel
    {
        public string Status { get; set; }
        public object Data { get; set; }
    }
}