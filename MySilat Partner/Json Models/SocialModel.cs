﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public partial class SocialModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("owner_id")]
        public string OwnerId { get; set; }

        [JsonProperty("owner_type")]
        public string OwnerType { get; set; }

        [JsonProperty("facebook_id")]
        public string FacebookId { get; set; }

        [JsonProperty("twitter_id")]
        public string TwitterId { get; set; }

        [JsonProperty("google_id")]
        public string GoogleId { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }

        [JsonProperty("email_alt")]
        public string EmailAlt { get; set; }

        [JsonProperty("created_dt")]
        public System.DateTimeOffset CreatedDt { get; set; }

        [JsonProperty("modified_dt")]
        public System.DateTimeOffset ModifiedDt { get; set; }

        [JsonProperty("active")]
        public string Active { get; set; }
    }
}