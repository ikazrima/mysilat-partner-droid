﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class UserProfileIC
    {
        [JsonProperty("identification")]
        public string Identification { get; set; }
    }
}