﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class UserBuahRootModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("peringkat_id")]
        public int PeringkatId { get; set; }

        [JsonProperty("buah_json")]
        public string BuahJson { get; set; }

        [JsonProperty("created_dt")]
        public System.DateTimeOffset CreatedDt { get; set; }

        [JsonProperty("modified_dt")]
        public System.DateTimeOffset ModifiedDt { get; set; }
    }
}