﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class BuahModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("buah_id")]
        public string BuahId { get; set; }

        [JsonProperty("pecahan_id")]
        public string PecahanId { get; set; }

        [JsonProperty("buah_name")]
        public string BuahName { get; set; }

        [JsonProperty("asas")]
        public int Asas { get; set; }

        [JsonProperty("jatuh")]
        public int Jatuh { get; set; }

        [JsonProperty("potong")]
        public int Potong { get; set; }

        [JsonProperty("tamat")]
        public int Tamat { get; set; }

        [JsonProperty("active")]
        public int Active { get; set; }
    }
}