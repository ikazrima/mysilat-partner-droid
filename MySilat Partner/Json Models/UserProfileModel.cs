﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class UserProfileModel
    {
        [JsonProperty("profile_id")]
        public string ProfileId { get; set; }

        [JsonProperty("sex")]
        public string Sex { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("alternate_name")]
        public string AlternateName { get; set; }

        [JsonProperty("peringkat_id")]
        public int PeringkatId { get; set; }

        [JsonProperty("peringkat")]
        public string Peringkat { get; set; }

        [JsonProperty("tauliah_id")]
        public string TauliahId { get; set; }

        [JsonProperty("jawatan_code")]
        public string JawatanCode { get; set; }

        [JsonProperty("jawatan")]
        public string Jawatan { get; set; }

        [JsonProperty("profile_img_id")]
        public string ProfileImgId { get; set; }

        [JsonProperty("profile_img_name")]
        public string ProfileImgName { get; set; }

        [JsonProperty("profile_img_uri")]
        public string ProfileImgUri { get; set; }

        [JsonProperty("facebook_id")]
        public string FacebookId { get; set; }

        [JsonProperty("year")]
        public int Year { get; set; }
    }
}