﻿using Newtonsoft.Json;
using System;

namespace MySilatPartner.JsonModel
{
    public partial class UserPersilatanModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }

        [JsonProperty("main_kelas_id")]
        public string MainKelasId { get; set; }

        [JsonProperty("peringkat_id")]
        public int PeringkatId { get; set; }

        [JsonProperty("ijazah_asas")]
        public DateTime IjazahAsas { get; set; }

        [JsonProperty("ijazah_potong")]
        public DateTime IjazahPotong { get; set; }

        [JsonProperty("ijazah_tamat")]
        public DateTime IjazahTamat { get; set; }

        [JsonProperty("srp")]
        public DateTime SRP { get; set; }

        [JsonProperty("smp")]
        public DateTime SMP { get; set; }

        [JsonProperty("created_dt")]
        public DateTime CreatedDt { get; set; }

        [JsonProperty("modified_dt")]
        public DateTime ModifiedDt { get; set; }
    }
}