﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    public class MyImageModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("owner_id")]
        public string OwnerId { get; set; }

        [JsonProperty("owner_type")]
        public string OwnerType { get; set; }

        [JsonProperty("img_name")]
        public string ImgName { get; set; }

        [JsonProperty("img_path")]
        public string ImgPath { get; set; }
    }
}