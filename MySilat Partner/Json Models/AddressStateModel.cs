﻿using Newtonsoft.Json;

namespace MySilatPartner.JsonModel
{
    class AddressStateModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("abbreviation")]
        public string Abbreviation { get; set; }
    }
}