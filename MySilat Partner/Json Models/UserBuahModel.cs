﻿using Newtonsoft.Json;
using System;

namespace MySilatPartner.JsonModel
{
    public class UserBuahModel
    {
        [JsonProperty("buah_id")]
        public string BuahId { get; set; }

        [JsonProperty("kelas_id")]
        public int KelasId { get; set; }

        [JsonProperty("kelas_code")]
        public string KelasCode { get; set; }

        [JsonProperty("jurulatih_id")]
        public int JurulatihId { get; set; }

        [JsonProperty("jurulatih_name")]
        public string JurulatihName { get; set; }

        [JsonProperty("received")]
        public int Received { get; set; }

        [JsonProperty("received_dt")]
        public DateTime? ReceivedDt { get; set; }

        [JsonProperty("verified_by")]
        public int VerifiedBy { get; set; }

        [JsonProperty("verified_dt")]
        public DateTime VerifiedDt { get; set; }
    }
}