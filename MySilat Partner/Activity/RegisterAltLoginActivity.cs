﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Telephony;
using Android.Text;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using Square.Picasso;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using v7App = Android.Support.V7.App;

namespace MySilatPartner.Activity
{
    [Activity(Label = "RegisterAltLoginActivity", Theme = "@style/MyTheme")]
    public class RegisterAltLoginActivity : v7App.AppCompatActivity
    {
        private FacebookLogin fbLogin = new FacebookLogin();
        private LoginButton btnFacebook;
        private ICallbackManager callbackManager;
        private FBProfileTracker profileTracker = new FBProfileTracker();

        int resizeDim = 256;
        ImageView imgProfile;
        ImageView imgProfileOverlay;

        ConstraintLayout root;

        TextInputLayout tilFB;
        TextInputEditText tieFB;

        private TelephonyManager tMgr;
        TextInputLayout tilPhoneNumber;
        TextInputEditText txtPhoneNumber;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.register_alt_login);

            root = (ConstraintLayout)FindViewById(Resource.Id.root);

            imgProfile = (ImageView)FindViewById(Resource.Id.profile_image);
            imgProfileOverlay = (ImageView)FindViewById(Resource.Id.profile_image_overlay);

            tilFB = (TextInputLayout)FindViewById(Resource.Id.til_fb_owner);
            tieFB = (TextInputEditText)FindViewById(Resource.Id.tie_fb_owner);

            tilPhoneNumber = (TextInputLayout)FindViewById(Resource.Id.til_phone_mobile);
            txtPhoneNumber = (TextInputEditText)FindViewById(Resource.Id.tie_phone_mobile);
            txtPhoneNumber.InputType = InputTypes.ClassPhone;
            txtPhoneNumber.TextChanged += TxtPhoneNumber_TextChanged;
            txtPhoneNumber.FocusChange += TxtPhoneNumber_FocusChange;

            #region Facebook
            btnFacebook = (LoginButton)FindViewById(Resource.Id.btn_fb);
            fbLogin.SetLoginButton(btnFacebook);
            fbLogin.Init(this);
            callbackManager = fbLogin.GetCallbackManager();

            profileTracker.Init(this);

            if (Profile.CurrentProfile == null)
                profileTracker.StartTracking();
            #endregion
        }


        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);

            AccessToken token = AccessToken.CurrentAccessToken;
            if (token != null && Profile.CurrentProfile != null)
            {
                tieFB.Text = Profile.CurrentProfile.Name;
                LoadProfileImage();
            }

            FillInPhone();
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (callbackManager.OnActivityResult(requestCode, (int)resultCode, data))
            {
                AccessToken token = AccessToken.CurrentAccessToken;
                if (token != null && Profile.CurrentProfile != null)
                {
                    tieFB.Text = Profile.CurrentProfile.Name;
                    LoadProfileImage();
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            switch (requestCode)
            {
                case MySilatCommon.REQUEST_CODE_READ_PHONE_STATE:
                    txtPhoneNumber.Text = tMgr.Line1Number;
                    break;
            }
        }

        public void FillInPhone()
        {
            tMgr = (TelephonyManager)GetSystemService(Context.TelephonyService);

            /// Needs to request permission to access phone state / sms for > 6.0
            #region Android > 6.0
            var permissionCheck = ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadPhoneState);

            if (permissionCheck != Android.Content.PM.Permission.Granted)
            {
                RequestPermissions(new string[] { Manifest.Permission.ReadPhoneState }, MySilatCommon.REQUEST_CODE_READ_PHONE_STATE);
                // AppCompatActivity.requestPermissions(this, new String[] { Manifest.permission.READ_PHONE_STATE }, REQUEST_READ_PHONE_STATE);
            }
            else
                txtPhoneNumber.Text = tMgr.Line1Number;
            #endregion
        }

        async void Register()
        {
            // Create socmed
            var finalResult = await MySilatSocial.AddSocialAsync(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);
            MySilatSocial.UserSocial = await MySilatSocial.GetSocialByOwnerAsync(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);

            // Update socmed - Facebook only
            finalResult = await MySilatSocial.UpdateFBAsync(MySilatSocial.UserSocial.Id, Profile.CurrentProfile.Id);
            genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);
            MySilatSocial.UserSocial = await MySilatSocial.GetSocialByOwnerAsync(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);

            // Create address book
            finalResult = await MySilatAddressBook.AddAddressBook(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);
            genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);
            MySilatAddressBook.UserAddressBook = await MySilatAddressBook.GetAddressBookByOwner(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);


            // Update address book - Mobile Phone Number
            // We're leaving Home Number empty here
            finalResult = await MySilatAddressBook.UpdatePhone(MySilatAddressBook.UserAddressBook.Id, txtPhoneNumber.Text, "");
            genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);
            MySilatAddressBook.UserAddressBook = await MySilatAddressBook.GetAddressBookByOwner(MySilatProfile.UserProfile.ProfileId, MySilatCommon.OWNER_USER);
        }

        private void TxtPhoneNumber_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidatePhone();
            }
        }

        private void TxtPhoneNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilPhoneNumber.ErrorEnabled = false;
        }

        private bool ValidatePhone()
        {
            if (txtPhoneNumber.Text.Length < 8)
            {
                tilPhoneNumber.ErrorEnabled = true;
                tilPhoneNumber.Error = "Minima 8 digit.";
                return false;
            }
            else if (txtPhoneNumber.Text.Length > 15)
            {
                tilPhoneNumber.ErrorEnabled = true;
                tilPhoneNumber.Error = "Maksima 15 digit.";
                return false;
            }

            tilPhoneNumber.ErrorEnabled = false;
            return true;
        }

        void LoadProfileImage()
        {
            Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(Profile.CurrentProfile.Id, MySilatCommon.IMAGE_SMALL);
            Picasso.With(imgProfile.Context).Load(uri).Resize(resizeDim, resizeDim).Into(imgProfile, HideImageOverlay, HideImageOverlay);
        }

        void HideImageOverlay()
        {
            imgProfileOverlay.Visibility = ViewStates.Gone;
        }
    }
}