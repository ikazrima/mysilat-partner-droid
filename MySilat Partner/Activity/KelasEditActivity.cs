﻿using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "KelasEditActivity", Theme = "@style/MyTheme")]
    class KelasEditActivity : v7App.AppCompatActivity
    {
        const string TAG = "KelasEditActivity";
        string activityType;

        #region Var
        bool init = false;
        bool loading = true;

        string kelasId;
        string kelasCode;
        string kelasName;
        int kelasType = 0;
        int kelasAddressState = 0;
        #endregion

        #region UI
        View rootView;

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        Button btnSave;

        List<int> kelasTypeId = new List<int>();
        List<string> kelasTypeName = new List<string>();
        KelasTypeList kelasTypeList;

        List<int> addressStateId = new List<int>();
        List<string> addressStateName = new List<string>();
        AddressStateList addressStateList;

        ArrayAdapter kelasTypeAdapter;
        ArrayAdapter addressStateAdapter;

        TextInputLayout tilKelasCode;
        TextInputEditText tieKelasCode;

        TextInputLayout tilKelasName;
        TextInputEditText tieKelasName;

        Spinner kelasTypeSpinner;
        Spinner addressStateSpinner;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.kelas_details_edit);

            rootView = FindViewById(Resource.Id.root);

            btnSave = (Button)FindViewById(Resource.Id.btn_save);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            kelasTypeSpinner = (Spinner)FindViewById(Resource.Id.spinner_kelas_type);
            addressStateSpinner = (Spinner)FindViewById(Resource.Id.spinner_kelas_negeri);

            tilKelasCode = (TextInputLayout)FindViewById(Resource.Id.til_kelas_code);
            tieKelasCode = (TextInputEditText)FindViewById(Resource.Id.tie_kelas_code);

            tilKelasName = (TextInputLayout)FindViewById(Resource.Id.til_kelas_name);
            tieKelasName = (TextInputEditText)FindViewById(Resource.Id.tie_kelas_name);

            GetIntent();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!loading)
                SaveEditAsync();
        }

        protected override async void OnResume()
        {
            base.OnResume();

            if (!init)
            {
                await InitKelasTypeSpinnerAsync();
                await InitAddressStateSpinnerAsync();

                init = true;
            }

            if (string.Compare(activityType, GetString(Resource.String.intent_new)) == 0)
            {
                tilKelasCode.Enabled = true;
            }

            else if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                tilKelasCode.Enabled = false;

                tieKelasCode.Text = kelasCode;
                tieKelasName.Text = kelasName;

                for (int i = 0; i < kelasTypeId.Count; i++)
                {
                    if (kelasTypeId[i] == kelasType)
                    {
                        kelasTypeSpinner.SetSelection(i);
                        break;
                    }
                }

                for (int i = 0; i < addressStateId.Count; i++)
                {
                    if (addressStateId[i] == kelasAddressState)
                    {
                        addressStateSpinner.SetSelection(i);
                        break;
                    }
                }
            }

            loading = false;
        }

        void GetIntent()
        {
            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            activityType = Intent.GetStringExtra(GetString(Resource.String.intent_activity_type));

            if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                kelasId = Intent.GetStringExtra("kelas_id");
                kelasCode = Intent.GetStringExtra("kelas_code");
                kelasName = Intent.GetStringExtra("kelas_name");
                int.TryParse(Intent.GetStringExtra("kelas_type"), out kelasType);
                kelasAddressState = Intent.GetIntExtra("address_state", 0);

                btnSave.Click += BtnSave_Click;
            }
        }

        async void SaveEditAsync()
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            kelasName = tieKelasName.Text;
            kelasType = kelasTypeId[kelasTypeSpinner.SelectedItemPosition];
            kelasAddressState = addressStateId[addressStateSpinner.SelectedItemPosition];

            snackbar.Show();

            var finalResult = await MySilatKelas.UpdateKelasDetails(kelasId, kelasName, kelasType, kelasAddressState);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "kelas_edit");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        async Task InitKelasTypeSpinnerAsync()
        {
            kelasTypeSpinner.Enabled = false;

            List<KelasTypeModel> result = await MySilatKelas.GetAllKelasType();

            kelasTypeList = new KelasTypeList();
            await kelasTypeList.GenerateAllKelasTypeAsync();

            foreach (var item in kelasTypeList.mKelasType)
            {
                int id;
                int.TryParse(item.Id, out id);

                kelasTypeId.Add(id);
                kelasTypeName.Add(item.Type);
            }

            kelasTypeAdapter = new ArrayAdapter<string>(this, Resource.Layout.my_spinner_left, kelasTypeName);
            kelasTypeAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item_left);
            kelasTypeSpinner.Adapter = kelasTypeAdapter;

            kelasTypeSpinner.Enabled = true;
        }

        async Task InitAddressStateSpinnerAsync()
        {
            addressStateSpinner.Enabled = false;

            List<AddressStateModel> result = await MySilatAddressBook.GetAllAddressStates();

            addressStateList = new AddressStateList();
            await addressStateList.GenerateAllAddressStateAsync();

            foreach (var item in addressStateList.mAddressState)
            {
                int id;
                int.TryParse(item.Id, out id);

                addressStateId.Add(id);
                addressStateName.Add(item.State);
            }

            addressStateAdapter = new ArrayAdapter<string>(this, Resource.Layout.my_spinner_left, addressStateName);
            addressStateAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item_left);
            addressStateSpinner.Adapter = addressStateAdapter;

            addressStateSpinner.Enabled = true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!loading)
            {
                switch (item.ItemId)
                {
                    case Android.Resource.Id.Home:
                        SetResult(Result.Canceled);
                        Finish();
                        return true;
                }
            }

            return base.OnOptionsItemSelected(item);
        }
    }
}