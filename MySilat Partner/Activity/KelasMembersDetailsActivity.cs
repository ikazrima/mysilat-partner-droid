﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.Constraints;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Charts;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "KelasMembersActivity", Theme = "@style/MyTheme")]
    [IntentFilter(new string[] { Intent.ActionSearch })]
    [MetaData("android.app.searchable", Resource = "@xml/searchable")]
    public class KelasMembersDetailsActivity : v7App.AppCompatActivity
    {
        const string TAG = "KelasMembersDetailsActivity";

        KelasMemberList kelasMembers;

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        ConstraintLayout rootView;
        TextView txtSubtitle;

        SfChart maleChart;
        SfChart femaleChart;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.kelas_members_details);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            rootView = (ConstraintLayout)FindViewById(Resource.Id.root);

            txtSubtitle = (TextView)FindViewById(Resource.Id.txt_sub_title);

            maleChart = (SfChart)rootView.FindViewById(Resource.Id.members_chart_male);
            femaleChart = (SfChart)rootView.FindViewById(Resource.Id.members_chart_female);

            txtSubtitle.Text = Intent.GetStringExtra("sub_title");

            kelasMembers = JsonConvert.DeserializeObject<KelasMemberList>(Intent.GetStringExtra("kelas_members"));

            string type = Intent.GetStringExtra("type");

            if(string.Compare(type, GetString(Resource.String.tp)) == 0)
            {
                PopulateTPChart();
            }
            else if (string.Compare(type, GetString(Resource.String.penuntut)) == 0)
            {
                PopulatePenuntutChart();
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        void PopulateTPChart()
        {
            ObservableCollection<TauliahData> tpMaleChartOC = new ObservableCollection<TauliahData>();
            ObservableCollection<TauliahData> tpFemaleChartOC = new ObservableCollection<TauliahData>();

            List<string> tauliahCode = new List<string> { "J", "PJ", "PJP" };

            List<TauliahData> tauliahData = new List<TauliahData>();
            for (int i = 0; i < tauliahCode.Count; i++)
            {
                tauliahData.Add(new TauliahData(tauliahCode[i], 0, 0));
            }

            KelasMemberList kelasTPListCopy = kelasMembers;
            for (int i = 0; i < kelasTPListCopy.Count; i++)
            {
                if (string.Compare(kelasTPListCopy[i].JawatanCode, "P") == 0
                    || string.Compare(kelasTPListCopy[i].JawatanCode, "PP") == 0)
                    kelasTPListCopy[i].JawatanCode = "J";
            }

            for (int i = 0; i < kelasTPListCopy.Count; i++)
            {
                for (int x = 0; x < tauliahData.Count; x++)
                {
                    if (string.Compare(kelasTPListCopy[i].JawatanCode, tauliahData[x].Tauliah) == 0)
                    {
                        switch (kelasTPListCopy[i].Sex.ToLower())
                        {
                            case "m":
                                tauliahData[x].Male++;
                                break;
                            case "f":
                                tauliahData[x].Female++;
                                break;
                        }
                    }
                }

            }

            foreach (var data in tauliahData)
            {
                if (data.Male > 0)
                    tpMaleChartOC.Add(data);
                if (data.Female > 0)
                    tpFemaleChartOC.Add(data);
            }

            if (tpMaleChartOC.Count > 0)
            {
                CategoryAxis malePrimaryAxis = new CategoryAxis();
                NumericalAxis maleSecondaryAxis = new NumericalAxis();
                maleChart.PrimaryAxis = malePrimaryAxis;
                maleChart.SecondaryAxis = maleSecondaryAxis;

                DoughnutSeries doughnutSeriesMale = new DoughnutSeries()
                {
                    ItemsSource = tpMaleChartOC,
                    XBindingPath = "Tauliah",
                    YBindingPath = "Male",
                    DoughnutCoefficient = 0.6f,
                    ColorModel = new ChartColorModel(ChartColorPalette.Metro)
                };
                doughnutSeriesMale.DataMarker.ShowLabel = true;
                doughnutSeriesMale.EnableAnimation = true;
                doughnutSeriesMale.AnimationDuration = 2;

                maleChart.Series.Clear();
                maleChart.Series.Add(doughnutSeriesMale);
                maleChart.Legend.LabelStyle.TextColor = Color.White;
                maleChart.Legend.Visibility = Visibility.Visible;
                maleChart.Legend.DockPosition = ChartDock.Bottom;
                maleChart.Title.SetTextColor(Color.White);
                maleChart.Title.Text = GetString(Resource.String.male) + " (" + tpMaleChartOC.Count + ")";
                maleChart.Title.TextSize = 12;

                maleChart.Visibility = ViewStates.Visible;

                for (int i = 0; i < maleChart.ChildCount; i++)
                {
                    maleChart.GetChildAt(i).Visibility = ViewStates.Visible;
                }

            }
            else
            {
                maleChart.Visibility = ViewStates.Gone;

                for (int i = 0; i < maleChart.ChildCount; i++)
                {
                    maleChart.GetChildAt(i).Visibility = ViewStates.Gone;
                }
            }

            if (tpFemaleChartOC.Count > 0)
            {
                CategoryAxis femalePrimaryAxis = new CategoryAxis();
                NumericalAxis femaleSecondaryAxis = new NumericalAxis();
                femaleChart.PrimaryAxis = femalePrimaryAxis;
                femaleChart.SecondaryAxis = femaleSecondaryAxis;

                DoughnutSeries doughnutSeriesFemale = new DoughnutSeries()
                {
                    ItemsSource = tpFemaleChartOC,
                    XBindingPath = "Tauliah",
                    YBindingPath = "Female",
                    DoughnutCoefficient = 0.6f,
                    ColorModel = new ChartColorModel(ChartColorPalette.Metro)
                };
                doughnutSeriesFemale.DataMarker.ShowLabel = true;
                doughnutSeriesFemale.EnableAnimation = true;
                doughnutSeriesFemale.AnimationDuration = 2;

                femaleChart.Series.Clear();
                femaleChart.Series.Add(doughnutSeriesFemale);
                femaleChart.Legend.LabelStyle.TextColor = Color.White;
                femaleChart.Legend.Visibility = Visibility.Visible;
                femaleChart.Legend.DockPosition = ChartDock.Bottom;
                femaleChart.Title.SetTextColor(Color.White);
                femaleChart.Title.Text = GetString(Resource.String.female) + " (" + tpFemaleChartOC.Count + ")";
                femaleChart.Title.TextSize = 12;

                femaleChart.Visibility = ViewStates.Visible;

                for (int i = 0; i < femaleChart.ChildCount; i++)
                {
                    femaleChart.GetChildAt(i).Visibility = ViewStates.Visible;
                }
            }
            else
            {
                femaleChart.Visibility = ViewStates.Gone;

                for (int i = 0; i < femaleChart.ChildCount; i++)
                {
                    femaleChart.GetChildAt(i).Visibility = ViewStates.Gone;
                }
            }
        }

        void PopulatePenuntutChart()
        {
            ObservableCollection<TauliahData> penuntutMaleChartOC = new ObservableCollection<TauliahData>();
            ObservableCollection<TauliahData> penuntutFemaleChartOC = new ObservableCollection<TauliahData>();

            List<string> peringkatCode = new List<string>{
                GetString(Resource.String.asas),
                GetString(Resource.String.jatuh),
                GetString(Resource.String.potong),
                GetString(Resource.String.tamat)
            };

            List<TauliahData> peringkatData = new List<TauliahData>();
            for (int i = 0; i < peringkatCode.Count; i++)
            {
                peringkatData.Add(new TauliahData(peringkatCode[i], 0, 0));
            }

            for (int i = 0; i < kelasMembers.Count; i++)
            {
                for (int x = 0; x < peringkatData.Count; x++)
                {
                    if (string.Compare(kelasMembers[i].Peringkat, peringkatData[x].Tauliah) == 0)
                    {
                        switch (kelasMembers[i].Sex.ToLower())
                        {
                            case "m":
                                peringkatData[x].Male++;
                                break;
                            case "f":
                                peringkatData[x].Female++;
                                break;
                        }
                    }
                }
            }

            foreach (var data in peringkatData)
            {
                if (data.Male > 0)
                    penuntutMaleChartOC.Add(data);
                if (data.Female > 0)
                    penuntutFemaleChartOC.Add(data);
            }

            if (penuntutMaleChartOC.Count > 0)
            {
                CategoryAxis malePrimaryAxis = new CategoryAxis();
                NumericalAxis maleSecondaryAxis = new NumericalAxis();
                maleChart.PrimaryAxis = malePrimaryAxis;
                maleChart.SecondaryAxis = maleSecondaryAxis;

                DoughnutSeries doughnutSeriesMale = new DoughnutSeries()
                {
                    ItemsSource = penuntutMaleChartOC,
                    XBindingPath = "Tauliah",
                    YBindingPath = "Male",
                    DoughnutCoefficient = 0.6f,
                    ColorModel = new ChartColorModel(ChartColorPalette.Metro)
                };
                doughnutSeriesMale.DataMarker.ShowLabel = true;
                doughnutSeriesMale.EnableAnimation = true;
                doughnutSeriesMale.AnimationDuration = 2;

                maleChart.Series.Clear();
                maleChart.Series.Add(doughnutSeriesMale);
                maleChart.Legend.LabelStyle.TextColor = Color.White;
                maleChart.Legend.Visibility = Visibility.Visible;
                maleChart.Legend.DockPosition = ChartDock.Bottom;
                maleChart.Title.SetTextColor(Color.White);
                maleChart.Title.Text = GetString(Resource.String.male) + " (" + penuntutMaleChartOC.Count + ")"; ;
                maleChart.Title.TextSize = 12;

                maleChart.Visibility = ViewStates.Visible;

                for (int i = 0; i < maleChart.ChildCount; i++)
                {
                    maleChart.GetChildAt(i).Visibility = ViewStates.Visible;
                }
            }
            else
            {
                maleChart.Visibility = ViewStates.Gone;

                for (int i = 0; i < maleChart.ChildCount; i++)
                {
                    maleChart.GetChildAt(i).Visibility = ViewStates.Gone;
                }
            }

            if (penuntutFemaleChartOC.Count > 0)
            {
                CategoryAxis femalePrimaryAxis = new CategoryAxis();
                NumericalAxis femaleSecondaryAxis = new NumericalAxis();
                femaleChart.PrimaryAxis = femalePrimaryAxis;
                femaleChart.SecondaryAxis = femaleSecondaryAxis;

                DoughnutSeries doughnutSeriesFemale = new DoughnutSeries()
                {
                    ItemsSource = penuntutFemaleChartOC,
                    XBindingPath = "Tauliah",
                    YBindingPath = "Female",
                    DoughnutCoefficient = 0.6f,
                    ColorModel = new ChartColorModel(ChartColorPalette.Metro)
                };
                doughnutSeriesFemale.DataMarker.ShowLabel = true;
                doughnutSeriesFemale.EnableAnimation = true;
                doughnutSeriesFemale.AnimationDuration = 2;

                femaleChart.Series.Clear();
                femaleChart.Series.Add(doughnutSeriesFemale);
                femaleChart.Legend.LabelStyle.TextColor = Color.White;
                femaleChart.Legend.Visibility = Visibility.Visible;
                femaleChart.Legend.DockPosition = ChartDock.Bottom;
                femaleChart.Title.SetTextColor(Color.White);
                femaleChart.Title.Text = GetString(Resource.String.female) + " (" + penuntutFemaleChartOC.Count + ")";
                femaleChart.Title.TextSize = 12;

                femaleChart.Visibility = ViewStates.Visible;

                for (int i = 0; i < femaleChart.ChildCount; i++)
                {
                    femaleChart.GetChildAt(i).Visibility = ViewStates.Visible;
                }
            }
            else
            {
                femaleChart.Visibility = ViewStates.Gone;

                for (int i = 0; i < femaleChart.ChildCount; i++)
                {
                    femaleChart.GetChildAt(i).Visibility = ViewStates.Gone;
                }
            }
        }
    }
}