﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Location;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Firebase;
using Firebase.Iid;
using Firebase.Storage;
using Java.Lang;
using MySilatPartner.API;
using MySilatPartner.Fragments;
using System;
using System.Threading.Tasks;
using Xamarin.Facebook.Login;
using v4App = Android.Support.V4.App;
using v4Widget = Android.Support.V4.Widget;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "@string/app_name", Theme = "@style/MyTheme")]
    public class MainActivity : v7App.AppCompatActivity
    {
        const string TAG = "MainActivity";

        #region UI
        public CoordinatorLayout lytCoordinator;

        public v7Widget.Toolbar toolbar;
        public v4Widget.DrawerLayout lytDrawer;
        public v7App.ActionBarDrawerToggle drawerToggle;
        public NavigationView nvDrawer;
        public TextView toolbarTitle;
        #endregion

        // Fused Location
        public FusedLocationProviderClient fusedLocationProviderClient;
        private LocationCallback locationCallback;
        private LocationRequest locationRequest;

        // Google Play Services
        private bool isGooglePlayServicesInstalled;
        private bool isRequestingLocationUpdates;

        // Permissions
        private const int WRITE_REQUEST_CODE = 500;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            InitControls();

            // Firebase
            FirebaseApp.InitializeApp(this);
            await FirebaseRegistration.RegisterFCMToken(MySilatProfile.UserProfile.ProfileId, FirebaseInstanceId.Instance.Token, FirebaseInstanceId.Instance.Id);

            /*
            if (Intent.Extras != null)
            {
                foreach (var key in Intent.Extras.KeySet())
                {
                    var value = Intent.Extras.GetString(key);
                    Log.Debug(TAG, "Key: {0} Value: {1}", key, value);
                }
            }*/

            // Fused Location
            //InitFusedLocation();

            // Start requesting location updates   
            // RequestLocationUpdate();

            // Write external storage
            // RequestWritePermission();

            // Go to Home Page
            try
            {
                v4App.Fragment fragment = null;
                Class fragmentClass = new HomeFragment().Class;

                fragment = (v4App.Fragment)fragmentClass.NewInstance();

                // Insert the fragment by replacing any existing fragment
                v4App.FragmentManager fragmentManager = SupportFragmentManager;
                FragmentManager.PopBackStack(null, PopBackStackFlags.Inclusive);
                fragmentManager.BeginTransaction().Replace(Resource.Id.coordinator, fragment).Commit();
            }
            catch (Java.Lang.Exception e)
            {
                e.PrintStackTrace();
            }
        }

        /*
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.toolbar_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }*/

        protected override async void OnResume()
        {
            base.OnResume();

            //RequestLocationUpdate(); 

        }

        protected override void OnPause()
        {
            base.OnPause();

            StopRequestionLocationUpdates();
            isRequestingLocationUpdates = false;
        }

        void InitControls()
        {
            lytCoordinator = (CoordinatorLayout)FindViewById(Resource.Id.coordinator);
            lytDrawer = (v4Widget.DrawerLayout)FindViewById(Resource.Id.drawer);
            nvDrawer = (NavigationView)FindViewById(Resource.Id.nav_view);
            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);

            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            SetupDrawerContent(nvDrawer);
        }

        /*
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            switch(MySilatCommon.RC_WRITE_EXTERNAL_PERMISSION_CHECK)
            {
                case WRITE_REQUEST_CODE:
                    if (grantResults[0] == Permission.Granted)
                    {
                        //Permission granted
                        var snack = Snackbar.Make(lytCoordinator, "Location permission is available, getting lat/long.", Snackbar.LengthShort);
                        snack.Show();
                    }
                    else
                    {
                        //Permission Denied :(
                        //Disabling location functionality
                        var snack = Snackbar.Make(lytCoordinator, "Location permission is denied.", Snackbar.LengthShort);
                        snack.Show();
                    }
                    break;
            }
        }
        */

        // Navigation Drawer

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);

            if (inputManager != null && CurrentFocus != null)
                inputManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);

            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    lytDrawer.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void SetupDrawerContent(NavigationView navigationView)
        {
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
        }

        private void UncheckAllMenuItem(IMenu menu)
        {
            int size = menu.Size();

            for (int i = 0; i < size; i++)
            {
                IMenuItem item = menu.GetItem(i);

                if (item.HasSubMenu)
                {
                    // Un check sub menu items
                    UncheckAllMenuItem(item.SubMenu);
                }
                else
                {
                    item.SetChecked(false);
                }
            }
        }

        private void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            SelectDrawerItem(e.MenuItem);

            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);

            if (inputManager != null && CurrentFocus != null)
                inputManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
        }

        public void SelectDrawerItem(IMenuItem menuItem)
        {
            // Create a new fragment and specify the fragment to show based on nav item clicked
            v4App.Fragment fragment = null;
            Class fragmentClass;

            switch (menuItem.ItemId)
            {
                case Resource.Id.nav_home:
                    fragmentClass = new HomeFragment().Class;
                    toolbarTitle.Text = ApplicationInfo.LoadLabel(PackageManager);
                    break;

                case Resource.Id.nav_profile:
                    fragmentClass = new UserProfileFragment().Class;
                    toolbarTitle.Text = GetString(Resource.String.profile);

                    Intent.PutExtra(
                        GetString(Resource.String.intent_user_type),
                        GetString(Resource.String.intent_user));
                    break;

                case Resource.Id.nav_kelas:
                    fragmentClass = new KelasDetailsFragment().Class;
                    toolbarTitle.Text = GetString(Resource.String.kelas);

                    Intent.PutExtra(
                        GetString(Resource.String.intent_user_type),
                        GetString(Resource.String.intent_user));

                    break;

                case Resource.Id.nav_user_tauliah:
                    fragmentClass = new UserTauliahFragment().Class;
                    toolbarTitle.Text = GetString(Resource.String.tauliah);
                    break;

                case Resource.Id.nav_user_persilatan:
                    fragmentClass = new UserPersilatanFragment().Class;
                    toolbarTitle.Text = GetString(Resource.String.persilatan);
                    break;

                case Resource.Id.nav_silibus:
                    fragmentClass = new UserBuahHostFragment().Class;
                    toolbarTitle.Text = GetString(Resource.String.silibus);
                    break;

                case Resource.Id.nav_jurulatih_approval:
                    fragmentClass = new JurulatihApprovalFragment().Class;
                    toolbarTitle.Text = "Pengesahan Jurulatih";
                    break;

                case Resource.Id.nav_user_logout:
                    LoginManager.Instance.LogOut();
                    Intent intent = new Intent(this, typeof(LoginActivity));
                    intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
                    StartActivity(intent);
                    return;

                default:
                    fragmentClass = new HomeFragment().Class;
                    SupportActionBar.Title = ApplicationInfo.LoadLabel(PackageManager);
                    break;
            }


            try
            {
                fragment = (v4App.Fragment)fragmentClass.NewInstance();
            }
            catch (Java.Lang.Exception e)
            {
                e.PrintStackTrace();
            }

            // Insert the fragment by replacing any existing fragment
            v4App.FragmentManager fragmentManager = SupportFragmentManager;
            FragmentManager.PopBackStack(null, PopBackStackFlags.Inclusive);
            fragmentManager.BeginTransaction().Replace(Resource.Id.coordinator, fragment).Commit();

            // Highlight the selected item has been done by NavigationView
            UncheckAllMenuItem(nvDrawer.Menu);
            menuItem.SetChecked(true);
            // Set toolbar title
            //SupportActionBar.SetTitle(menuItem.ItemId);
            // Close the navigation drawer
            lytDrawer.CloseDrawers();

        }

        // Fused Location
        void InitFusedLocation()
        {
            fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(this);
            isGooglePlayServicesInstalled = GoogleServices.IsGooglePlayServicesInstalled(this);

            if (isGooglePlayServicesInstalled)
            {
                locationRequest = new LocationRequest()
                                  .SetPriority(LocationRequest.PriorityHighAccuracy)
                                  .SetInterval(MySilatCommon.TEN_SECONDS)
                                  .SetFastestInterval(MySilatCommon.FIVE_SECONDS);
                locationCallback = new FusedLocationProviderCallback(this);

                fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(this);
            }
            else
            {
                // If there is no Google Play Services installed, then this sample won't run.
                Snackbar.Make(lytCoordinator, Resource.String.missing_googleplayservices_terminating, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, delegate { FinishAndRemoveTask(); })
                        .Show();
            }
        }

        async void RequestLocationUpdate()
        {
            if (v4App.ActivityCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) == Permission.Granted)
            {
                if (isRequestingLocationUpdates == false)
                {
                    await StartRequestingLocationUpdates();
                    isRequestingLocationUpdates = true;
                }
            }
            else
            {
                RequestLocationPermission(MySilatCommon.RC_LAST_LOCATION_PERMISSION_CHECK);
            }
        }

        async void RequestLocationUpdatesButtonOnClick(object sender, EventArgs eventArgs)
        {
            // No need to request location updates if we're already doing so.
            if (isRequestingLocationUpdates)
            {
                StopRequestionLocationUpdates();
                isRequestingLocationUpdates = false;
            }
            else
            {
                if (v4App.ActivityCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) == Permission.Granted)
                {
                    await StartRequestingLocationUpdates();
                    isRequestingLocationUpdates = true;
                }
                else
                {
                    RequestLocationPermission(MySilatCommon.RC_LAST_LOCATION_PERMISSION_CHECK);
                }
            }
        }

        async void GetLastLocationButtonOnClick(object sender, EventArgs eventArgs)
        {
            if (v4App.ActivityCompat.CheckSelfPermission(this, Manifest.Permission.AccessFineLocation) == Permission.Granted)
            {
                await GetLastLocationFromDevice();
            }
            else
            {
                RequestLocationPermission(MySilatCommon.RC_LAST_LOCATION_PERMISSION_CHECK);
            }
        }

        async Task GetLastLocationFromDevice()
        {
            var location = await fusedLocationProviderClient.GetLastLocationAsync();
        }

        void RequestLocationPermission(int requestCode)
        {
            if (ShouldShowRequestPermissionRationale(Manifest.Permission.AccessFineLocation))
            {
                Snackbar.Make(lytCoordinator, Resource.String.permission_location_rationale, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok,
                                   delegate
                                   {
                                       RequestPermissions(new[] { Manifest.Permission.AccessFineLocation }, requestCode);
                                   })
                        .Show();
            }
            else
            {
                RequestPermissions(new[] { Manifest.Permission.AccessFineLocation }, requestCode);
            }
        }

        void RequestWritePermission()
        {
            int requestCode = MySilatCommon.RC_WRITE_EXTERNAL_PERMISSION_CHECK;

            if (v4App.ActivityCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) == Permission.Denied)
            {
                if (ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                {
                    Snackbar.Make(lytCoordinator, Resource.String.permission_location_rationale, Snackbar.LengthIndefinite)
                            .SetAction(Resource.String.ok,
                                       delegate
                                       {
                                           RequestPermissions(new[] { Manifest.Permission.WriteExternalStorage }, requestCode);
                                       })
                            .Show();
                }
                else
                {
                    RequestPermissions(new[] { Manifest.Permission.WriteExternalStorage }, requestCode);
                }
            }
        }

        async Task StartRequestingLocationUpdates()
        {
            await fusedLocationProviderClient.RequestLocationUpdatesAsync(locationRequest, locationCallback);
        }

        async void StopRequestionLocationUpdates()
        {
            if (isRequestingLocationUpdates)
            {
                await fusedLocationProviderClient.RemoveLocationUpdatesAsync(locationCallback);
            }
        }

        ///
        /// CRASHES
        ///
        // Clear TextInputLayout focus in activty and its fragments
        /*
        public override bool DispatchTouchEvent(MotionEvent ev)
        {
            if (ev.Action == MotionEventActions.Down)
            {
                View v = CurrentFocus;

                if (v.GetType() == typeof(EditText))
                {
                    Rect outRect = new Rect();
                    v.GetGlobalVisibleRect(outRect);

                    if (!outRect.Contains((int)ev.RawX, (int)ev.RawY))
                    {
                        v.ClearFocus();
                        InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                        imm.HideSoftInputFromWindow(v.WindowToken, 0);
                    }
                }
            }

            return DispatchTouchEvent(ev);
        }*/
    }
}
