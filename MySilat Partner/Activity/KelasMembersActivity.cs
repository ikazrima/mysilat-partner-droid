﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.Constraints;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "KelasMembersActivity", Theme = "@style/MyTheme")]
    [IntentFilter(new string[] { Intent.ActionSearch })]
    [MetaData("android.app.searchable", Resource = "@xml/searchable")]
    public class KelasMembersActivity : v7App.AppCompatActivity, v7Widget.SearchView.IOnQueryTextListener, v7Widget.SearchView.IOnFocusChangeListener
    {
        const string TAG = "KelasMembersActivity";

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        ConstraintLayout contentRootView;

        KelasMemberList kelasMembers;
        KelasMemberRecyclerAdapter kelasMembersAdapter;
        v7Widget.GridLayoutManager kelasMembersLytMgr;
        v7Widget.RecyclerView kelasMembersRecycler;

        TextView txtTitle;
        TextView txtSubtitle;

        ViewTreeObserver vto;
        bool vtoRunOnce = false;

        v7Widget.SearchView searchView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.recycler_generic);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            contentRootView = (ConstraintLayout)FindViewById(Resource.Id.root);

            txtTitle = (TextView)FindViewById(Resource.Id.txt_title);
            txtSubtitle = (TextView)FindViewById(Resource.Id.txt_sub_title);

            txtTitle.Text = Intent.GetStringExtra("title");
            txtSubtitle.Text = Intent.GetStringExtra("sub_title");

            kelasMembersRecycler = (v7Widget.RecyclerView)FindViewById(Resource.Id.content_recycler);
            kelasMembers = JsonConvert.DeserializeObject<KelasMemberList>(Intent.GetStringExtra("kelas_members"));
            kelasMembersAdapter = new KelasMemberRecyclerAdapter(kelasMembers);

            Dictionary<int, string> tmpUri = new Dictionary<int, string>();
            tmpUri = JsonConvert.DeserializeObject<Dictionary<int, string>>(Intent.GetStringExtra("img_uri"));

            foreach (var item in tmpUri)
            {
                Android.Net.Uri path = Android.Net.Uri.Parse(item.Value);
                kelasMembersAdapter.imgUri.Add(item.Key, path);
            }

            kelasMembersAdapter.ItemClick += KelasMembersAdapter_ItemClick;

            kelasMembersLytMgr = new v7Widget.GridLayoutManager(this, 1, v7Widget.GridLayoutManager.Horizontal, false);
            kelasMembersRecycler.SetLayoutManager(kelasMembersLytMgr);
            kelasMembersRecycler.SetAdapter(kelasMembersAdapter);

            vto = contentRootView.ViewTreeObserver;
            vto.GlobalLayout += vto_GlobalLayout;

            searchView = (v7Widget.SearchView)FindViewById(Resource.Id.search);
            searchView.SetOnQueryTextListener(this);
            searchView.SetOnQueryTextFocusChangeListener(this);

            v7Widget.SearchView.SearchAutoComplete searchAutoComplete = (v7Widget.SearchView.SearchAutoComplete)searchView.FindViewById(Resource.Id.search_src_text);
            searchAutoComplete.SetHintTextColor(Color.White);
            searchAutoComplete.SetTextColor(Color.White);
        }

        private void vto_GlobalLayout(object sender, EventArgs e)
        {
            ViewTreeObserver vto = (ViewTreeObserver)sender;
            if (vto.IsAlive)
                vto.GlobalLayout -= vto_GlobalLayout; //even after removing it seems to continue to fire...

            if (!vtoRunOnce)
            {
                vtoRunOnce = true;

                float widthDp = kelasMembersRecycler.FindViewHolderForAdapterPosition(0).ItemView.Width / Resources.DisplayMetrics.Density;
                int numberOfColumns = MySilatCommon.CalculateNoOfColumns(this, widthDp);

                kelasMembersLytMgr = new v7Widget.GridLayoutManager(this, numberOfColumns, v7Widget.GridLayoutManager.Vertical, false);
                kelasMembersRecycler.SetLayoutManager(kelasMembersLytMgr);
            }
        }

        private void KelasMembersAdapter_ItemClick(object sender, int e)
        {
            // throw new NotImplementedException();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public bool OnQueryTextChange(string newText)
        {
            kelasMembersAdapter.Filter(newText);

            if (string.IsNullOrWhiteSpace(newText))
                txtSubtitle.Text = kelasMembersAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower();
            else
                txtSubtitle.Text = kelasMembersAdapter.ItemCount + " " + GetString(Resource.String.search_result).ToLower();

            return true;
        }

        public bool OnQueryTextSubmit(string query)
        {
            kelasMembersAdapter.Filter(query);

            if (string.IsNullOrWhiteSpace(query))
                txtSubtitle.Text = kelasMembersAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower();
            else
                txtSubtitle.Text = kelasMembersAdapter.ItemCount + " " + GetString(Resource.String.search_result).ToLower();

            return true;
        }

        public void OnFocusChange(View v, bool hasFocus)
        {
            if (v == searchView)
            {
                if (!v.HasFocus)
                    toolbarTitle.Visibility = ViewStates.Visible;
                else
                    toolbarTitle.Visibility = ViewStates.Gone;
            }
        }
    }
}