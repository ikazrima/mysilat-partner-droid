﻿using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Theartofdev.Edmodo.Cropper;
// using Firebase.Firestore;
using Firebase.Storage;
using Java.Lang;
using Java.Util;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using Square.Picasso;
using System;
using System.Collections.Generic;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "UserICEditActivity", Theme = "@style/MyTheme")]
    public class UserICEditActivity : v7App.AppCompatActivity, IOnSuccessListener, IOnFailureListener, IOnProgressListener
    {
        const string TAG = "UserICEditActivity";
        string activityType;

        #region Var
        string profileId;
        #endregion

        #region Image
        int resizeDim = 512;
        string uploadFileName;
        Android.Gms.Tasks.Task downloadTask;
        Android.Gms.Tasks.Task uploadTask;
        Android.Gms.Tasks.Task storeTask;
        Android.Net.Uri resultUri;
        StorageReference imageRef;
        #endregion

        #region UI
        View rootView { get; set; }

        v7Widget.Toolbar toolbar { get; set; }
        TextView toolbarTitle { get; set; }

        TextInputLayout tilIC { get; set; }
        TextInputEditText tieIC { get; set; }

        ImageButton btnCamera { get; set; }
        ImageView imgIC { get; set; }
        ProgressBar progressBar { get; set; }

        ConstraintLayout lytIC;
        ConstraintLayout lytSubmit;
        Button btnSave { get; set; }

        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.user_ic_edit);

            rootView = FindViewById(Resource.Id.root);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            tilIC = (TextInputLayout)FindViewById(Resource.Id.til_ic_number);
            tieIC = (TextInputEditText)FindViewById(Resource.Id.tie_ic_number);

            imgIC = (ImageView)FindViewById(Resource.Id.ic_image);

            btnCamera = (ImageButton)FindViewById(Resource.Id.btn_camera);

            lytIC = (ConstraintLayout)FindViewById(Resource.Id.lyt_ic_image);

            lytSubmit = (ConstraintLayout)FindViewById(Resource.Id.lyt_submit);
            btnSave = (Button)FindViewById(Resource.Id.btn_save);

            progressBar = (ProgressBar)FindViewById(Resource.Id.progressBar);

            btnCamera.Click += BtnCamera_Click;
            btnSave.Click += BtnSave_Click;

            lytSubmit.Visibility = ViewStates.Gone;
        }

        protected override void OnResume()
        {
            base.OnResume();

            GetIntent();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    SetResult(Result.Canceled);
                    Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            CropImage.ActivityResult result = CropImage.GetActivityResult(data);

            if (requestCode == CropImage.CropImageActivityRequestCode)
            {
                if (resultCode == Result.Ok)
                {
                    resultUri = result.Uri;
                    Picasso.With(this).Load(resultUri);
                    try
                    {
                        var heightDip = TypedValue.ApplyDimension(ComplexUnitType.Dip, 200, Resources.DisplayMetrics);
                        lytIC.SetMinimumHeight((int)heightDip);

                        Picasso.With(this).Load(resultUri).Resize((int)(resizeDim * 1.5f), resizeDim)
                            .Into(imgIC, PicassoComplete, PicassoComplete);
                    }
                    catch (System.Exception e)
                    {
                        NetworkManager.IsOnline(this, true, ToastLength.Long);
                        Console.WriteLine(e);
                    }
                    finally
                    {
                        downloadTask = null;
                    }
                }
            }
            else if (requestCode == CropImage.CropImageActivityResultErrorCode)
            {
                System.Exception error = result.Error;
                btnCamera.Visibility = ViewStates.Visible;
            }
        }

        void IOnProgressListener.snapshot(Java.Lang.Object p0)
        {
            if (uploadTask != null)
            {
                var taskSnapshot = (UploadTask.TaskSnapshot)p0;
                double progress = (100.0 * taskSnapshot.BytesTransferred / taskSnapshot.TotalByteCount);
                progressBar.Progress = (int)progress;
            }
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            if (downloadTask != null)
            {
                if (downloadTask.IsSuccessful)
                {
                    try
                    {
                        Android.Net.Uri uri = (Android.Net.Uri)downloadTask.Result;
                        Picasso.With(imgIC.Context).Load(uri).Resize((int)(resizeDim * 1.5f), resizeDim)
                            .Into(imgIC, PicassoComplete, PicassoComplete);
                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine(e);
                        NetworkManager.IsOnline(this, true, ToastLength.Long);
                    }
                    finally
                    {
                        downloadTask = null;
                    }
                }
            }

            if (uploadTask != null)
            {
                if (uploadTask.IsSuccessful)
                {
                    var taskSnapshot = (UploadTask.TaskSnapshot)result;

                    string path = taskSnapshot.Storage.Path;

                    // SubmitFirestore(profileId, path);

                    /*var finalResult = await MySilatProfile.AddProfileImageAsync(
                        profileId,
                        uploadFileName,
                        taskSnapshot.DownloadUrl.ToString());*/


                    /*
                    GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

                    if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
                    {
                        /// TODO
                    }
                    else
                    {
                        Snackbar snackbar = Snackbar.Make(rootView, Resource.String.server_error, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, v => { });
                    }*/

                    progressBar.Visibility = ViewStates.Gone;
                    btnCamera.Visibility = ViewStates.Visible;

                    uploadTask = null;
                }
            }

            if (storeTask != null)
            {
                if (storeTask.IsSuccessful)
                {
                    // var res = (DocumentSnapshot)result;
                    storeTask = null;
                    Console.WriteLine("Doc has been saved.");
                }
            }
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            if (downloadTask != null)
            {
                if (!downloadTask.IsSuccessful)
                {
                    downloadTask = null;
                    NetworkManager.IsOnline(this, true, ToastLength.Long);
                }
            }

            if (uploadTask != null)
            {
                if (!uploadTask.IsSuccessful)
                {
                    progressBar.Visibility = ViewStates.Gone;
                    btnCamera.Visibility = ViewStates.Visible;

                    lytSubmit.Visibility = ViewStates.Gone;

                    Snackbar snackbar = Snackbar.Make(this.rootView, Resource.String.saving_error, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, v => { });

                    lytSubmit.Visibility = ViewStates.Gone;

                    uploadTask = null;

                    NetworkManager.IsOnline(this, true, ToastLength.Long);
                }
            }
        }

        async void GetIntent()
        {
            try
            {
                activityType = Intent.GetStringExtra(GetString(Resource.String.intent_activity_type));

                if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
                {
                    profileId = Intent.GetStringExtra("user_id");

                    UserProfileIC profile = await MySilatProfile.GetProfileICAsync(profileId);

                    if (profile != null)
                        tieIC.Text = profile.Identification;
                    else
                        NetworkManager.IsOnline(this, true, ToastLength.Long);

                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.ToString());
                NetworkManager.IsOnline(this, true, ToastLength.Long);
            }
        }

        void UploadICImage()
        {
            progressBar.Visibility = ViewStates.Visible;
            btnCamera.Visibility = ViewStates.Gone;
            lytSubmit.Visibility = ViewStates.Gone;

            uploadFileName = Guid.NewGuid().ToString() + "-" + resultUri.LastPathSegment;

            FirebaseMyStorage.Init();
            imageRef = FirebaseMyStorage.storageRef.Child("profile/" + profileId + "/identification/" + uploadFileName);

            uploadTask = imageRef.PutFile(resultUri)
                .AddOnProgressListener(this)
                .AddOnSuccessListener(this)
                .AddOnFailureListener(this);
        }

        void SubmitFirestore(string _profileId, string _uri)
        {
            /*
            const string profileKey = "PROFILE_KEY";
            const string imgUriKey = "IMG_URI_KEY";

            Dictionary<string, object> dataToSave = new Dictionary<string, object>();
            HashMap hashMap = new HashMap(dataToSave);

            dataToSave.Add(profileKey, _profileId);
            dataToSave.Add(imgUriKey, _uri);

            DocumentReference mDocRef = FirebaseFirestore.Instance.Document("support/profile");
            storeTask = mDocRef.Set(hashMap)
                .AddOnSuccessListener(this)
                .AddOnFailureListener(this);*/

        }

        void PicassoComplete()
        {
            progressBar.Visibility = ViewStates.Gone;
            btnCamera.Visibility = ViewStates.Visible;
            lytSubmit.Visibility = ViewStates.Visible;
        }

        private void BtnCamera_Click(object sender, EventArgs e)
        {
            // for fragment (DO NOT use `getActivity()`)
            CropImage.Builder()
                .SetGuidelines(CropImageView.Guidelines.On)
                .SetAspectRatio(3, 2)
                .SetMinCropResultSize(resizeDim, resizeDim)
                .SetFixAspectRatio(true)
                .Start(this);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            UploadICImage();
        }
    }
}