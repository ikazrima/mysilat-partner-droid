﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "ContactEditActivity", Theme = "@style/MyTheme")]
    public class ContactEditActivity : v7App.AppCompatActivity
    {
        const string TAG = "ContactEditActivity";
        string activityType;

        #region Var
        bool loading = true;

        string id;
        string contactMobile;
        string contactFixed;
        #endregion

        #region UI
        View rootView;

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        Button btnSave;

        TextInputLayout tilFixed;
        TextInputEditText tieFixed;

        TextInputLayout tilMobile;
        TextInputEditText tieMobile;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.address_phone_edit);

            rootView = FindViewById(Resource.Id.root);

            btnSave = (Button)FindViewById(Resource.Id.btn_save);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            tilFixed = (TextInputLayout)FindViewById(Resource.Id.til_fixed);
            tieFixed = (TextInputEditText)FindViewById(Resource.Id.tie_fixed);

            tilMobile = (TextInputLayout)FindViewById(Resource.Id.til_mobile);
            tieMobile = (TextInputEditText)FindViewById(Resource.Id.tie_mobile);

            GetIntent();
        }

        protected override void OnResume()
        {
            base.OnResume();

            loading = true;

            if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                tieFixed.Text = contactFixed;
                tieMobile.Text = contactMobile;
            }

            loading = false;
        }

        void GetIntent()
        {
            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            activityType = Intent.GetStringExtra(GetString(Resource.String.intent_activity_type));

            if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                    id = Intent.GetStringExtra("id");
                    contactFixed = Intent.GetStringExtra("fixed");
                    contactMobile = Intent.GetStringExtra("mobile");

                    btnSave.Click += BtnSave_Click; ;
            }
        }

        async void SaveEditAsync()
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            contactFixed = tieFixed.Text;
            contactMobile = tieMobile.Text;

            snackbar.Show();

            var finalResult = await MySilatAddressBook.UpdatePhone(id, contactFixed, contactMobile);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "contact_edit");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!loading)
            {
                switch (item.ItemId)
                {
                    case Android.Resource.Id.Home:
                        SetResult(Result.Canceled);
                        Finish();
                        return true;
                }
            }

            return base.OnOptionsItemSelected(item);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!loading)
                SaveEditAsync();
        }
    }
}