﻿using Android.Accounts;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Sfbusyindicator;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using AlertDialog = Android.Support.V7.App.AlertDialog;

namespace MySilatPartner.Activity
{
    [Activity(Label = "MySilat Partner", MainLauncher = true, Theme = "@style/MyTheme")]
    public class LoginActivity : AppCompatActivity, IDisposable
    {
        private FacebookLogin fbLogin = new FacebookLogin();
        private LoginButton btnFacebook;
        private ICallbackManager callbackManager;
        private FBProfileTracker profileTracker = new FBProfileTracker();

        #region UI var
        Button btnLogin;
        TextInputLayout tilEmailLogin;
        TextInputLayout tilPasswordLogin;
        TextInputEditText tieEmailLogin;
        TextInputEditText tiePasswordLogin;

        private LinearLayout lytMain;
        private CardView loginCard;
        private LinearLayout lytRegister;
        private TextView txtRegister;
        private SfBusyIndicator sfBusyIndicator;

        AlertDialog registerDialog;
        TextInputLayout tilEmailReg;
        TextInputLayout tilPasswordReg;
        TextInputLayout tilPasswordReg2;
        TextInputLayout tilICNumber;
        TextInputLayout tilFirstName;
        TextInputLayout tilLastName;
        TextInputLayout tilAltName;

        TextInputEditText tieEmailReg;
        TextInputEditText tiePasswordReg;
        TextInputEditText tiePasswordReg2;
        TextInputEditText tieICNumber;
        TextInputEditText tieFirstName;
        TextInputEditText tieLastName;
        TextInputEditText tieAltName;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Login);

            lytMain = (LinearLayout)FindViewById(Resource.Id.lyt_main);

            loginCard = (CardView)FindViewById(Resource.Id.card_login);
            lytRegister = (LinearLayout)FindViewById(Resource.Id.lyt_register);

            txtRegister = (TextView)FindViewById(Resource.Id.txt_register);
            txtRegister.Click += TxtRegister_Click;

            sfBusyIndicator = (SfBusyIndicator)FindViewById(Resource.Id.busyIndicator);
            sfBusyIndicator.SetBackgroundColor(Color.Transparent);
            sfBusyIndicator.TextColor = new Color(ContextCompat.GetColor(this, Resource.Color.Accent));

            btnFacebook = (LoginButton)FindViewById(Resource.Id.btn_fb);
            fbLogin.SetLoginButton(btnFacebook);
            fbLogin.Init(this);
            callbackManager = fbLogin.GetCallbackManager();

            btnLogin = (Button)FindViewById(Resource.Id.btn_login);
            tilEmailLogin = (TextInputLayout)FindViewById(Resource.Id.til_email);
            tilPasswordLogin = (TextInputLayout)FindViewById(Resource.Id.til_password);
            tieEmailLogin = (TextInputEditText)FindViewById(Resource.Id.tie_email);
            tiePasswordLogin = (TextInputEditText)FindViewById(Resource.Id.tie_password);

            btnLogin.Click += BtnLogin_Click;
            tieEmailLogin.TextChanged += TieEmailLogin_TextChanged;

            profileTracker.Init(this);

            if (Profile.CurrentProfile == null)
                profileTracker.StartTracking();
        }

        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);

            AccessToken token = AccessToken.CurrentAccessToken;

            if (token != null && Profile.CurrentProfile != null)
            {
                LoginByFacebook();
            }
            else
            {
                loginCard.Visibility = ViewStates.Visible;
                lytRegister.Visibility = ViewStates.Visible;
                sfBusyIndicator.Visibility = ViewStates.Gone;
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (callbackManager.OnActivityResult(requestCode, (int)resultCode, data))
            {
                AccessToken token = AccessToken.CurrentAccessToken;
                if (token != null && Profile.CurrentProfile != null)
                {
                    LoginByFacebook();
                }
            }
        }

        void StartMainActivity()
        {
            // Proceed to main screen
            Intent intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
            StartActivity(intent);
        }

        async void LoginAttemptByIdentification(bool auth)
        {
            if (auth)
            {
                MySilatProfile.UserProfile = await MySilatProfile.GetProfileByIdentificationAsync(tieEmailLogin.Text);

                if (MySilatProfile.UserProfile == null)
                {
                    LoginError();
                }
                else
                {
                    StartMainActivity();
                }
            }
            else
            {
                LoginError();
            }
        }

        async void LoginAttemptByEmail(bool auth)
        {
            if (auth)
            {
                MySilatProfile.UserProfile = await MySilatProfile.GetProfileByEmailAsync(tieEmailLogin.Text);

                if (MySilatProfile.UserProfile == null)
                {
                    LoginError();
                }
                else
                {
                    StartMainActivity();
                }
            }
            else
            {
                LoginError();
            }
        }

        public async void LoginByEmail()
        {
            try
            {
                bool auth = await MySilatAccount.LoginByEmail(tieEmailLogin.Text, tiePasswordLogin.Text);

                LoginAttemptByEmail(auth);
            }
            catch (Exception e)
            {
                LoginException(e);
            }
        }

        public async void LoginByIdentification()
        {
            try
            {
                bool auth = await MySilatAccount.LoginByIdentification(tieEmailLogin.Text, tiePasswordLogin.Text);

                LoginAttemptByIdentification(auth);
            }
            catch (Exception e)
            {
                LoginException(e);
            }
        }

        public async void LoginByFacebook()
        {
            try
            {
                sfBusyIndicator.Visibility = ViewStates.Visible;
                loginCard.Visibility = ViewStates.Invisible;
                lytRegister.Visibility = ViewStates.Invisible;

                MySilatProfile.UserProfile = await MySilatProfile.GetProfileByFBIDAsync(Profile.CurrentProfile.Id);

                if (MySilatProfile.UserProfile == null)
                {
                    fbLogin.Logout();

                    LoginError();

                    return;
                }
                else
                {
                    StartMainActivity();
                }
            }
            catch (Exception e)
            {
                LoginException(e);
            }
        }

        void LoginException(Exception e)
        {
            loginCard.Visibility = ViewStates.Visible;
            lytRegister.Visibility = ViewStates.Visible;
            sfBusyIndicator.Visibility = ViewStates.Gone;

            Console.WriteLine(e);
            NetworkManager.IsOnline(this, true, ToastLength.Long);
        }

        void LoginError()
        {
            loginCard.Visibility = ViewStates.Visible;
            lytRegister.Visibility = ViewStates.Visible;
            sfBusyIndicator.Visibility = ViewStates.Gone;

            Snackbar
                  .Make(lytMain, GetString(Resource.String.acc_not_registered), Snackbar.LengthLong)
                  .SetAction(GetString(Resource.String.ok), (view) => { })
                  .Show();
        }

        private void ShowRegisterDialog()
        {
            View dialogView = this.LayoutInflater.Inflate(Resource.Layout.register_card, null);

            AlertDialog.Builder tauliahBuilder = new AlertDialog.Builder(this);
            tauliahBuilder.SetView(dialogView);
            registerDialog = tauliahBuilder.Create();

            Button btnRegister = (Button)dialogView.FindViewById(Resource.Id.btn_register);
            btnRegister.Click += BtnRegister_Click;

            Button btnCancel = (Button)dialogView.FindViewById(Resource.Id.btn_cancel);
            btnCancel.Click += new EventHandler((s, args) =>
            {
                RemoveRegisterDialogListener();
                registerDialog.Dismiss();
            });

            tilEmailReg = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_email_reg);
            tilPasswordReg = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_password_reg);
            tilPasswordReg2 = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_password_reg2);

            tilICNumber = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_ic_number);
            tilFirstName = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_first_name);
            tilLastName = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_last_name);
            tilAltName = (TextInputLayout)dialogView.FindViewById(Resource.Id.til_alt_name);

            tieEmailReg = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_email_reg);
            tiePasswordReg = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_password_reg);
            tiePasswordReg2 = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_password_reg2);

            tieICNumber = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_ic_number);
            tieFirstName = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_first_name);
            tieLastName = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_last_name);
            tieAltName = (TextInputEditText)dialogView.FindViewById(Resource.Id.tie_alt_name);

            tieICNumber.InputType = InputTypes.ClassNumber;
            tieFirstName.InputType = InputTypes.TextVariationPersonName;
            tieLastName.InputType = InputTypes.TextVariationPersonName;
            tieAltName.InputType = InputTypes.TextVariationPersonName;

            AddRegisterDialogListener();

            registerDialog.Show();
            registerDialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            FillInRegisterForm();
        }

        private async void RegisterNewUserAsync()
        {
            sfBusyIndicator.Visibility = ViewStates.Visible;

            bool valid = true;

            if (ValidateEmailReg() == false
                || ValidatePasswordReg() == false
                || await ValidateICAsync() == false
                || ValidateFirstName() == false
                || ValidateLastName() == false
                || ValidateAltName() == false)
                valid = false;

            if (!valid)
            {
                Snackbar
                  .Make(lytMain, GetString(Resource.String.acc_not_registered), Snackbar.LengthLong)
                  .Show();
                return;
            }

            try
            {
                if (!NetworkManager.IsOnline(this, true, ToastLength.Long))
                    return;

                // Register user
                var finalResult = await MySilatAccount.RegisterUser(tieEmailReg.Text, tiePasswordReg.Text, tieICNumber.Text, tieFirstName.Text, tieLastName.Text, tieAltName.Text);
                GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);
                MySilatProfile.UserProfile = await MySilatProfile.GetProfileByIdentificationAsync(tieICNumber.Text);

                // Everything successful
                if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0
                    && MySilatProfile.UserProfile != null)
                {
                    // Proceed to next screen
                    Intent intent = new Intent(this, typeof(RegisterAltLoginActivity));
                    intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTask);
                    StartActivity(intent);
                }
                else
                {
                    RegisterError(GetString(Resource.String.oops));
                    return;
                }
            }
            catch (Exception ex)
            {
                RegisterError(ex.ToString());
                return;
            }
        }

        public void FillInRegisterForm()
        {
            AccountManager accMgr = AccountManager.Get(this);
            Account[] tabAccount = accMgr.GetAccountsByType("com.google");
            List<Account> accList = new List<Account>();

            foreach (Account act in tabAccount)
            {
                string name = act.Name;
                accList.Add(act);
            }

            if (accList.Count > 0)
            {
                tieEmailReg.Text = accList[0].Name;
                tieFirstName.Text = "";
                tieICNumber.Text = "";
                tieLastName.Text = "";
                tieAltName.Text = "";
            }
        }

        private bool ValidateEmailReg()
        {
            if (!MySilatCommon.IsValidEmailAddress(tieEmailReg.Text))
            {
                tilEmailReg.ErrorEnabled = true;
                tilEmailReg.Error = "E-mel tidak sah";
                return false;
            }
            else
            {
                tilEmailReg.ErrorEnabled = false;
                return true;
            }
        }

        private bool ValidatePasswordReg()
        {
            if (string.Compare(tiePasswordReg.Text, tiePasswordReg2.Text) == 0)
            {
                tilPasswordReg.ErrorEnabled = false;
                tilPasswordReg2.ErrorEnabled = false;

                return true;
            }
            else
            {
                tilPasswordReg.ErrorEnabled = true;
                tilPasswordReg2.ErrorEnabled = true;

                tilPasswordReg.Error = "Kata laluan tidak sepadan.";
                tilPasswordReg2.Error = "Kata laluan tidak sepadan.";

                return false;
            }
        }

        private async Task<bool> ValidateICAsync()
        {
            string identification = new string(tieICNumber.Text.Where(c => char.IsDigit(c)).ToArray());
            tieICNumber.Text = identification;

            if (!MySilatProfile.IsIdentificationValid(identification))
            {
                tilICNumber.ErrorEnabled = true;
                tilICNumber.Error = "IC tidak sah.";
                return false;
            }

            int exist = await MySilatProfile.CheckICExistAsync(identification);

            if (exist != 0)
            {
                tilICNumber.ErrorEnabled = true;
                tilICNumber.Error = "IC sudah digunakan.";
                return false;
            }

            tilICNumber.ErrorEnabled = false;
            return true;
        }

        private bool ValidateFirstName()
        {
            string formattedName = MySilatProfile.FormattedName(tieFirstName.Text);

            tieFirstName.Text = formattedName;

            if (formattedName.Length < 3)
            {
                tilFirstName.ErrorEnabled = true;
                tilFirstName.Error = "Minima 2 huruf.";
                return false;
            }

            else if (formattedName.Length > 50)
            {
                tilFirstName.ErrorEnabled = true;
                tilFirstName.Error = "Maksima 50 huruf.";
                return false;
            }

            tilFirstName.ErrorEnabled = false;
            return true;
        }

        private bool ValidateLastName()
        {
            string formattedName = MySilatProfile.FormattedName(tieLastName.Text);

            tieLastName.Text = formattedName;

            if (formattedName.Length < 3)
            {
                tilLastName.ErrorEnabled = true;
                tilLastName.Error = "Minima 2 huruf.";
                return false;
            }

            else if (formattedName.Length > 50)
            {
                tilLastName.ErrorEnabled = true;
                tilLastName.Error = "Maksima 50 huruf.";
                return false;
            }

            tilLastName.ErrorEnabled = false;
            return true;
        }

        private bool ValidateAltName()
        {
            string formattedName = MySilatProfile.FormattedName(tieAltName.Text);

            tieAltName.Text = formattedName;

            if (formattedName.Length < 4)
            {
                tilAltName.ErrorEnabled = true;
                tilAltName.Error = "Minima 3 huruf.";
                return false;
            }

            else if (formattedName.Length > 15)
            {
                tilAltName.ErrorEnabled = true;
                tilAltName.Error = "Maksima 15 huruf.";
                return false;
            }

            tilAltName.ErrorEnabled = false;
            return true;
        }

        void RegisterError(string error)
        {
            Console.WriteLine("Exception : " + error);
            NetworkManager.IsOnline(this, true, ToastLength.Long);

            Snackbar
                  .Make(lytMain, GetString(Resource.String.oops), Snackbar.LengthShort)
                  .Show();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            tilEmailLogin.ErrorEnabled = false;

            sfBusyIndicator.Visibility = ViewStates.Visible;
            loginCard.Visibility = ViewStates.Invisible;
            lytRegister.Visibility = ViewStates.Invisible;

            if (MySilatCommon.IsValidEmailAddress(tieEmailLogin.Text))
            {
                LoginByEmail();
            }
            else if (MySilatProfile.IsIdentificationValid(tieEmailLogin.Text))
            {
                LoginByIdentification();
            }
            else
            {
                tilEmailLogin.ErrorEnabled = true;
                tilEmailLogin.Error = "Input tidak tepat.";
            }

        }

        private void TxtRegister_Click(object sender, EventArgs e)
        {
            if (registerDialog != null)
            {
                if (registerDialog.IsShowing)
                    return;
                else
                    ShowRegisterDialog();
            }
            else
                ShowRegisterDialog();
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            RegisterNewUserAsync();
        }

        private void TieEmailLogin_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilEmailLogin.ErrorEnabled = false;
        }

        private void TieEmailReg_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidateEmailReg();
            }
        }

        private async void TxtICNumber_FocusChangeAsync(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                await ValidateICAsync();
            }
        }

        private void TiePasswordReg_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidatePasswordReg();
            }
        }

        private void TxtFirstName_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidateFirstName();
            }
        }

        private void TxtLastName_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidateLastName();
            }
        }

        private void TxtAltName_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == false)
            {
                ValidateAltName();
            }
        }

        private void TieEmailReg_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilEmailReg.ErrorEnabled = false;
        }

        private void TiePasswordReg_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilPasswordReg.ErrorEnabled = false;
            tilPasswordReg2.ErrorEnabled = false;
        }

        private void TxtICNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilICNumber.ErrorEnabled = false;
        }

        private void TxtFirstName_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilFirstName.ErrorEnabled = false;
        }

        private void TxtLastName_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilLastName.ErrorEnabled = false;
        }

        private void TxtAltName_TextChanged(object sender, TextChangedEventArgs e)
        {
            tilAltName.ErrorEnabled = false;
        }

        private void AddRegisterDialogListener()
        {
            tieEmailReg.TextChanged += TieEmailReg_TextChanged;
            tieEmailReg.FocusChange += TieEmailReg_FocusChange;

            tiePasswordReg.TextChanged += TiePasswordReg_TextChanged;
            tiePasswordReg.FocusChange += TiePasswordReg_FocusChange;

            tiePasswordReg2.TextChanged += TiePasswordReg_TextChanged;
            tiePasswordReg2.FocusChange += TiePasswordReg_FocusChange;

            tieICNumber.TextChanged += TxtICNumber_TextChanged;
            tieICNumber.FocusChange += TxtICNumber_FocusChangeAsync;

            tieFirstName.TextChanged += TxtFirstName_TextChanged;
            tieFirstName.FocusChange += TxtFirstName_FocusChange;

            tieLastName.TextChanged += TxtLastName_TextChanged;
            tieLastName.FocusChange += TxtLastName_FocusChange;

            tieAltName.TextChanged += TxtAltName_TextChanged;
            tieAltName.FocusChange += TxtAltName_FocusChange;
        }

        private void RemoveRegisterDialogListener()
        {
            tieEmailReg.TextChanged -= TieEmailReg_TextChanged;
            tieEmailReg.FocusChange -= TieEmailReg_FocusChange;

            tiePasswordReg.TextChanged -= TiePasswordReg_TextChanged;
            tiePasswordReg.FocusChange -= TiePasswordReg_FocusChange;

            tiePasswordReg2.TextChanged -= TiePasswordReg_TextChanged;
            tiePasswordReg2.FocusChange -= TiePasswordReg_FocusChange;

            tieICNumber.TextChanged -= TxtICNumber_TextChanged;
            tieICNumber.FocusChange -= TxtICNumber_FocusChangeAsync;

            tieFirstName.TextChanged -= TxtFirstName_TextChanged;
            tieFirstName.FocusChange -= TxtFirstName_FocusChange;

            tieLastName.TextChanged -= TxtLastName_TextChanged;
            tieLastName.FocusChange -= TxtLastName_FocusChange;

            tieAltName.TextChanged -= TxtAltName_TextChanged;
            tieAltName.FocusChange -= TxtAltName_FocusChange;
        }
    }
}