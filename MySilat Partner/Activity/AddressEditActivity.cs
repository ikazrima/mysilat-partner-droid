﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "AddressEditActivity", Theme = "@style/MyTheme")]
    class AddressEditActivity : v7App.AppCompatActivity
    {
        const string TAG = "AddressEditActivity";

        #region Var
        string activityType;
        bool init = false;
        bool loading = true;

        string id;
        int ownerId;
        int ownerType;

        string line1;
        string line2;
        string postcode;
        string addressState;
        #endregion

        #region UI
        View rootView;

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        Button btnSave;

        List<int> addressStateId = new List<int>();
        List<string> addressStateName = new List<string>();
        AddressStateList addressStateList;

        ArrayAdapter addressStateAdapter;

        TextInputLayout tilAddress;
        TextInputEditText tieAddress;

        TextInputLayout tilDistrict;
        TextInputEditText tieDistrict;

        TextInputLayout tilPostcode;
        TextInputEditText tiePostcode;

        Spinner addressStateSpinner;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.address_edit);

            rootView = FindViewById(Resource.Id.root);

            btnSave = (Button)FindViewById(Resource.Id.btn_save);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            addressStateSpinner = (Spinner)FindViewById(Resource.Id.spinner_negeri);

            tilAddress = (TextInputLayout)FindViewById(Resource.Id.til_address);
            tieAddress = (TextInputEditText)FindViewById(Resource.Id.tie_address);

            tilDistrict = (TextInputLayout)FindViewById(Resource.Id.til_district);
            tieDistrict = (TextInputEditText)FindViewById(Resource.Id.tie_district);

            tilPostcode = (TextInputLayout)FindViewById(Resource.Id.til_postcode);
            tiePostcode = (TextInputEditText)FindViewById(Resource.Id.tie_postcode);

            GetIntent();
        }

        protected override async void OnResume()
        {
            base.OnResume();

            if (!init)
            {
                await InitAddressStateSpinnerAsync();

                init = true;
            }


            if (string.Compare(activityType, GetString(Resource.String.intent_new)) == 0)
            {
                tilAddress.Enabled = true;
            }

            else if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                tieAddress.Text = line1;
                tieDistrict.Text = line2;
                tiePostcode.Text = postcode;

                for (int i = 0; i < addressStateId.Count; i++)
                {
                    int tempStateId = 0;
                    int.TryParse(addressState, out tempStateId);

                    if (addressStateId[i] == tempStateId)
                    {
                        addressStateSpinner.SetSelection(i);
                        break;
                    }
                }
            }

            loading = false;
        }

        void GetIntent()
        {
            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            activityType = Intent.GetStringExtra(GetString(Resource.String.intent_activity_type));

            if (string.Compare(activityType, GetString(Resource.String.intent_new)) == 0)
            {
                ownerId = Intent.GetIntExtra("owner_id", 0);
                ownerType = Intent.GetIntExtra("owner_type", 0);

            }
            else if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                id = Intent.GetStringExtra("id");
                line1 = Intent.GetStringExtra("line1");
                line2 = Intent.GetStringExtra("line2");
                postcode = Intent.GetStringExtra("postcode");
                addressState = Intent.GetStringExtra("address_state");

                btnSave.Click += BtnSave_Click;
            }
        }

        async void SaveEditAsync()
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            line1 = tieAddress.Text;
            line2 = tieDistrict.Text;
            postcode = tiePostcode.Text;
            addressState = "" + addressStateId[addressStateSpinner.SelectedItemPosition];

            snackbar.Show();

            var finalResult = await MySilatAddressBook.UpdateAddress(id, line1, line2, postcode, addressState);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "address_edit");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        async Task InitAddressStateSpinnerAsync()
        {
            addressStateSpinner.Enabled = false;

            List<AddressStateModel> result = await MySilatAddressBook.GetAllAddressStates();

            addressStateList = new AddressStateList();
            await addressStateList.GenerateAllAddressStateAsync();

            foreach (var item in addressStateList.mAddressState)
            {
                int id;
                int.TryParse(item.Id, out id);

                addressStateId.Add(id);
                addressStateName.Add(item.State);
            }

            addressStateAdapter = new ArrayAdapter<string>(this, Resource.Layout.my_spinner_left, addressStateName);
            addressStateAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item_left);
            addressStateSpinner.Adapter = addressStateAdapter;

            addressStateSpinner.Enabled = true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!loading)
            {
                switch (item.ItemId)
                {
                    case Android.Resource.Id.Home:
                        SetResult(Result.Canceled);
                        Finish();
                        return true;
                }
            }

            return base.OnOptionsItemSelected(item);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!loading)
                SaveEditAsync();
        }
    }
}