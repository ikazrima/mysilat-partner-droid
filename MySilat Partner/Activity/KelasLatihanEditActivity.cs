﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.Fragments;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using v7App = Android.Support.V7.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Activity
{
    [Activity(Label = "KelasLatihanEditActivity", Theme = "@style/MyTheme")]
    class KelasLatihanEditActivity : v7App.AppCompatActivity
    {
        const string TAG = "KelasLatihanEditActivity";

        #region Var
        bool loading = true;

        string kelasId;

        string kelasLatihanId;
        string description;
        DateTime startTime;
        DateTime endTime;
        string days;
        #endregion

        #region UI
        View rootView;

        public v7Widget.Toolbar toolbar;
        public TextView toolbarTitle;

        Button btnSave;
        Button btnDelete;

        TextInputLayout tilDescription;
        TextInputEditText tieDescription;

        Button btnStart;
        Button btnEnd;

        CheckBox chkMon;
        CheckBox chkTue;
        CheckBox chkWed;
        CheckBox chkThu;
        CheckBox chkFri;
        CheckBox chkSat;
        CheckBox chkSun;

        ConstraintLayout lytDelete;
        #endregion

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.kelas_latihan_edit);

            rootView = FindViewById(Resource.Id.root);

            btnSave = (Button)FindViewById(Resource.Id.btn_save);
            btnDelete = (Button)FindViewById(Resource.Id.btn_delete);

            toolbarTitle = (TextView)FindViewById(Resource.Id.toolbar_title);
            toolbar = (v7Widget.Toolbar)FindViewById(Resource.Id.toolbar);
            toolbar.SetTitleTextColor(Android.Graphics.Color.Black);
            toolbar.SetSubtitleTextColor(Android.Graphics.Color.Black);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_arrow_back_white_24dp);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

            tilDescription = (TextInputLayout)FindViewById(Resource.Id.til_kelas_latihan);
            tieDescription = (TextInputEditText)FindViewById(Resource.Id.tie_kelas_latihan);

            btnStart = (Button)FindViewById(Resource.Id.btn_time_start);
            btnEnd = (Button)FindViewById(Resource.Id.btn_time_end);

            chkMon = (CheckBox)FindViewById(Resource.Id.check_monday);
            chkTue = (CheckBox)FindViewById(Resource.Id.check_tuesday);
            chkWed = (CheckBox)FindViewById(Resource.Id.check_wednesday);
            chkThu = (CheckBox)FindViewById(Resource.Id.check_thursday);
            chkFri = (CheckBox)FindViewById(Resource.Id.check_friday);
            chkSat = (CheckBox)FindViewById(Resource.Id.check_saturday);
            chkSun = (CheckBox)FindViewById(Resource.Id.check_sunday);

            lytDelete = (ConstraintLayout)FindViewById(Resource.Id.lyt_delete);

            btnStart.Click += BtnStart_Click;
            btnEnd.Click += BtnEnd_Click;

            GetIntent();

            btnStart.Text = string.Format("{0:hh:mm tt}", startTime);
            btnEnd.Text = string.Format("{0:hh:mm tt}", endTime);

            if(!string.IsNullOrWhiteSpace(days))
                SetCheckboxDays(days);

            if(!string.IsNullOrWhiteSpace(description))
                tieDescription.Text = description;

            loading = false;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (!loading)
            {
                switch (item.ItemId)
                {
                    case Android.Resource.Id.Home:
                        SetResult(Result.Canceled);
                        Finish();
                        return true;
                }
            }

            return base.OnOptionsItemSelected(item);
        }

        void GetIntent()
        {
            toolbarTitle.Text = Intent.GetStringExtra("app_bar_title");

            string activityType = Intent.GetStringExtra(GetString(Resource.String.intent_activity_type));

            if (string.Compare(activityType, GetString(Resource.String.intent_new)) == 0)
            {
                kelasId = Intent.GetStringExtra("kelas_id");

                TimeSpan tsStart = new TimeSpan(20, 30, 0);
                startTime = startTime.Date + tsStart;

                TimeSpan tsEnd = new TimeSpan(22, 30, 0);
                endTime = endTime.Date + tsEnd;

                lytDelete.Visibility = ViewStates.Gone;
                btnSave.Click += BtnAdd_Click;
            }

            else if (string.Compare(activityType, GetString(Resource.String.intent_edit)) == 0)
            {
                kelasLatihanId = Intent.GetStringExtra("kelas_latihan_id");
                description = Intent.GetStringExtra("description");
                startTime = DateTime.ParseExact(Intent.GetStringExtra("start_time"), "HH:mm",
                                    CultureInfo.InvariantCulture);
                endTime = DateTime.ParseExact(Intent.GetStringExtra("end_time"), "HH:mm",
                                    CultureInfo.InvariantCulture);
                days = Intent.GetStringExtra("days");

                btnSave.Click += BtnSave_Click;

                lytDelete.Visibility = ViewStates.Visible;
                btnDelete.Click += BtnDelete_Click;
            }
        }

        async void AddAsync()
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            description = tieDescription.Text;

            List<int> newDays = new List<int>();

            if (chkMon.Checked)
                newDays.Add(1);
            if (chkTue.Checked)
                newDays.Add(2);
            if (chkWed.Checked)
                newDays.Add(3);
            if (chkThu.Checked)
                newDays.Add(4);
            if (chkFri.Checked)
                newDays.Add(5);
            if (chkSat.Checked)
                newDays.Add(6);
            if (chkSun.Checked)
                newDays.Add(7);

            days = string.Join(",", newDays.Select(x => x.ToString()).ToArray());

            snackbar.Show();

            var finalResult = await MySilatKelas.AddKelasLatihan(kelasId, description, startTime, endTime, days);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "kelas_latihan_add");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        async void SaveEditAsync()
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            description = tieDescription.Text;

            List<int> newDays = new List<int>();

            if (chkMon.Checked)
                newDays.Add(1);
            if (chkTue.Checked)
                newDays.Add(2);
            if (chkWed.Checked)
                newDays.Add(3);
            if (chkThu.Checked)
                newDays.Add(4);
            if (chkFri.Checked)
                newDays.Add(5);
            if (chkSat.Checked)
                newDays.Add(6);
            if (chkSun.Checked)
                newDays.Add(7);

            days = string.Join(",", newDays.Select(x => x.ToString()).ToArray());

            snackbar.Show();

            var finalResult = await MySilatKelas.UpdateKelasLatihan(kelasLatihanId, description, startTime, endTime, days);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "kelas_latihan_edit");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        async void ActivateAsync(int status)
        {
            loading = true;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);

            snackbar.Show();

            var finalResult = await MySilatKelas.ActivateKelasLatihan(kelasLatihanId, status);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // SUCCESS
                Intent returnIntent = new Intent();
                returnIntent.PutExtra(GetString(Resource.String.intent_activity_type), "kelas_latihan_edit");
                SetResult(Result.Ok, returnIntent);
                Finish();
            }
            else
            {
                // FAIL
                Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();
            }

            loading = false;
        }

        void SetCheckboxDays(string days)
        {
            string[] daysArray = days.Split(',');
            List<string> daysList = new List<string>(daysArray.Length);
            daysList.AddRange(daysArray);
            daysList.Reverse();

            for (int i = 0; i < daysArray.Length; i++)
            {
                string day = "";

                if (string.Compare(daysArray[i], "1") == 0)
                {
                    chkMon.Checked = true;
                    day = GetString(Resource.String.monday);
                }

                if (string.Compare(daysArray[i], "2") == 0)
                {
                    chkTue.Checked = true;
                    day = GetString(Resource.String.tuesday);
                }

                if (string.Compare(daysArray[i], "3") == 0)
                {
                    chkWed.Checked = true;
                    day = GetString(Resource.String.wednesday);
                }

                if (string.Compare(daysArray[i], "4") == 0)
                {
                    chkThu.Checked = true;
                    day = GetString(Resource.String.thursday);
                }

                if (string.Compare(daysArray[i], "5") == 0)
                {
                    chkFri.Checked = true;
                    day = GetString(Resource.String.friday);
                }

                if (string.Compare(daysArray[i], "6") == 0)
                {
                    chkSat.Checked = true;
                    day = GetString(Resource.String.saturday);
                }

                if (string.Compare(daysArray[i], "7") == 0)
                {
                    chkSun.Checked = true;
                    day = GetString(Resource.String.sunday);
                }
            }
        }

        private void BtnEnd_Click(object sender, EventArgs e)
        {
            TimePickerFragment frag = TimePickerFragment.NewInstance(
                delegate (DateTime time)
                {
                    btnEnd.Text = time.ToShortTimeString();
                    endTime = time;
                });

            frag.Show(FragmentManager, TimePickerFragment.TAG);
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            TimePickerFragment frag = TimePickerFragment.NewInstance(
                delegate (DateTime time)
                {
                    btnStart.Text = time.ToShortTimeString();
                    startTime = time;
                });

            frag.Show(FragmentManager, TimePickerFragment.TAG);
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (!loading)
            {
                AddAsync();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!loading)
                SaveEditAsync();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (!loading)
            {
                ActivateAsync(0);
            }
        }
    }
}