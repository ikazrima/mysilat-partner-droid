﻿using Android.Content;
using MySilatPartner.Activity;
using System;
using System.Collections.Generic;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;

namespace MySilatPartner
{
    class FacebookLogin : AccessTokenTracker
    {
        Context _context;

        FacebookCallBack facebookCallBack = new FacebookCallBack();

        private LoginButton fbLoginButton;

        private ICallbackManager callbackManager;

        private string[] PERMISSIONS = { "email", "public_profile", "user_birthday" };

        public void Init(Context context)
        {
            _context = context;
            facebookCallBack._context = context;

            FacebookSdk.SdkInitialize(_context);

            callbackManager = CallbackManagerFactory.Create();

            fbLoginButton.SetReadPermissions(PERMISSIONS);

            List<string> publicProfile = new List<string>();

            LoginManager.Instance.RegisterCallback(callbackManager, facebookCallBack);
        }

        public bool IsLoggedIn()
        {
            AccessToken accessToken = AccessToken.CurrentAccessToken;
            return accessToken != null;
        }

        public void Logout()
        {
            LoginManager.Instance.LogOut();
        }

        public ICallbackManager GetCallbackManager()
        {
            return callbackManager;
        }

        public void SetLoginButton(LoginButton loginButton)
        {
            fbLoginButton = loginButton;
        }

        protected override async void OnCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken)
        {
            // This way we can access the facebook login from different activities
            /*
             * Disabling this, we only do login if PROFILE change instead
             * 
            if (_context.GetType() == (typeof(LoginActivity)))
            {
                await ((LoginActivity)_context).MainLoginActivityAsync();
            }
            */
        }
    }

    class FBProfileTracker : ProfileTracker
    {
        Context _context;

        public void Init(Context context)
        {
            _context = context;
        }

        protected override void OnCurrentProfileChanged(Profile oldProfile, Profile newProfile)
        {
            try
            {
                if (Profile.CurrentProfile == null)
                    StartTracking();
                else
                    StopTracking();

                Profile.CurrentProfile = newProfile;

                // This way we can access the facebook login from different activities
                if (_context.GetType() == (typeof(LoginActivity)))

                    if (Profile.CurrentProfile != null)
                        ((LoginActivity)_context).LoginByFacebook();
            }
            catch(Exception e)
            {
                Console.WriteLine("FB error : " + e.ToString());
            }
        }
    }
}