﻿using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Android.Util;
using Firebase.Messaging;
using MySilatPartner.Activity;
using System.Collections.Generic;

namespace MySilatPartner
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class FirebaseMsgService : FirebaseMessagingService
    {
        const string TAG = "FirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(TAG, "From: " + message.From);
            Log.Debug(TAG, "Notification Message Body: " + message.GetNotification().Body);

            SendNotification(message.GetNotification().Body, message.Data);
        }

         void SendNotification(string messageBody, IDictionary<string, string> data)
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            foreach (string key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            var action = new NotificationCompat.Action.Builder(Resource.Drawable.ic_check_circle_white_24dp, "Accept", pendingIntent);

            var notificationCompatBuilder = new NotificationCompat.Builder(this)
                .AddAction(action.Build())
                .SetSmallIcon(Resource.Drawable.ic_menu_white_24dp)
                .SetContentTitle("MySilat Partner")
                .SetContentText(messageBody)
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent)
                .SetStyle(new NotificationCompat.InboxStyle())
                .SetPriority(2) // max
                .SetWhen(0);

            var notificationManager = NotificationManager.FromContext(this);
            notificationManager.Notify(0, notificationCompatBuilder.Build());
        }
    }
}