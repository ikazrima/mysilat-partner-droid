﻿using Android.Content;
using Android.Content.PM;
using Android.Gms.Maps.Model;
using Android.Locations;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class GoogleLocationsCommon
    {
        public static async Task<string> GetDirections(LatLng origin, LatLng destination, Context context)
        {
            string apiPath = "https://maps.googleapis.com/maps/api/directions/json?";
            string strOrigin = "origin=" + origin.Latitude + "," + origin.Longitude;
            string strDestination = "destination=" + destination.Latitude + "," + destination.Longitude;

            var bundle = context.PackageManager.GetApplicationInfo(context.PackageName, PackageInfoFlags.MetaData).MetaData;
            string keyName = "com.google.android.maps.v2.API_KEY";
            string strKey = "key=" + bundle.GetString(keyName);

            apiPath += strOrigin + "&" + strDestination + "&" + strKey;

            var uri = Android.Net.Uri.Parse(apiPath);

            var httpClient = new HttpClient();

            var response = await httpClient.GetAsync(uri.ToString());
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<Address> ReverseGeocodeLocation(Location location, Context context)
        {
            Geocoder geocoder = new Geocoder(context);
            IList<Address> addressList =
                await geocoder.GetFromLocationAsync(location.Latitude, location.Longitude, 10);

            Address address = addressList.FirstOrDefault();
            return address;
        }
    }
}