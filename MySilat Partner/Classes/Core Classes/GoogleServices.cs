﻿using Android.Content;
using Android.Gms.Common;
using System;

namespace MySilatPartner
{
    class GoogleServices
    {
        // GooglePlayServices
        public static bool IsGooglePlayServicesInstalled(Context context)
        {
            var queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(context);
            if (queryResult == ConnectionResult.Success)
            {
                Console.WriteLine("Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                var errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                Console.WriteLine("MainActivity", "There is a problem with Google Play Services on this device: {0} - {1}",
                          queryResult, errorString);
            }

            return false;
        }
    }
}