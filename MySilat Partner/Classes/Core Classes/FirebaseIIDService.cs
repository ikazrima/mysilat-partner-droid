﻿using Android.App;
using Firebase.Iid;
using MySilatPartner.API;
using Xamarin.Facebook;

namespace MySilatPartner
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "FirebaseIIDService";

        public override void OnTokenRefresh()
        {
            //FirebaseRegistration.firebaseSet = true;
            //await FirebaseRegistration.RegisterFCMToken(MySilatProfile.UserProfile.Id, FirebaseInstanceId.Instance.Token, FirebaseInstanceId.Instance.Id);
        }
    }
}