﻿using Android.Content;
using Android.Net;
using Android.Widget;
using System;

namespace MySilatPartner
{
    class NetworkManager
    {
        public static Toast toast;

        public static bool IsOnline(Context context, bool showToast, ToastLength length)
        {
            try
            {
                ConnectivityManager connectivityManager = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
                NetworkInfo networkInfo = connectivityManager.ActiveNetworkInfo;

                bool isOnline = true;

                if (networkInfo != null)
                    isOnline = false;
                else
                    isOnline = networkInfo.IsConnected;

                if (!isOnline && showToast)
                {
                    if (toast != null)
                    {
                        if (!toast.View.IsShown)
                        {
                            toast = Toast.MakeText(context, "Not online.", length);
                            toast.Show();
                        }
                    }

                    else
                    {
                        toast = Toast.MakeText(context, "Not online.", length);
                        toast.Show();
                    }
                }

                return isOnline;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

                if (showToast)
                {
                    toast = Toast.MakeText(context, "Not online.", length);
                    toast.Show();
                }

                return false;
            }
        }
    }
}