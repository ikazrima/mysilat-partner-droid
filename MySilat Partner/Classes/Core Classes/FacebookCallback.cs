﻿using Android.Content;
using MySilatPartner.Activity;
using System;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;

namespace MySilatPartner
{
    class FacebookCallBack : Java.Lang.Object, IFacebookCallback, IDisposable
    {
        public Context _context;

        #region IFacebookCallback implementation
        public void OnCancel()
        {

        }

        public void OnError(FacebookException p0)
        {

        }

        public void OnSuccess(Java.Lang.Object p0)
        {
            try
            {
                LoginResult loginResult = (LoginResult)p0;

                // Do login
                // This way we can access the facebook login from different activities
                if (_context.GetType() == (typeof(LoginActivity)))
                {
                    if (Profile.CurrentProfile != null)
                        ((LoginActivity)_context).LoginByFacebook();
                }

                //await FirebaseRegistration.RegisterFCMToken(MySilatProfile.UserProfile.Id, FirebaseInstanceId.Instance.Token, FirebaseRegistration.firebaseDeviceId);
                //await FirebaseRegistration.RegisterFCMToken(Profile.CurrentProfile.Id, FirebaseInstanceId.Instance.Token);
            }
            catch (Exception e)
            {
                Console.WriteLine("FB error : " + e.ToString());
            }
        }

        #endregion
    }
}