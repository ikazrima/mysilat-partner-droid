﻿using Firebase.Storage;

namespace MySilatPartner
{
    public static class FirebaseMyStorage
    {
        public static FirebaseStorage storage;
        public static StorageReference storageRef;

        public static void Init()
        {
            storage = FirebaseStorage.GetInstance("gs://mysilat-partner.appspot.com/");
            storageRef = storage.Reference;
        }
    }
}