﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class FirebaseRegistration
    {
        public static async Task<string> RegisterFCMToken(string user_id, string fcmToken, string fcmDeviceId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "firebase/addfcm");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = user_id;
            parameters["fcm_token"] = fcmToken;
            parameters["fcm_device_id"] = fcmDeviceId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }
    }
}