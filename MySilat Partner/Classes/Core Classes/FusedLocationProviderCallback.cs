﻿using Android.Gms.Location;
using Android.Gms.Maps.Model;
using System;
using System.Linq;

namespace MySilatPartner.Activity
{
    public class FusedLocationProviderCallback : LocationCallback
    {
        readonly MainActivity activity;

        public FusedLocationProviderCallback(MainActivity activity)
        {
            this.activity = activity;
        }

        public override void OnLocationAvailability(LocationAvailability locationAvailability)
        {
            Console.WriteLine("FusedLocationProvider", "IsLocationAvailable: {0}", locationAvailability.IsLocationAvailable);
        }

        public override void OnLocationResult(LocationResult result)
        {
            if (result.Locations.Any())
            {
                var location = result.Locations.First();

                LatLng latlng = new LatLng(location.Latitude, location.Longitude);

                // Do something
            }
            else
            {
                Console.WriteLine("Sample", "No loc.");
                // No locations to work with.
            }
        }
    }
}