﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;

namespace MySilatPartner
{
    public class UserProfileList : ObservableCollection<UserProfile>, INotifyPropertyChanged
    {
        public UserProfileList()
        {
        }

        public async Task GenerateMemberByKelasIdYearTauliahIdAsync(string kelasId, DateTime year, string tauliahIdJson)
        {
            List<UserProfileModel> result = await MySilatKelas.GetMemberByKelasIdAndYear(kelasId, year, tauliahIdJson);

            if (result != null)
            {
                foreach (var item in result)
                {
                    this.Add(new UserProfile(item.ProfileId, item.Sex, item.FirstName, item.LastName, item.AlternateName,
                        item.PeringkatId, item.Peringkat, item.TauliahId, item.JawatanCode, item.Jawatan,
                        item.FacebookId, item.ProfileImgId, item.ProfileImgName, item.ProfileImgUri, item.Year));
                }
            }
        }
    }
}