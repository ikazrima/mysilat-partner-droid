﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;

namespace MySilatPartner
{
    public class BuahList : ObservableCollection<Buah>, INotifyPropertyChanged
    {
        public BuahList()
        {
        }

        public async Task GenerateUserBuahAsync(int peringkatId)
        {
            List<UserBuahModel> userBuahModel = await MySilatBuah.GetUserBuah(MySilatProfile.UserProfile.ProfileId, peringkatId);

            List<BuahModel> buah = await MySilatBuah.GetBuahAll();

            for (int i = 0; i < buah.Count; i++)
            {
                if (buah[i].Active == 0)
                    return;

                int kelasId = 0;
                string kelasCode = "";
                int jurulatihId = 0;
                string jurulatihName = "";
                bool received = false;
                DateTime? receivedDt = DateTime.Today;
                int verifiedBy = 0;
                DateTime verifiedDt = DateTime.Today;

                if (MySilatPersilatan.UserPersilatan != null
                    && MySilatPersilatan.UserPersilatan.PeringkatId >= peringkatId)
                {
                    if (userBuahModel != null)
                    {
                        foreach (var userBuah in userBuahModel)
                        {
                            if (string.Compare(userBuah.BuahId, buah[i].BuahId) == 0)
                            {
                                kelasId = userBuah.KelasId;
                                kelasCode = userBuah.KelasCode;
                                jurulatihId = userBuah.JurulatihId;
                                jurulatihName = userBuah.JurulatihName;
                                received = userBuah.Received != 0;
                                receivedDt = userBuah.ReceivedDt;
                                verifiedBy = userBuah.VerifiedBy;
                                verifiedDt = userBuah.VerifiedDt;

                                break;
                            }
                        }
                    }
                }

                switch (peringkatId)
                {
                    case 1:
                        if (buah[i].Asas == 1)
                        {
                            if (string.Compare(buah[i].BuahId, buah[i].PecahanId) == 0)
                            {
                                // Check if next buah pecahan is true for the peringkat
                                // if not true, display non-pecahan only
                                // if true, display pecahan only

                                if (buah[i + 1].Asas != 1)
                                {
                                    this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                                }
                            }

                            else if (string.Compare(buah[i].PecahanId, "") != 0)
                            {
                                this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                            }
                        }
                        break;

                    case 2:
                        if (buah[i].Jatuh == 1)
                        {
                            if (string.Compare(buah[i].BuahId, buah[i].PecahanId) == 0)
                            {
                                // Refer ASAS

                                if (buah[i + 1].Jatuh != 1)
                                {
                                    this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                                }
                            }

                            else if (string.Compare(buah[i].PecahanId, "") != 0)
                            {
                                this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                            }
                        }
                        break;

                    case 3:
                        if (buah[i].Potong == 1)
                        {
                            if (string.Compare(buah[i].BuahId, buah[i].PecahanId) == 0)
                            {
                                // Refer ASAS

                                if (buah[i + 1].Potong != 1)
                                {
                                    this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                                }
                            }

                            else if (string.Compare(buah[i].PecahanId, "") != 0)
                            {
                                this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                            }
                        }
                        break;

                    case 4:
                        if (buah[i].Tamat == 1)
                        {
                            if (string.Compare(buah[i].BuahId, buah[i].PecahanId) == 0)
                            {
                                // Refer ASAS

                                if (buah[i + 1].Tamat != 1)
                                {
                                    this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                                }
                            }

                            else if (string.Compare(buah[i].PecahanId, "") != 0)
                            {
                                this.Add(new Buah(buah[i].BuahId, buah[i].BuahName, kelasId, kelasCode, jurulatihId, jurulatihName, received, receivedDt, verifiedBy, verifiedDt));
                            }
                        }
                        break;
                }
            }
        }
    }
}