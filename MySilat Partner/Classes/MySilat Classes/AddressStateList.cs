﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class AddressStateList
    {
        public List<AddressState> mAddressState = new List<AddressState>();

        public AddressStateList()
        {
        }

        public int Count
        {
            get { return mAddressState.Count; }
        }

        // Indexer (read only) for accessing a photo:
        public AddressState this[int i]
        {
            get { return mAddressState[i]; }
        }

        public async Task GenerateAllAddressStateAsync()
        {
            List<AddressStateModel> result = await MySilatAddressBook.GetAllAddressStates();

            if (result != null)
            {
                foreach (var item in result)
                {
                    mAddressState.Add(new AddressState(item.Id, item.State, item.Abbreviation));
                }
            }
        }
    }
}