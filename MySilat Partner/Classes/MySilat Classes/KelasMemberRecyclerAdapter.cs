﻿using Android.Support.V7.Widget;
using Android.Views;
using Java.Lang;
using MySilatPartner.Models;
using Square.Picasso;
using System;
using System.Collections.Generic;

namespace MySilatPartner
{
    class KelasMemberRecyclerAdapter : RecyclerView.Adapter
    {
        public event EventHandler<int> ItemClick;

        public KelasMemberList mKelasMemberList;
        List<UserProfile> kelasMemberCopy;

        public Dictionary<int, Android.Net.Uri> imgUri = new Dictionary<int, Android.Net.Uri>();

        int resizeDim = 256;

        public KelasMemberRecyclerAdapter(KelasMemberList kelasMemberlist)
        {
            mKelasMemberList = kelasMemberlist;
            kelasMemberCopy = new List<UserProfile>(kelasMemberlist.mUserProfile);
        }

        public void Filter(string text)
        {
            mKelasMemberList.mUserProfile.Clear();

            if (string.IsNullOrWhiteSpace(text))
            {
                mKelasMemberList.mUserProfile = new List<UserProfile>(kelasMemberCopy);
            }
            else
            {
                text = text.ToLower();

                foreach (var item in kelasMemberCopy)
                {
                    if (item.FirstName.ToLower().Contains(text)
                        || item.LastName.ToLower().Contains(text)
                        || item.AlternateName.ToLower().Contains(text))
                    {
                        mKelasMemberList.mUserProfile.Add(item);
                    }
                }
            }

            NotifyDataSetChanged();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.user_item_card, parent, false);

            KelasMemberViewHolder vh = new KelasMemberViewHolder(itemView);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            KelasMemberViewHolder vh = holder as KelasMemberViewHolder;
  
            DisplayProfileImage(vh, position);
            DisplayTauliah(vh, position);

            vh.AltName.Text = mKelasMemberList[position].AlternateName;
            vh.FullName.Text = mKelasMemberList[position].FirstName + " | " + mKelasMemberList[position].LastName;
        }

        public override void OnViewRecycled(Java.Lang.Object holder)
        {
            base.OnViewRecycled(holder);

            (holder as KelasMemberViewHolder).CleanUp();
        }

        public override int ItemCount
        {
            get { return mKelasMemberList.Count; }
        }

        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }

        void DisplayTauliah(KelasMemberViewHolder vh, int position)
        {
            switch (mKelasMemberList[position].JawatanCode)
            {
                default:
                case "AB":
                case "PK":
                    vh.Status.SetTextColor(Android.Graphics.Color.Black);
                    vh.StatusHeader.SetBackgroundColor(Android.Graphics.Color.White);
                    vh.Status.Text = mKelasMemberList[position].Peringkat;
                    break;
                case "PJP":
                    vh.Status.SetTextColor(Android.Graphics.Color.Black);
                    vh.StatusHeader.SetBackgroundColor(Android.Graphics.Color.White);
                    vh.Status.Text = mKelasMemberList[position].Jawatan;
                    break;
                case "PJ":
                case "J":
                case "PP":
                case "P":
                    vh.Status.SetTextColor(Android.Graphics.Color.White);
                    vh.StatusHeader.SetBackgroundColor(Android.Graphics.Color.Red);
                    vh.Status.Text = mKelasMemberList[position].Jawatan;
                    break;
                case "GU":
                    vh.Status.SetTextColor(Android.Graphics.Color.Black);
                    vh.StatusHeader.SetBackgroundColor(Android.Graphics.Color.Yellow);
                    vh.Status.Text = mKelasMemberList[position].Jawatan;
                    break;
            }
        }

        void DisplayProfileImage(KelasMemberViewHolder vh, int position)
        {
            string userIdString = mKelasMemberList[position].ProfileId;
            int userId = 0;
            int.TryParse(userIdString, out userId);

            if (!Android.Net.Uri.Empty.Equals(imgUri[userId]))
            {
                try
                {
                    Picasso.With(vh.ProfileImage.Context).Load(imgUri[userId]).Resize(resizeDim, resizeDim).Into(vh.ProfileImage);
                }
                catch(System.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    vh.ProfilePlaceHolder.Visibility = ViewStates.Gone;
                    vh.ProfileImage.Visibility = ViewStates.Visible;
                }

            }
            else
            {
                vh.ProfilePlaceHolder.Visibility = ViewStates.Visible;
                vh.ProfileImage.Visibility = ViewStates.Gone;
            }
        }
    }
}