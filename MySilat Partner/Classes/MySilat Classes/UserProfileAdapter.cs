﻿using Android.Content;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Views;
using Android.Widget;
using MySilatPartner.Models;
using MySilatPartner.MyCallbacks;
using Square.Picasso;
using Syncfusion.DataSource;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace MySilatPartner
{
    public class UserProfileAdapter : BaseAdapter<object>
    {
        UserBuahCallback.IAdapterCallback mAdapterCallback;

        DataSource DataSource;
        DataSource DataSourceCopy;

        private Context context;

        int resizeDim = 128;

        public Dictionary<int, Android.Net.Uri> imgUri = new Dictionary<int, Android.Net.Uri>();

        public UserProfileAdapter(DataSource dataSource, Context _context, UserBuahCallback.IAdapterCallback adapterCallback)
            : base()
        {
            this.DataSourceCopy = new DataSource();

            this.DataSource = dataSource;
            this.DataSource.DisplayItems.CollectionChanged += DisplayItems_CollectionChanged;
            context = _context;
            mAdapterCallback = adapterCallback;
        }

        public void Filter(string text)
        {
            if (DataSourceCopy.Items.Count == 0)
            {
                foreach (var item in DataSource.Items)
                {
                    this.DataSourceCopy.Items.Add(item);
                }
            }

            this.DataSource.Items.Clear();

            if (string.IsNullOrWhiteSpace(text))
            {
                foreach (var item in DataSourceCopy.Items)
                {
                    this.DataSource.Items.Add(item);
                }
            }
            else
            {
                text = text.ToLower();

                foreach (var item in DataSourceCopy.Items)
                {
                    if ((item as UserProfile).AlternateName.ToLower().Contains(text)
                        || (item as UserProfile).FirstName.ToLower().Contains(text)
                        || (item as UserProfile).LastName.ToLower().Contains(text))
                    {
                        this.DataSource.Items.Add(item);
                    }
                }
            }

            NotifyDataSetChanged();
        }

        public override int Count
        {
            get
            {
                return DataSource.DisplayItems.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override object this[int index]
        {
            get { return this.DataSource.DisplayItems[index]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View v = convertView;

            if (v == null)
            {
                LayoutInflater vi;
                vi = LayoutInflater.From(this.context);
                v = vi.Inflate(Resource.Layout.user_simple_item_row, null);
            }

            var obj = this[position];

            if (obj != null)
            {
                if (!v.HasOnClickListeners)
                    v.Click += Item_Click;

                v.Tag = position;

                DisplayProfileImage(obj, v);
                DisplayTauliah(obj, v);

                TextView userName = (TextView)v.FindViewById(Resource.Id.txt_user_name);
                userName.Text = (obj as UserProfile).AlternateName;
            }

            return v;
        }

        void DisplayProfileImage(object obj, View v)
        {
            ImageView profileImage = (ImageView)v.FindViewById(Resource.Id.profile_image);
            ImageView profileImagePlaceHolder = (ImageView)v.FindViewById(Resource.Id.profile_image_placeholder);

            string userIdString = (obj as UserProfile).ProfileId;
            int userId = 0;
            int.TryParse(userIdString, out userId);

            if (!Android.Net.Uri.Empty.Equals(imgUri[userId]))
            {
                try
                {
                    Picasso.With(profileImage.Context).Load(imgUri[userId]).Resize(resizeDim, resizeDim).Into(profileImage);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    profileImagePlaceHolder.Visibility = ViewStates.Gone;
                    profileImage.Visibility = ViewStates.Visible;
                }
            }

            else
            {
                profileImagePlaceHolder.Visibility = ViewStates.Visible;
                profileImage.Visibility = ViewStates.Gone;
            }
        }

        void DisplayTauliah(object obj, View v)
        {
            TextView status = (TextView)v.FindViewById(Resource.Id.status_text);
            Color backgroundColor = new Color(ContextCompat.GetColor(status.Context, Resource.Color.SecondaryHeader));

            switch ((obj as UserProfile).JawatanCode)
            {
                default:
                case "AB":
                case "PK":
                case "PJP":

                    status.SetTextColor(Color.White);
                    status.SetBackgroundColor(backgroundColor);
                    status.Text = (obj as UserProfile).JawatanCode;
                    break;
                case "PJ":
                case "J":
                case "PP":
                case "P":
                    status.SetTextColor(Color.White);
                    status.SetBackgroundColor(Color.Red);
                    status.Text = (obj as UserProfile).JawatanCode;
                    break;
                case "GU":
                    status.SetTextColor(Color.Black);
                    status.SetBackgroundColor(Color.Yellow);
                    status.Text = (obj as UserProfile).JawatanCode;
                    break;
            }

        }

        private void Item_Click(object sender, System.EventArgs e)
        {
            int position = (int)(sender as View).Tag;

            mAdapterCallback.OnTenagaPengajarClick(position);
        }

        private void DisplayItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.NotifyDataSetChanged();
        }
    }
}