﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class JurulatihApprovalList : ObservableCollection<JurulatihApproval>, INotifyPropertyChanged
    {
        public List<JurulatihApprovalModel> approvalList;
        List<BuahModel> buahList;
        public List<string> kelasCodeList = new List<string>();

        public JurulatihApprovalList()
        {

        }

        public async Task GenerateApprovalListAsync()
        {
            if (this != null)
                this.Clear();

            if (approvalList != null)
                approvalList.Clear();

            if (buahList != null)
                buahList.Clear();

            approvalList = await MySilatBuah.GetJurulatihApproval(MySilatProfile.UserProfile.ProfileId, DateTime.Now);
            buahList = await MySilatBuah.GetBuahAll();

            if (kelasCodeList != null)
                kelasCodeList.Clear();

            kelasCodeList.Add("Semua Kelas");

            GenerateUnfilteredApprovalList(true);
        }

        public void AddKelasCode(string kelasCode)
        {
            if (kelasCodeList.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(kelasCode))
                {
                    var matchingvalues = kelasCodeList.FirstOrDefault(stringToCheck => stringToCheck.Contains(kelasCode));

                    if (string.IsNullOrEmpty(matchingvalues))
                    {
                        kelasCodeList.Add(kelasCode);
                    }
                }
            }
            else
                kelasCodeList.Add(kelasCode);
        }

        public void GenerateFilteredApprovalList(string filterKelasCode)
        {
            if (this != null)
                this.Clear();

            if (ReferenceEquals(approvalList, null))
                return;

            foreach (var approval in approvalList)
            {
                string buahId;
                string buahName;
                DateTime? receivedDt;
                int userBuahId;
                string profileId;
                string userAltName;
                string profileImgId;
                string profileImgName;
                string profileImgUri;
                string facebookId;
                int peringkatId;
                string peringkat;
                int kelasId;
                string kelasCode;

                BuahModel buah = buahList.Find(x => x.BuahId == approval.BuahId);

                if (string.Compare(approval.KelasCode, filterKelasCode) == 0)
                {
                    buahId = approval.BuahId;
                    buahName = buah.BuahName;
                    receivedDt = approval.ReceivedDt;
                    userBuahId = approval.UserBuahId;
                    profileId = approval.ProfileId;
                    userAltName = approval.UserAltName;
                    profileImgId = approval.ProfileImgId;
                    profileImgName = approval.ProfileImgName;
                    profileImgUri = approval.ProfileImgUri;
                    facebookId = approval.FacebookId;
                    peringkatId = approval.PeringkatId;
                    peringkat = approval.Peringkat;
                    kelasId = approval.KelasId;
                    kelasCode = approval.KelasCode;

                    this.Add(new JurulatihApproval(buahId, buahName, receivedDt, userBuahId, profileId, userAltName, facebookId,
                        profileImgId, profileImgName, profileImgUri,
                        peringkatId, peringkat, kelasId, kelasCode));
                }
            }
        }

        public void GenerateUnfilteredApprovalList(bool populateKelasCode)
        {
            if (this != null)
            {
                this.Clear();
            }

            if (ReferenceEquals(approvalList, null))
                return;

            foreach (var approval in approvalList)
            {
                string buahId;
                string buahName;
                DateTime? receivedDt;
                int userBuahId;
                string userId;
                string userAltName;
                string profileImgId;
                string profileImgName;
                string profileImgUri;
                string facebookId;
                int peringkatId;
                string peringkat;
                int kelasId;
                string kelasCode;

                BuahModel buah = buahList.Find(x => x.BuahId == approval.BuahId);

                buahId = approval.BuahId;
                buahName = buah.BuahName;
                receivedDt = approval.ReceivedDt;
                userBuahId = approval.UserBuahId;
                userId = approval.ProfileId;
                userAltName = approval.UserAltName;
                profileImgId = approval.ProfileImgId;
                profileImgName = approval.ProfileImgName;
                profileImgUri = approval.ProfileImgUri;
                facebookId = approval.FacebookId;
                peringkatId = approval.PeringkatId;
                peringkat = approval.Peringkat;
                kelasId = approval.KelasId;
                kelasCode = approval.KelasCode;

                this.Add(new JurulatihApproval(buahId, buahName, receivedDt, userBuahId, userId, userAltName, facebookId,
                        profileImgId, profileImgName, profileImgUri,
                        peringkatId, peringkat, kelasId, kelasCode));

                if (populateKelasCode)
                    AddKelasCode(kelasCode);
            }
        }
    }
}