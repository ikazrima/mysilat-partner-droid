﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace MySilatPartner
{
    class KelasLatihanViewHolder : RecyclerView.ViewHolder
    {
        public TextView kelasCode { get; set; }
        public TextView description { get; set; }
        public TextView time { get; set; }
        public TextView days { get; set; }
        public Switch kelasAttendace { get; set; }
        public ImageButton btnMore { get; set; }

        public KelasLatihanViewHolder(View itemView) : base(itemView)
        {
            kelasCode = (TextView)itemView.FindViewById(Resource.Id.txt_kelas_code);
            description = (TextView)itemView.FindViewById(Resource.Id.txt_description);
            time = (TextView)itemView.FindViewById(Resource.Id.txt_time);
            days = (TextView)itemView.FindViewById(Resource.Id.txt_days);
            kelasAttendace = (Switch)itemView.FindViewById(Resource.Id.switch_kelas_attendance);
            btnMore = (ImageButton)itemView.FindViewById(Resource.Id.btn_more);
        }
    }
}