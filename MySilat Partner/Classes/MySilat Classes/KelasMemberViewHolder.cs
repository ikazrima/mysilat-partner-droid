﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Square.Picasso;
using System;

namespace MySilatPartner
{
    class KelasMemberViewHolder : RecyclerView.ViewHolder
    {
        public ImageView ProfilePlaceHolder { get; set; }
        public ImageView ProfileImage { get; set; }
        public LinearLayout StatusHeader { get; set; }
        public TextView Status { get; set; }
        public TextView AltName { get; set; }
        public TextView FullName { get; set; }

        public KelasMemberViewHolder(View itemView) : base(itemView)
        {
            ProfilePlaceHolder = (ImageView)itemView.FindViewById(Resource.Id.profile_image_placeholder);
            ProfileImage = (ImageView)itemView.FindViewById(Resource.Id.profile_image);
            StatusHeader = (LinearLayout)itemView.FindViewById(Resource.Id.status_header);
            Status = (TextView)itemView.FindViewById(Resource.Id.status_text);
            AltName = (TextView)itemView.FindViewById(Resource.Id.txtAltName);
            FullName = (TextView)itemView.FindViewById(Resource.Id.txtFullName);
        }

        public void CleanUp()
        {
            try
            {
                Picasso.With(ProfileImage.Context).CancelRequest(ProfileImage);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}