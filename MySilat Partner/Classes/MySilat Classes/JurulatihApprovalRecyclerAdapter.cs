﻿using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using MySilatPartner.MyCallbacks;
using Square.Picasso;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MySilatPartner
{
    class JurulatihApprovalRecyclerAdapter : RecyclerView.Adapter
    {
        public JurulatihApprovalCallback.IAdapterCallback mAdapterCallback;

        public event EventHandler<int> ItemClick;
        public event EventHandler<int> ItemLongClick;

        JurulatihApprovalList mJurulatihApprovalList;
        public List<JurulatihApprovalModel> approvalList;
        public List<string> kelasCodeList = new List<string>();

        public List<bool> selected = new List<bool>();
        public Dictionary<int, Android.Net.Uri> imgUri = new Dictionary<int, Android.Net.Uri>();
        int resizeDim = 128;

        public JurulatihApprovalRecyclerAdapter(JurulatihApprovalList jurulatihApprovalList, JurulatihApprovalCallback.IAdapterCallback adapterCallback)
        {
            mJurulatihApprovalList = jurulatihApprovalList;
            mAdapterCallback = adapterCallback;
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.jurulatih_approval_item_row, parent, false);

            JurulatihApprovalViewHolder vh = new JurulatihApprovalViewHolder(itemView, OnClick);

            vh.ItemView.Click += ItemView_Click;
            vh.ItemView.LongClick += ItemView_LongClick;

            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            JurulatihApprovalViewHolder vh = holder as JurulatihApprovalViewHolder;

            vh.ItemView.Tag = position;

            if (selected[position])
                vh.ItemSelected.Visibility = ViewStates.Visible;
            else
                vh.ItemSelected.Visibility = ViewStates.Gone;

            DisplayProfileImage(mJurulatihApprovalList[position].ProfileId, vh);

            vh.UserName.Text = mJurulatihApprovalList[position].UserAltName;
            vh.KelasCode.Text = mJurulatihApprovalList[position].KelasCode;
            vh.BuahName.Text = mJurulatihApprovalList[position].BuahName;
            vh.Peringkat.Text = mJurulatihApprovalList[position].Peringkat;
            vh.ReceivedDt.Text = mJurulatihApprovalList[position].ReceivedDt.Value.ToString("dd MMM yyyy"); ;

            vh.BuahName.Selected = true;
            vh.BuahName.Ellipsize = TextUtils.TruncateAt.Marquee;
            vh.BuahName.SetSingleLine(true);
            vh.BuahName.SetMarqueeRepeatLimit(-1);
        }

        private void ItemView_Click(object sender, EventArgs e)
        {
            int position = (int)(sender as View).Tag;

            selected[position] = !selected[position];

            mAdapterCallback.OnApprovalSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        private void ItemView_LongClick(object sender, View.LongClickEventArgs e)
        {
            int position = (int)(sender as View).Tag;

            selected[position] = !selected[position];

            for (int x = 0; x < selected.Count; x++)
            {
                selected[x] = selected[position];
            }

            mAdapterCallback.OnApprovalSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        public override int ItemCount
        {
            get { return mJurulatihApprovalList.Count; }
        }

        //This will fire any event handlers that are registered with our ItemClick
        //event.
        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }

        void DisplayProfileImage(string profileId, JurulatihApprovalViewHolder vh)
        {
            int userId = 0;
            int.TryParse(profileId, out userId);

            Android.Net.Uri uri = imgUri[userId];

            if (!Android.Net.Uri.Empty.Equals(uri))
            {
                try
                {
                    Picasso.With(vh.ProfileImg.Context).Load(imgUri[userId]).Resize(resizeDim, resizeDim).Into(vh.ProfileImg);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    vh.ProfileImgPlaceholder.Visibility = ViewStates.Gone;
                    vh.ProfileImg.Visibility = ViewStates.Visible;
                }
            }
            else
            {
                vh.ProfileImgPlaceholder.Visibility = ViewStates.Visible;
                vh.ProfileImg.Visibility = ViewStates.Gone;
            }
        }

        public int SelectedCount()
        {
            int selectedCount = 0;

            for (int i = 0; i < selected.Count; i++)
            {
                if (selected[i])
                {
                    selectedCount++;
                }
            }

            return selectedCount;
        }

        public void DeselectAll()
        {
            for (int i = 0; i < selected.Count; i++)
            {
                selected[i] = false;
            }

            NotifyDataSetChanged();
        }
    }
}