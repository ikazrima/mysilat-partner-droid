﻿using Android.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.Models;
using MySilatPartner.MyCallbacks;
using Syncfusion.DataSource;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace MySilatPartner
{
    public class BuahAdapter : BaseAdapter<object>
    {
        UserBuahCallback.IAdapterCallback mAdapterCallback;

        DataSource DataSource;
        public int peringkatId = 1;
        public List<bool> selected = new List<bool>();

        private Context context;

        public BuahAdapter(DataSource dataSource, Context _context, UserBuahCallback.IAdapterCallback adapterCallback)
            : base()
        {
            this.DataSource = dataSource;
            this.DataSource.DisplayItems.CollectionChanged += DisplayItems_CollectionChanged;
            context = _context;
            mAdapterCallback = adapterCallback;
        }

        public override int Count
        {
            get
            {
                return DataSource.DisplayItems.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override object this[int index]
        {
            get { return this.DataSource.DisplayItems[index]; }
        }

        /* Hackish way to disable recycling view*/
        public override int ViewTypeCount => base.ViewTypeCount;
        public override int GetItemViewType(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View v = convertView;

            if (v == null)
            {
                LayoutInflater vi;
                vi = LayoutInflater.From(this.context);
                v = vi.Inflate(Resource.Layout.user_buah_item_row, null);
            }

            var obj = this[position];

            if (obj != null)
            {
                v.Tag = position;

                if (MySilatPersilatan.UserPersilatan.PeringkatId >= peringkatId)
                {
                    if (!v.HasOnClickListeners)
                    {
                        v.Click += V_Click;
                        v.LongClick += V_LongClick;
                    }
                }

                View viewItemSelected = (View)v.FindViewById(Resource.Id.shape_item_selected);

                if (selected[position])
                    viewItemSelected.Visibility = ViewStates.Visible;
                else
                    viewItemSelected.Visibility = ViewStates.Gone;

                TextView txtBuahId = (TextView)v.FindViewById(Resource.Id.buah_id);
                TextView txtBuahName = (TextView)v.FindViewById(Resource.Id.buah_name);
                ImageView imgVerified = (ImageView)v.FindViewById(Resource.Id.img_verified);
                CardView cardReceived = (CardView)v.FindViewById(Resource.Id.received_card);
                TextView txtReceivedDt = (TextView)v.FindViewById(Resource.Id.received_dt);
                CardView cardTP = (CardView)v.FindViewById(Resource.Id.tp_card);
                TextView txtJurulatih = (TextView)v.FindViewById(Resource.Id.jurulatih);

                if (txtBuahId != null)
                {
                    txtBuahId.Text = (obj as Buah).BuahId;

                    // Buah 6-6
                    // Buah Serang
                    if ((obj as Buah).BuahId.Equals("022")
                        || (obj as Buah).BuahId.Equals("023"))
                        txtBuahId.Text = "Umum";
                }

                if ((obj as Buah).VerifiedBy == 0)
                    imgVerified.Visibility = ViewStates.Gone;
                else
                    imgVerified.Visibility = ViewStates.Visible;

                if (txtBuahName != null)
                    txtBuahName.Text = (obj as Buah).BuahName;

                if (cardReceived != null)
                    cardReceived.Visibility = ViewStates.Invisible;

                if (txtReceivedDt != null)
                    txtReceivedDt.Visibility = ViewStates.Gone;

                if (cardTP != null)
                    cardTP.Visibility = ViewStates.Invisible;


                if ((obj as Buah).Received == true)
                {
                    cardReceived.Visibility = ViewStates.Visible;

                    if ((obj as Buah).JurulatihId != 0)
                    {
                        if (txtJurulatih != null)
                        {
                            txtJurulatih.Text = (obj as Buah).JurulatihName;
                            cardTP.Visibility = ViewStates.Visible;
                        }
                    }

                    if (txtReceivedDt != null)
                    {
                        if ((obj as Buah).ReceivedDt.HasValue)
                        {
                            txtReceivedDt.Text = (obj as Buah).ReceivedDt.Value.ToString("dd MMM yyyy");
                            txtReceivedDt.Visibility = ViewStates.Visible;
                        }
                    }
                }
            }

            return v;
        }

        private void V_LongClick(object sender, View.LongClickEventArgs e)
        {
            int position = (int)(sender as View).Tag;

            selected[position] = !selected[position];

            for (int x = 0; x < selected.Count; x++)
            {
                var obj = this[x];

                if ((obj as Buah).VerifiedBy != 0)
                    selected[x] = false;
                else
                    selected[x] = selected[position];
            }

            mAdapterCallback.OnUserBuahSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        private void V_Click(object sender, EventArgs e)
        {
            int position = (int)(sender as View).Tag;

            var obj = this[position];

            if ((obj as Buah).VerifiedBy != 0)
                selected[position] = false;
            else
                selected[position] = !selected[position];

            mAdapterCallback.OnUserBuahSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        public int SelectedCount()
        {
            int selectedCount = 0;

            for (int i = 0; i < selected.Count; i++)
            {
                if (selected[i])
                {
                    selectedCount++;
                }
            }

            return selectedCount;
        }

        private void DisplayItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.NotifyDataSetChanged();
        }
    }
}