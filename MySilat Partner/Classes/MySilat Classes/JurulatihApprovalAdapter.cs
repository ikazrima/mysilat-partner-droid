﻿using Android.Content;
using Android.Views;
using Android.Widget;
using MySilatPartner.Models;
using MySilatPartner.MyCallbacks;
using Square.Picasso;
using Syncfusion.DataSource;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace MySilatPartner
{
    public class JurulatihApprovalAdapter : BaseAdapter<object>
    {
        JurulatihApprovalCallback.IAdapterCallback mAdapterCallback;

        DataSource DataSource;
        public List<bool> selected = new List<bool>();

        private Context context;

        public Dictionary<int, Android.Net.Uri> imgUri = new Dictionary<int, Android.Net.Uri>();

        int resizeDim = 128;

        public JurulatihApprovalAdapter(DataSource dataSource, Context _context, JurulatihApprovalCallback.IAdapterCallback adapterCallback)
            : base()
        {
            this.DataSource = dataSource;
            this.DataSource.DisplayItems.CollectionChanged += DisplayItems_CollectionChanged;
            context = _context;
            mAdapterCallback = adapterCallback;
        }

        public override int Count
        {
            get
            {
                return DataSource.DisplayItems.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override object this[int index]
        {
            get { return this.DataSource.DisplayItems[index]; }
        }

        /* Hackish way to disable recycling view*/
        public override int ViewTypeCount => base.ViewTypeCount;

        public override int GetItemViewType(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View v = convertView;

            if (v == null)
            {
                LayoutInflater vi;
                vi = LayoutInflater.From(this.context);
                v = vi.Inflate(Resource.Layout.jurulatih_approval_item_row, null);
            }

            var obj = this[position];

            if (obj != null)
            {
                v.Tag = position;

                if (!v.HasOnClickListeners)
                {
                    v.Click += V_Click;
                    v.LongClick += V_LongClick;
                }

                View viewItemSelected = (View)v.FindViewById(Resource.Id.shape_item_selected);

                if (selected[position])
                    viewItemSelected.Visibility = ViewStates.Visible;
                else
                    viewItemSelected.Visibility = ViewStates.Gone;

                DisplayProfileImage((obj as JurulatihApproval), v);

                TextView txtUserName = (TextView)v.FindViewById(Resource.Id.user_name);
                TextView txtKelasCode = (TextView)v.FindViewById(Resource.Id.kelas_code);
                TextView txtBuahName = (TextView)v.FindViewById(Resource.Id.buah_name);
                TextView txtPeringkat = (TextView)v.FindViewById(Resource.Id.peringkat);
                TextView txtReceivedDt = (TextView)v.FindViewById(Resource.Id.received_dt);

                if (txtUserName != null)
                    txtUserName.Text = (obj as JurulatihApproval).UserAltName;

                if (txtKelasCode != null)
                    txtKelasCode.Text = (obj as JurulatihApproval).KelasCode;

                if (txtBuahName != null)
                    txtBuahName.Text = (obj as JurulatihApproval).BuahName;

                if (txtPeringkat != null)
                    txtPeringkat.Text = (obj as JurulatihApproval).Peringkat;

                if (txtReceivedDt != null)
                    txtReceivedDt.Text = (obj as JurulatihApproval).ReceivedDt.Value.ToString("dd MMM yyyy");

            }

            return v;
        }

        private void V_LongClick(object sender, View.LongClickEventArgs e)
        {
            int position = (int)(sender as View).Tag;

            selected[position] = !selected[position];

            for (int x = 0; x < selected.Count; x++)
            {
                selected[x] = selected[position];
            }

            mAdapterCallback.OnApprovalSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        private void V_Click(object sender, EventArgs e)
        {
            int position = (int)(sender as View).Tag;

            var obj = this[position];
            selected[position] = !selected[position];

            mAdapterCallback.OnApprovalSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        public void DeselectAll()
        {
            for (int i = 0; i < selected.Count; i++)
            {
                selected[i] = false;
            }

            mAdapterCallback.OnApprovalSelectedChange(SelectedCount());
            NotifyDataSetChanged();
        }

        public int SelectedCount()
        {
            int selectedCount = 0;

            for (int i = 0; i < selected.Count; i++)
            {
                if (selected[i])
                {
                    selectedCount++;
                }
            }

            return selectedCount;
        }

        private void DisplayItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.NotifyDataSetChanged();
        }

        void DisplayProfileImage(JurulatihApproval jurulatihApproval, View v)
        {
            ImageView profileImage = (ImageView)v.FindViewById(Resource.Id.profile_image);
            ImageView profileImagePlaceHolder = (ImageView)v.FindViewById(Resource.Id.profile_image_placeholder);

            string userIdString = jurulatihApproval.ProfileId;
            int userId = 0;
            int.TryParse(userIdString, out userId);

            Android.Net.Uri uri = imgUri[userId];

            if (!Android.Net.Uri.Empty.Equals(uri))
            {
                try
                {
                    Picasso.With(profileImage.Context).Load(imgUri[userId]).Resize(resizeDim, resizeDim).Into(profileImage);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    profileImagePlaceHolder.Visibility = ViewStates.Gone;
                    profileImage.Visibility = ViewStates.Visible;
                }
            }
            else
            {
                profileImagePlaceHolder.Visibility = ViewStates.Visible;
                profileImage.Visibility = ViewStates.Gone;
            }
        }

        public void CleanUp()
        {
            foreach (var item in DataSource.DisplayItems)
            {
                try
                {
                    ImageView profileImage = (ImageView)(item as View).FindViewById(Resource.Id.profile_image);
                    Picasso.With(profileImage.Context).CancelRequest(profileImage);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }
            }

        }
    }
}