﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;

namespace MySilatPartner
{
    class TauliahViewHolder : RecyclerView.ViewHolder
    {
        public TextView KelasName { get; set; }
        public TextView KelasCode { get; set; }
        public TextView Year { get; set; }
        public ImageView Icon { get; set; }
        public CardView JawatanCard { get; set; }
        public TextView TauliahCode { get; set; }

        public TauliahViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            KelasName = (TextView)itemView.FindViewById(Resource.Id.txt_kelas_name);
            KelasCode = (TextView)itemView.FindViewById(Resource.Id.txt_kelas_code);
            Year = (TextView)itemView.FindViewById(Resource.Id.txt_year);
            Icon = (ImageView)itemView.FindViewById(Resource.Id.kelas_icon);
            JawatanCard = (CardView)itemView.FindViewById(Resource.Id.jawatan_card);
            TauliahCode = (TextView)itemView.FindViewById(Resource.Id.txt_tauliah_code);

            itemView.Click += (sender, e) => listener(base.Position);
        }
    }
}