﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class KelasTypeList
    {
        public List<KelasType> mKelasType = new List<KelasType>();

        public KelasTypeList()
        {
        }

        public int Count
        {
            get { return mKelasType.Count; }
        }

        // Indexer (read only) for accessing a photo:
        public KelasType this[int i]
        {
            get { return mKelasType[i]; }
        }

        public async Task GenerateAllKelasTypeAsync()
        {
            List<KelasTypeModel> result = await MySilatKelas.GetAllKelasType();

            if (result != null)
            {
                foreach (var item in result)
                {
                    mKelasType.Add(new KelasType(item.Id, item.Type));
                }
            }
        }
    }
}