﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class TauliahList
    {
        public List<Tauliah> mTauliah = new List<Tauliah>();

        public TauliahList()
        {
        }

        public int Count
        {
            get { return mTauliah.Count; }
        }

        // Indexer (read only) for accessing a photo:
        public Tauliah this[int i]
        {
            get { return mTauliah[i]; }
        }

        public async Task GenerateMemberByKelasIdYearTauliahIdAsync(string userId, int year)
        {
            List<TauliahModel> result = await MySilatTauliah.GetTauliahUserYear(userId, "" + year);

            if (result != null)
            {
                foreach (var item in result)
                {
                    mTauliah.Add(new Tauliah(item.TauliahId, item.Code, item.Jawatan, item.KelasId, item.KelasCode, item.KelasName, item.KelasType, item.Year));
                }
            }
        }
    }
}