﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class KelasLatihanList
    {
        private List<KelasLatihan> mKelasLatihan = new List<KelasLatihan>();

        public KelasLatihanList()
        {
        }

        public int Count
        {
            get { return mKelasLatihan.Count; }
        }

        // Indexer (read only) for accessing a photo:
        public KelasLatihan this[int i]
        {
            get { return mKelasLatihan[i]; }
        }

        public async Task GenerateUserKelasAllAsync(string userId, DateTime date)
        {
            if(mKelasLatihan.Count > 0)
                mKelasLatihan.Clear();

            List<KelasLatihanModel> userKelasList = await MySilatKelas.GetLatihanByUserIdAndYear(userId, date);

            if (userKelasList != null)
            {
                foreach (var item in userKelasList)
                {
                    mKelasLatihan.Add(new KelasLatihan
                    {
                        Id = item.Id,
                        KelasId = item.KelasId,
                        KelasCode = item.KelasCode,
                        KelasName = item.KelasName,
                        KelasDescription = item.KelasDescription,
                        Days = item.Days,
                        StartTime = item.StartTime,
                        EndTime = item.EndTime,
                        Latitude = item.Latitude,
                        Longitude = item.Longitude
                    });
                }
            }
        }

        public async Task GenerateUserKelasTodayAsync(string userId, DateTime date)
        {
            if (mKelasLatihan.Count > 0)
                mKelasLatihan.Clear();

            List<KelasLatihanModel> userKelasLatihanList = await MySilatKelas.GetLatihanByUserIdAndYear(userId, date);

            if (userKelasLatihanList != null)
            {
                foreach (var item in userKelasLatihanList)
                {
                    List<int> days = item.Days.Split(',').Select(x => int.Parse(x)).ToList();

                    for (int i = 0; i < days.Count; i++)
                    {
                        var today = (int)date.DayOfWeek;

                        if (days[i] == today)
                        {
                            mKelasLatihan.Add(new KelasLatihan
                            {
                                Id = item.Id,
                                KelasId = item.KelasId,
                                KelasCode = item.KelasCode,
                                KelasName = item.KelasName,
                                KelasDescription = item.KelasDescription,
                                Days = "" + days[i],
                                StartTime = item.StartTime,
                                EndTime = item.EndTime,
                                Latitude = item.Latitude,
                                Longitude = item.Longitude
                            });
                        }
                    }
                }
            }
        }

        public async Task GenerateKelasLatihanAsync(string kelasId, DateTime date)
        {
            if (mKelasLatihan.Count > 0)
                mKelasLatihan.Clear();

            List<KelasLatihanModel> kelasLatihanList = await MySilatKelas.GetLatihanByKelasId(kelasId);

            if (kelasLatihanList != null)
            {
                foreach (var item in kelasLatihanList)
                {
                    mKelasLatihan.Add(new KelasLatihan
                    {
                        Id = item.Id,
                        KelasId = item.KelasId,
                        KelasCode = item.KelasCode,
                        KelasName = item.KelasName,
                        KelasDescription = item.KelasDescription,
                        Days = item.Days,
                        StartTime = item.StartTime,
                        EndTime = item.EndTime,
                        Latitude = item.Latitude,
                        Longitude = item.Longitude
                    });
                }
            }
        }
    }
}