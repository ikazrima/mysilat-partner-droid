﻿using Android.Support.V7.Widget;
using Android.Views;
using System;
using System.Collections.Generic;

namespace MySilatPartner
{
    class KelasLatihanAdapter : RecyclerView.Adapter
    {
        private Callback mCallBack;

        public event EventHandler<int> ItemClick;

        public KelasLatihanList mKelasList;

        public bool showKelasCode = false;
        public bool showDays = true;
        public bool showAttendanceSwitch = false;

        public KelasLatihanAdapter(KelasLatihanList kelasList, Callback callback)
        {
            mKelasList = kelasList;
            mCallBack = callback;

        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.kelas_latihan_item_row, parent, false);

            KelasLatihanViewHolder vh = new KelasLatihanViewHolder(itemView);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            KelasLatihanViewHolder vh = holder as KelasLatihanViewHolder;

            vh.kelasCode.Text = mKelasList[position].KelasCode;
            vh.description.Text =  mKelasList[position].KelasDescription;
            vh.time.Text = string.Format("{0:hh:mm tt}", mKelasList[position].StartTime.Value)
                + " - " + string.Format("{0:hh:mm tt}", mKelasList[position].EndTime.Value);

            if (mCallBack != null)
            {
                vh.btnMore.Visibility = ViewStates.Visible;
                vh.btnMore.Click += new EventHandler((s, args) => { mCallBack.StartEditKelasLatihan(position); });
            }
            else
            {
                vh.btnMore.Visibility = ViewStates.Gone;
            }

            string[] daysArray = mKelasList[position].Days.Split(',');
            List<string> daysList = new List<string>(daysArray.Length);
            daysList.AddRange(daysArray);
            daysList.Reverse();

            string daysText = "";

            for(int i = 0; i <daysArray.Length; i++)
            {
                string day = "";

                if (string.Compare(daysArray[i], "1") == 0)
                    day = vh.days.Context.GetString(Resource.String.monday);
                if (string.Compare(daysArray[i], "2") == 0)
                    day = vh.days.Context.GetString(Resource.String.tuesday);
                if (string.Compare(daysArray[i], "3") == 0)
                    day = vh.days.Context.GetString(Resource.String.wednesday);
                if (string.Compare(daysArray[i], "4") == 0)
                    day = vh.days.Context.GetString(Resource.String.thursday);
                if (string.Compare(daysArray[i], "5") == 0)
                    day = vh.days.Context.GetString(Resource.String.friday);
                if (string.Compare(daysArray[i], "6") == 0)
                    day = vh.days.Context.GetString(Resource.String.saturday);
                if (string.Compare(daysArray[i], "7") == 0)
                    day = vh.days.Context.GetString(Resource.String.sunday);

                daysText += day;

                if (i == 0 && daysArray.Length > 1)
                    daysText += ", ";

                if (i >= 1 && i < daysArray.Length - 1)
                    daysText += ", ";
            }

            vh.days.Text = daysText;

            if (!showKelasCode)
                vh.kelasCode.Visibility = ViewStates.Gone;
            else
                vh.kelasCode.Visibility = ViewStates.Visible;

            if (!showDays)
                vh.days.Visibility = ViewStates.Gone;
            else
                vh.days.Visibility = ViewStates.Visible;

            if (!showAttendanceSwitch)
                vh.kelasAttendace.Visibility = ViewStates.Gone;
            else
                vh.kelasAttendace.Visibility = ViewStates.Visible;
        }

        public override int ItemCount
        {
            get { return mKelasList.Count; }
        }

        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }

        public interface Callback
        {
            void StartEditKelasLatihan(int position);
        }
    }
}