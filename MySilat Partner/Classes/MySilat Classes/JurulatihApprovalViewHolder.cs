﻿using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System;

namespace MySilatPartner
{
    class JurulatihApprovalViewHolder : RecyclerView.ViewHolder
    {
        public View ItemSelected { get; set; }
        public TextView UserName { get; set; }
        public TextView KelasCode { get; set; }
        public TextView BuahName { get; set; }
        public TextView Peringkat { get; set; }
        public TextView ReceivedDt { get; set; }
        public ImageView ProfileImg { get; set; }
        public ImageView ProfileImgPlaceholder { get; set; }

        public JurulatihApprovalViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            ItemSelected = itemView.FindViewById(Resource.Id.shape_item_selected);
            UserName = (TextView)itemView.FindViewById(Resource.Id.user_name);
            KelasCode = (TextView)itemView.FindViewById(Resource.Id.kelas_code);
            BuahName = (TextView)itemView.FindViewById(Resource.Id.buah_name);
            Peringkat = (TextView)itemView.FindViewById(Resource.Id.peringkat);
            ReceivedDt = (TextView)itemView.FindViewById(Resource.Id.received_dt);
            ProfileImg = (ImageView)itemView.FindViewById(Resource.Id.profile_image);
            ProfileImgPlaceholder = (ImageView)itemView.FindViewById(Resource.Id.profile_image_placeholder);

            itemView.Click += (sender, e) => listener(base.Position);
        }
    }
}