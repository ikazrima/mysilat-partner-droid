﻿using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;

namespace MySilatPartner
{
    class TauliahRecyclerAdapter : RecyclerView.Adapter
    {
        public bool displayJawatanCard = false;

        public event EventHandler<int> ItemClick;

        public TauliahList mTauliahList;
        List<Tauliah> tauliahCopy;

        public TauliahRecyclerAdapter(TauliahList tauliahList)
        {
            mTauliahList = tauliahList;
            tauliahCopy = new List<Tauliah>(tauliahList.mTauliah);
        }

        public void Filter(string text)
        {
            mTauliahList.mTauliah.Clear();

            if (string.IsNullOrWhiteSpace(text))
            {
                mTauliahList.mTauliah = new List<Tauliah>(tauliahCopy);
            }
            else
            {
                text = text.ToLower();

                foreach (var item in tauliahCopy)
                {
                    if (item.KelasCode.ToLower().Contains(text)
                        || item.KelasName.ToLower().Contains(text))
                    {
                        mTauliahList.mTauliah.Add(item);
                    }
                }
            }

            NotifyDataSetChanged();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.tauliah_item_row, parent, false);

            TauliahViewHolder vh = new TauliahViewHolder(itemView, OnClick);
            return vh;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            TauliahViewHolder vh = holder as TauliahViewHolder;

            switch(mTauliahList[position].KelasType)
            {
                // IPT
                case "1":
                    vh.Icon.SetImageDrawable(ContextCompat.GetDrawable(vh.Icon.Context, Resource.Drawable.ic_graduate_cap_white_24dp));
                    break;
                // Sekolah
                case "2":
                    vh.Icon.SetImageDrawable(ContextCompat.GetDrawable(vh.Icon.Context, Resource.Drawable.ic_classroom_white_24dp));
                    break;
                // Awam
                case "3":
                    vh.Icon.SetImageDrawable(ContextCompat.GetDrawable(vh.Icon.Context, Resource.Drawable.ic_team_white_24dp));
                    break;
            }

            vh.KelasName.Text = mTauliahList[position].KelasName;
            vh.KelasCode.Text = mTauliahList[position].KelasCode;
            vh.Year.Text = mTauliahList[position].Year;

            if (!displayJawatanCard)
            {
                vh.JawatanCard.Visibility = ViewStates.Gone;
            }
            else
            {
                vh.JawatanCard.Visibility = ViewStates.Visible;
                vh.TauliahCode.Text = mTauliahList[position].Code;

                /*
                switch(mTauliahList[position].Code)
                {
                    default:
                    case "AB":
                    case "PK":
                        vh.TauliahCode.SetTextColor(Android.Graphics.Color.Black);
                        vh.TauliahCode.SetBackgroundColor(Android.Graphics.Color.White);
                        break;
                    case "PJP":
                        vh.TauliahCode.SetTextColor(Android.Graphics.Color.Black);
                        vh.TauliahCode.SetBackgroundColor(Android.Graphics.Color.White);
                        break;
                    case "PJ":
                    case "J":
                    case "PP":
                    case "P":
                        vh.TauliahCode.SetTextColor(Android.Graphics.Color.White);
                        vh.TauliahCode.SetBackgroundColor(Android.Graphics.Color.Red);
                        break;
                    case "GU":
                        vh.TauliahCode.SetTextColor(Android.Graphics.Color.Black);
                        vh.TauliahCode.SetBackgroundColor(Android.Graphics.Color.Yellow);
                        break;
                }*/
            }

            vh.KelasName.Selected = true;
            vh.KelasName.Ellipsize = TextUtils.TruncateAt.Marquee;
            vh.KelasName.SetSingleLine(true);
            vh.KelasName.SetMarqueeRepeatLimit(-1);
        }

        public override int ItemCount
        {
            get { return mTauliahList.Count; }
        }

        //This will fire any event handlers that are registered with our ItemClick
        //event.
        void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }
    }
}