﻿using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySilatPartner
{
    class KelasMemberList
    {
        public List<UserProfile> mUserProfile = new List<UserProfile>();

        public KelasMemberList()
        {
        }

        public int Count
        {
            get { return mUserProfile.Count; }
        }

        // Indexer (read only) for accessing a photo:
        public UserProfile this[int i]
        {
            get { return mUserProfile[i]; }
        }

        public async Task GenerateMemberByKelasIdYearTauliahIdAsync(string kelasId, DateTime year, string tauliahIdJson)
        {
            if (mUserProfile.Count > 0)
                mUserProfile.Clear();

            List<UserProfileModel> result = await MySilatKelas.GetMemberByKelasIdAndYear(kelasId, year, tauliahIdJson);

            if (result != null)
            {
                foreach (var item in result)
                {
                    mUserProfile.Add(new UserProfile(item.ProfileId, item.Sex, item.FirstName, item.LastName, item.AlternateName,
                        item.PeringkatId, item.Peringkat, item.TauliahId, item.JawatanCode, item.Jawatan, item.FacebookId,
                        item.ProfileImgId, item.ProfileImgName, item.ProfileImgUri,
                        item.Year));
                }
            }
        }
    }
}