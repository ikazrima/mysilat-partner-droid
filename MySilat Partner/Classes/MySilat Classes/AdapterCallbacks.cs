﻿namespace MySilatPartner.MyCallbacks
{
    public class JurulatihApprovalCallback
    {
        public interface IAdapterCallback
        {
            void OnApprovalSelectedChange(int count);
        }
    }

    public class KelasMemberCallback
    {
        public interface IAdapterCallback
        {
            void OnMemberClick(int position);
        }
    }

    public class UserBuahCallback
    {
        public interface IAdapterCallback
        {
            void OnUserBuahSelectedChange(int count);
            void OnTenagaPengajarClick(int position);
        }
    }

    public class UserKelasCallback
    {
        public interface IAdapterCallback
        {
            void OnKelasClick(int position);
        }
    }
}