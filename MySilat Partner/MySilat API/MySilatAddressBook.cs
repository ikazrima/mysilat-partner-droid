﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    class MySilatAddressBook
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static AddressBookModel UserAddressBook;

        public static async Task<AddressBookModel> GetAddressBookByID(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/id/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<AddressBookModel> addressBook = JsonConvert.DeserializeObject<List<AddressBookModel>>(genericObject.Data.ToString(), settings);

                    return addressBook[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<AddressBookModel> GetAddressBookByOwner(string ownerId, string type)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/owner_type");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["owner_id"] = ownerId;
            parameters["type"] = type;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<AddressBookModel> addressBook = JsonConvert.DeserializeObject<List<AddressBookModel>>(genericObject.Data.ToString(), settings);

                    return addressBook[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> AddAddressBook(string ownerId, string type)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/add");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["owner_id"] = ownerId;
            parameters["type"] = type;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateAddress(string id, string line1, string line2, string postcode, string stateId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/update_address");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["line1"] = line1;
            parameters["line2"] = line2;
            parameters["postcode"] = postcode;
            parameters["state_id"] = stateId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdatePhone(string id, string contactFixed, string contactMobile)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/update_phone");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["home_phone"] = contactFixed;
            parameters["mobile_phone"] = contactMobile;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<List<AddressStateModel>> GetAllAddressStates()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "address/states/all");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<AddressStateModel> addressBook = JsonConvert.DeserializeObject<List<AddressStateModel>>(genericObject.Data.ToString(), settings);

                    return addressBook;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

    }
}