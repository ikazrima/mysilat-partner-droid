﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    public class MySilatTauliah
    {
        public static async Task<List<TauliahDetailedModel>> GetAllTauliahKelasYear(string kelasId, DateTime year)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/tauliah_kelas_year");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["kelas_id"] = "" + kelasId;
            parameters["year"] = year.ToString("yyyy");

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<TauliahDetailedModel> tauliah = JsonConvert.DeserializeObject<List<TauliahDetailedModel>>(genericObject.Data.ToString());

                    return tauliah;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<TauliahModel>> GetTauliahUser(string userId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/tauliah_user");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<TauliahModel> tauliah = JsonConvert.DeserializeObject<List<TauliahModel>>(genericObject.Data.ToString());

                    return tauliah;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<TauliahModel>> GetTauliahUserYear(string userId, string year)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/tauliah_user_year");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["year"] = year;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<TauliahModel> tauliah = JsonConvert.DeserializeObject<List<TauliahModel>>(genericObject.Data.ToString());

                    return tauliah;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }
    }
}