﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    class MySilatAccount
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static async Task<bool> LoginByEmail(string email, string password)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "account/login/email");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["email"] = email;
            parameters["password"] = password;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                    return true;
                else
                    return false;
            }

            // Fail / Error
            else
                return false;
        }

        public static async Task<bool> LoginByIdentification(string identification, string password)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "account/login/identification");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["identification"] = identification;
            parameters["password"] = password;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                    return true;
                else
                    return false;
            }

            // Fail / Error
            else
                return false;
        }

        public static async Task<string> RegisterUser(string email, string password, string identification, string first_name, string last_name, string alternate_name)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "account/register");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["email"] = identification;
            parameters["password"] = identification;
            parameters["identification"] = identification;
            parameters["first_name"] = first_name;
            parameters["last_name"] = last_name;
            parameters["alternate_name"] = alternate_name;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }
    }
}