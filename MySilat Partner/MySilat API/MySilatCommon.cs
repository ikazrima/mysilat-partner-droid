﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Net;
using Android.Provider;
using Android.Util;
using Java.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace MySilatPartner
{
    class MySilatCommon
    {
        public static string APIPath = "https://www.atherons.com/mysilat/api/";

        // Response status
        public const string SUCCESS = "success";
        public const string FAIL = "fail";
        public const string ERROR = "error";

        public const string OWNER_USER = "1";
        public const string OWNER_KELAS = "2";

        public const int ONE_SECOND = 1 * 1000;
        public const int FIVE_SECONDS = 5 * 1000;
        public const int TEN_SECONDS = 10 * 1000;
        public const int ONE_MINUTE = 60 * 1000;
        public const long FIVE_MINUTES = 5 * ONE_MINUTE;
        public const long TWO_MINUTES = 2 * ONE_MINUTE;

        public static readonly int RC_LAST_LOCATION_PERMISSION_CHECK = 1000;
        public static readonly int RC_LOCATION_UPDATES_PERMISSION_CHECK = 1100;
        public static readonly int RC_WRITE_EXTERNAL_PERMISSION_CHECK = 1200;

        public static readonly string IMAGE_LARGE = "large";
        public static readonly string IMAGE_NORMAL = "normal";
        public static readonly string IMAGE_SMALL = "small";

        static readonly string KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates";

        public const int REQUEST_CODE_READ_PHONE_STATE = 0;

        public static DateTime FromUnixLocalTime(long unixTime)
        {
            return epoch.AddMilliseconds(unixTime).ToLocalTime();
        }
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static readonly string stdDtFormat = "yyyy-MM-dd hh:mm:ss";

        public static int PixelsToDP(Context context, int pixels)
        {
            float scale = context.Resources.DisplayMetrics.Density;
            pixels = (int)(pixels * scale + 0.5f);

            return pixels;
        }

        public static int CalculateNoOfColumns(Context context, float columnWidthDp)
        { // For example columnWidthdp=180
            DisplayMetrics displayMetrics = context.Resources.DisplayMetrics;
            float screenWidthDp = displayMetrics.WidthPixels / displayMetrics.Density;
            int noOfColumns = (int)(screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
            return noOfColumns;
        }

        public static void DialPhone(string phoneNumber, Context context)
        {
            Intent intent = new Intent(Intent.ActionDial, Android.Net.Uri.FromParts("tel", phoneNumber, null));
            context.StartActivity(intent);
        }

        public static void SendWhatsapp(string phoneNumber, string text, Context context)
        {
            String url = "https://api.whatsapp.com/send?phone=" + phoneNumber;
            Intent intent = new Intent(Intent.ActionView);
            intent.SetData(Android.Net.Uri.Parse(url));
            context.StartActivity(intent);
        }

        public static string GetMeid(Context context)
        {
            Android.Telephony.TelephonyManager mTelephonyMgr;
            mTelephonyMgr = (Android.Telephony.TelephonyManager)context.GetSystemService(Context.TelephonyService);
            return mTelephonyMgr.Meid;
        }

        public static string GetImei(Context context)
        {
            Android.Telephony.TelephonyManager mTelephonyMgr;
            mTelephonyMgr = (Android.Telephony.TelephonyManager)context.GetSystemService(Context.TelephonyService);
            return mTelephonyMgr.Imei;
        }

        // Convert an object to a byte array
        public static byte[] ObjectToByteArray(Object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        // Convert a byte array to an Object
        public static Object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        public static Android.Net.Uri GetImageUri(Context inContext, Bitmap inImage)
        {
            MemoryStream bytes = new MemoryStream();
            inImage.Compress(Bitmap.CompressFormat.Jpeg, 100, bytes);
            string filePath = MediaStore.Images.Media.InsertImage(inContext.ContentResolver, inImage, "Title", null);

            ContentValues values = new ContentValues();

            values.Put(MediaStore.Images.Media.InterfaceConsts.DateTaken, System.DateTime.Now.ToString());
            values.Put(MediaStore.Images.Media.InterfaceConsts.MimeType, "image/jpeg");
            values.Put(MediaStore.MediaColumns.Data, filePath);

            Android.Net.Uri uri = inContext.ContentResolver.Insert(MediaStore.Images.Media.ExternalContentUri, values);

            return uri;

        }

        public static bool IsValidEmailAddress(string emailAddress)
        {
            try
            {
                Regex rx = new Regex(
                @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
                return rx.IsMatch(emailAddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}