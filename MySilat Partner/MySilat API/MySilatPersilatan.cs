﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    public class MySilatPersilatan
    {
        public static UserPersilatanModel UserPersilatan;

        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static async Task<UserPersilatanModel> GetPersilatanByUserId(string userId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/user_id/" + userId);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    //var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = MySilatCommon.stdDtFormat };
                    List<UserPersilatanModel> persilatan = JsonConvert.DeserializeObject<List<UserPersilatanModel>>(genericObject.Data.ToString(), settings);

                    return persilatan[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> AddUserPersilatan(string userId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/add");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateUserKelasUtama(string userId, string kelasId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_kelas");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["main_kelas_id"] = kelasId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateUserPeringkat(string userId, string peringkatId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_peringkat");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["peringkat_id"] = peringkatId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateIjazahAsas(string userId, DateTime date)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_asas");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            string dateStr = date.ToString(MySilatCommon.stdDtFormat);

            parameters["user_id"] = userId;
            parameters["date"] = dateStr;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateIjazahPotong(string userId, DateTime date)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_potong");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            string dateStr = date.ToString(MySilatCommon.stdDtFormat);

            parameters["user_id"] = userId;
            parameters["date"] = dateStr;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateIjazahTamat(string userId, DateTime date)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_tamat");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            string dateStr = date.ToString(MySilatCommon.stdDtFormat);

            parameters["user_id"] = userId;
            parameters["date"] = dateStr;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateSRP(string userId, DateTime date)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_srp");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            string dateStr = date.ToString(MySilatCommon.stdDtFormat);

            parameters["user_id"] = userId;
            parameters["date"] = dateStr;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateSMP(string userId, DateTime date)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/silat/update_smp");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            string dateStr = date.ToString(MySilatCommon.stdDtFormat);

            parameters["user_id"] = userId;
            parameters["date"] = dateStr;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }
    }
}