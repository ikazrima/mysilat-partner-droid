﻿using MySilatPartner.JsonModel;
using MySilatPartner.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    public class MySilatBuah
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static async Task<List<BuahModel>> GetBuahAll()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/all");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuah21()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/21");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuahSilibus()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/silibus");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuahAsas()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/asas");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuahJatuh()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/jatuh");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuahPotong()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/potong");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<BuahModel>> GetBuahTamat()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/tamat");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<BuahModel> buahList = JsonConvert.DeserializeObject<List<BuahModel>>(genericObject.Data.ToString(), settings);

                    return buahList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<UserBuahModel>> GetUserBuah(string userId, int peringkatId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/buah");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["peringkat_id"] = "" + peringkatId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserBuahRootModel> userBuahRoot = JsonConvert.DeserializeObject<List<UserBuahRootModel>>(genericObject.Data.ToString(), settings);

                    if (userBuahRoot[0].BuahJson != null)
                    {
                        var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = MySilatCommon.stdDtFormat };
                        List<UserBuahModel> userBuah = JsonConvert.DeserializeObject<List<UserBuahModel>>(userBuahRoot[0].BuahJson, dateTimeConverter);

                        if(userBuah == null)
                            return null;

                        else if (userBuah.Count > 0)
                            return userBuah;

                        else
                            return null;
                    }

                    return null;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> SetUserBuah(string userId, int peringkatId, string buahJson)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/buah/set");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["peringkat_id"] = "" + peringkatId;
            parameters["buah_json"] = buahJson;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task SaveUserBuah(BuahAdapter buahAdapter, int peringkatId)
        {
            List<Dictionary<string, string>> checkList = new List<Dictionary<string, string>>();

            for (int i = 0; i < buahAdapter.Count; i++)
            {
                Dictionary<string, string> userBuah = new Dictionary<string, string>();

                var item = (buahAdapter[i] as Buah);

                string buah_id = item.BuahId;
                string kelas_id = "" + item.KelasId;
                string jurulatih_id = "" + item.JurulatihId;

                string received = "0";
                if (item.Received == true)
                    received = "1";

                string received_dt = "";

                if (item.ReceivedDt.HasValue)
                    received_dt = item.ReceivedDt.Value.ToString(MySilatCommon.stdDtFormat);

                string verified_by = "" + item.VerifiedBy;
                string verified_dt = item.VerifiedDt.ToString(MySilatCommon.stdDtFormat);

                userBuah.Add("buah_id", buah_id);
                userBuah.Add("kelas_id", kelas_id);
                userBuah.Add("jurulatih_id", jurulatih_id);
                userBuah.Add("received", received);
                userBuah.Add("received_dt", received_dt);
                userBuah.Add("verified_by", verified_by);
                userBuah.Add("verified_dt", verified_dt);

                checkList.Add(userBuah);
            }

            string json = JsonConvert.SerializeObject(checkList, Formatting.None);

            var result = await MySilatBuah.SetUserBuah(MySilatProfile.UserProfile.ProfileId, peringkatId, json);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);
        }

        #region Jurulatih approval
        public static async Task<List<JurulatihApprovalModel>> GetJurulatihApproval(string userId, DateTime maxYear)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/get_jurulatih_approval");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["jurulatih_id"] = userId;
            parameters["year"] = "" + maxYear.Year;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<JurulatihApprovalModel> jurulatihBuahRoot = JsonConvert.DeserializeObject<List<JurulatihApprovalModel>>(genericObject.Data.ToString(), settings);

                    return jurulatihBuahRoot;

                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }
        
        public static async Task<string> SetJurulatihApproval(string jurulatihId, string approvalJson, int approvalStatus)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "silat/buah/set_jurulatih_approval");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["jurulatih_id"] = jurulatihId;
            parameters["approval_json"] = approvalJson;
            parameters["status"] = "" + approvalStatus;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task SaveJurulatihApproval(List<int> userBuahId, List<string> buahId, string jurulatihId, int approvalStatus)
        {
            List<Dictionary<string, string>> checkList = new List<Dictionary<string, string>>();

            for (int i = 0; i < userBuahId.Count; i++)
            {
                Dictionary<string, string> approval = new Dictionary<string, string>();

                int user_buah_id = userBuahId[i];
                string buah_id = buahId[i];

                approval.Add("user_buah_id", user_buah_id + "");
                approval.Add("buah_id", buah_id);

                checkList.Add(approval);
            }

            string json = JsonConvert.SerializeObject(checkList, Formatting.None);

            var result = await MySilatBuah.SetJurulatihApproval(jurulatihId, json, approvalStatus);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);
        }
        #endregion
    }
}
