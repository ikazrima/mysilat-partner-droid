﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    class MySilatSocial
    {
        public static SocialModel UserSocial;

        public static async Task<SocialModel> GetSocialByIDAsync(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/id/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<SocialModel> social = JsonConvert.DeserializeObject<List<SocialModel>>(genericObject.Data.ToString());

                    return social[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<SocialModel> GetSocialByFBAsync(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/fb/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<SocialModel> social = JsonConvert.DeserializeObject<List<SocialModel>>(genericObject.Data.ToString());

                    return social[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<SocialModel> GetSocialByTwitterAsync(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/twitter/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<SocialModel> social = JsonConvert.DeserializeObject<List<SocialModel>>(genericObject.Data.ToString());

                    return social[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<SocialModel> GetSocialByGoogleAsync(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/google/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<SocialModel> social = JsonConvert.DeserializeObject<List<SocialModel>>(genericObject.Data.ToString());

                    return social[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<SocialModel> GetSocialByOwnerAsync(string ownerId, string ownerType)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/getowner");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["owner_id"] = ownerId;
            parameters["owner_type"] = ownerType;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<SocialModel> social = JsonConvert.DeserializeObject<List<SocialModel>>(genericObject.Data.ToString());

                    return social[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> AddSocialAsync(string ownerId, string type)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/add");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["owner_id"] = ownerId;
            parameters["type"] = type;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateFBAsync(string id, string facebookId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/fb_update");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["facebook_id"] = facebookId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateWebEmailAsync(string id, string website, string emailAlt)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/webemail_update");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["website"] = website;
            parameters["email_alt"] = emailAlt;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }
    }
}