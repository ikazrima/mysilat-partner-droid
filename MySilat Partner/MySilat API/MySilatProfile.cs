﻿using Android.Widget;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    class MySilatProfile
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static UserProfileModel UserProfile;

        public static async Task<Android.Net.Uri> GetUserProfileImageUri(UserProfileModel userProfile, string fbImgSize)
        {
            Android.Net.Uri uri;
            uri = Android.Net.Uri.Empty;

            int profileImgId = 0;
            int.TryParse(userProfile.ProfileImgId, out profileImgId);

            if (profileImgId > 0)
            {
                MyImageModel myImage = await GetProfileImageAsync(userProfile.ProfileId);

                if (myImage != null)
                    uri = Android.Net.Uri.Parse(myImage.ImgPath);
            }

            else if (!string.IsNullOrEmpty(MySilatSocial.UserSocial.FacebookId))
            {
                SocialModel socialModel = await MySilatSocial.GetSocialByOwnerAsync(userProfile.ProfileId, MySilatCommon.OWNER_USER);

                if (socialModel != null)
                {
                    string path = "http://graph.facebook.com/" + socialModel.FacebookId + "/picture?type=" + fbImgSize;
                    uri = Android.Net.Uri.Parse(path);
                }
            }

            return uri;
        }

        public static Android.Net.Uri GetUserProfileImageUri(string imgPath)
        {
            Android.Net.Uri uri;
            uri = Android.Net.Uri.Parse(imgPath);

            return uri;
        }

        public static Android.Net.Uri GetUserProfileImageUri(string facebookId, string fbImgSize)
        {
            Android.Net.Uri uri;
            uri = Android.Net.Uri.Empty;
            string path = "http://graph.facebook.com/" + facebookId + "/picture?type=" + fbImgSize;
            uri = Android.Net.Uri.Parse(path);

            return uri;
        }

        public static async Task<int> CheckFBExistAsync(string fbid)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "socmed/fb/" + fbid);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                    return 1;
                else
                    return 0;
            }

            // Fail / Error
            else
                return -1;
        }

        public static async Task<int> CheckICExistAsync(string identification)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/ic/" + identification);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                    return 1;
                else
                    return 0;
            }

            // Fail / Error
            else
                return -1;
        }

        public static async Task<UserProfileModel> GetProfileByIDAsync(string id)
        {           
            try
            {
                var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/id/" + id);

                var httpClient = new HttpClient();

                Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

                string contents = await contentsTask;

                GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

                if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
                {
                    // Record exist
                    if (genericObject.Data != null)
                    {
                        List<UserProfileModel> profile = JsonConvert.DeserializeObject<List<UserProfileModel>>(genericObject.Data.ToString(), settings);

                        return profile[0];
                    }
                    else
                        return null;
                }

                // Fail / Error
                else
                    return null;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static async Task<UserProfileModel> GetProfileByIdentificationAsync(string identification)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/ic/" + identification);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserProfileModel> profile = JsonConvert.DeserializeObject<List<UserProfileModel>>(genericObject.Data.ToString(), settings);

                    return profile[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<UserProfileModel> GetProfileByEmailAsync(string email)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/email");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["email"] = email;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserProfileModel> profile = JsonConvert.DeserializeObject<List<UserProfileModel>>(genericObject.Data.ToString(), settings);

                    return profile[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }


        public static async Task<UserProfileModel> GetProfileByFBIDAsync(string fbId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/fb/" + fbId);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserProfileModel> profile = JsonConvert.DeserializeObject<List<UserProfileModel>>(genericObject.Data.ToString(), settings);

                    return profile[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<MyImageModel> GetProfileImageAsync(string userId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "image/profile/userId/" + userId);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<MyImageModel> myImage = JsonConvert.DeserializeObject<List<MyImageModel>>(genericObject.Data.ToString(), settings);

                    return myImage[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> AddProfileAsync(string identification, string first_name, string last_name, string alternate_name)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/add");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["identification"] = identification;
            parameters["first_name"] = first_name;
            parameters["last_name"] = last_name;
            parameters["alternate_name"] = alternate_name;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> AddProfileImageAsync(string ownerId, string imgName, string imgPath)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "image/profile/save");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["owner_id"] = ownerId;
            parameters["img_name"] = imgName;
            parameters["img_path"] = imgPath;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateProfileNamesAsync(string id, string first_name, string last_name, string alternate_name)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/update_names");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["first_name"] = first_name;
            parameters["last_name"] = last_name;
            parameters["alternate_name"] = alternate_name;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<UserProfileIC> GetProfileICAsync(string id)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "user/profile/identification/" + id);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserProfileIC> identification = JsonConvert.DeserializeObject<List<UserProfileIC>>(genericObject.Data.ToString(), settings);

                    return identification[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static bool IsIdentificationValid(string identification)
        {
            Regex regex = new Regex("^[0-9]{12}$");

            if (regex.IsMatch(identification))
                return true;
            else
                return false;
        }

        public static string FormattedName(string text)
        {
            return Regex.Replace(text, @"\s+", " ").Trim();
        }
    }
}