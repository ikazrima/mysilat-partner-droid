﻿using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MySilatPartner.API
{
    class MySilatKelas
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public static async Task<KelasDetailsModel> GetDetailsById(string kelasId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/id/" + kelasId);

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<KelasDetailsModel> result = JsonConvert.DeserializeObject<List<KelasDetailsModel>>(genericObject.Data.ToString(), settings);

                    return result[0];
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<KelasLatihanModel>> GetLatihanByKelasId(string kelasId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/latihan/kelas_id");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["kelas_id"] = kelasId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<KelasLatihanModel> result = JsonConvert.DeserializeObject<List<KelasLatihanModel>>(genericObject.Data.ToString(), settings);

                    return result;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<KelasLatihanModel>> GetLatihanByUserIdAndYear(string userId, DateTime year)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/latihan/userId_year");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["user_id"] = userId;
            parameters["year"] = year.ToString("yyyy");

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<KelasLatihanModel> result = JsonConvert.DeserializeObject<List<KelasLatihanModel>>(genericObject.Data.ToString(), settings);

                    return result;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<UserProfileModel>> GetMemberByKelasIdAndYear(string kelasId, DateTime year, string tauliahIdJson)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/members_by_year");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["kelas_id"] = kelasId;
            parameters["year"] = year.ToString("yyyy");
            parameters["tauliah_id"] = tauliahIdJson;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<UserProfileModel> result = JsonConvert.DeserializeObject<List<UserProfileModel>>(genericObject.Data.ToString(), settings);

                    return result;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<List<KelasTypeModel>> GetAllKelasType()
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/type/all");

            var httpClient = new HttpClient();

            Task<string> contentsTask = httpClient.GetStringAsync(uri.ToString());

            string contents = await contentsTask;

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(contents, settings);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    List<KelasTypeModel> kelasTypeList = JsonConvert.DeserializeObject<List<KelasTypeModel>>(genericObject.Data.ToString(), settings);

                    return kelasTypeList;
                }
                else
                    return null;
            }

            // Fail / Error
            else
                return null;
        }

        public static async Task<string> AddKelasLatihan(string kelasId, string description, DateTime startTime, DateTime endTime, string days)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/latihan/add");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["kelas_id"] = kelasId;
            parameters["description"] = description;
            parameters["start_time"] = startTime.ToString("HH:mm:ss");
            parameters["end_time"] = endTime.ToString("HH:mm:ss");
            parameters["days"] = days;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateKelasDetails(string kelasId, string name, int type, int stateId)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/update");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["kelas_id"] = kelasId;
            parameters["name"] = name;
            parameters["type"] = "" + type;
            parameters["state_id"] = "" + stateId;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> UpdateKelasLatihan(string id, string description, DateTime startTime, DateTime endTime, string days)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/latihan/update");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["description"] = description;
            parameters["start_time"] = startTime.ToString("HH:mm:ss");
            parameters["end_time"] = endTime.ToString("HH:mm:ss");
            parameters["days"] = days;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }

        public static async Task<string> ActivateKelasLatihan(string id, int active)
        {
            var uri = Android.Net.Uri.Parse(MySilatCommon.APIPath + "kelas/latihan/activate");

            var httpClient = new HttpClient();

            var parameters = new Dictionary<string, string>();

            parameters["id"] = id;
            parameters["active"] = "" + active;

            var response = await httpClient.PostAsync(uri.ToString(), new FormUrlEncodedContent(parameters));
            var contents = await response.Content.ReadAsStringAsync();

            return contents;
        }
    }
}