﻿using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Com.Theartofdev.Edmodo.Cropper;
using Firebase.Storage;
using MySilatPartner.Activity;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using Square.Picasso;
using System;
using System.Threading.Tasks;
using v4App = Android.Support.V4.App;

namespace MySilatPartner.Fragments
{
    public class UserProfileFragment : v4App.Fragment, IOnSuccessListener, IOnFailureListener, IOnProgressListener
    {
        #region Profile Image
        StorageReference profileImageRef;
        string userType;
        UserProfileModel currentProfile { get; set; }
        MyImageModel myImage { get; set; }
        string uploadFileName;

        int thumbSize = 512;
        int resizeDim = 512;

        Android.Gms.Tasks.Task downloadTask;
        Android.Gms.Tasks.Task uploadTask;
        Android.Net.Uri resultUri;
        #endregion

        Snackbar snackbar;

        #region UI
        View rootView;

        ProgressBar progressBar { get; set; }

        Button btnSave { get; set; }
        Button btnCancel { get; set; }
        ImageButton btnCamera { get; set; }
        ImageView imgProfile { get; set; }

        View btnCameraOverlay { get; set; }
        ImageView imgProfileOverlay { get; set; }

        ConstraintLayout lytProfile { get; set; }

        TextInputLayout tilFullName { get; set; }
        TextInputLayout tilLastName { get; set; }
        TextInputLayout tilAltName { get; set; }

        TextInputEditText tieFullName { get; set; }
        TextInputEditText tieLastName { get; set; }
        TextInputEditText tieAltName { get; set; }

        TextInputLayout tilIC { get; set; }
        TextInputEditText tieIC { get; set; }

        Button btnIC { get; set; }
        #endregion

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            rootView = inflater.Inflate(Resource.Layout.user_profile_edit, container, false);

            progressBar = (ProgressBar)rootView.FindViewById(Resource.Id.progressBar);

            btnCamera = (ImageButton)rootView.FindViewById(Resource.Id.btn_camera);
            imgProfile = (ImageView)rootView.FindViewById(Resource.Id.profile_image);

            btnCameraOverlay = rootView.FindViewById(Resource.Id.btn_camera_overlay);
            imgProfileOverlay = (ImageView)rootView.FindViewById(Resource.Id.profile_image_overlay);

            lytProfile = (ConstraintLayout)rootView.FindViewById(Resource.Id.lyt_profile);
            btnSave = (Button)rootView.FindViewById(Resource.Id.btn_save);
            btnCancel = (Button)rootView.FindViewById(Resource.Id.btn_cancel);

            tilFullName = (TextInputLayout)rootView.FindViewById(Resource.Id.til_full_name);
            tilLastName = (TextInputLayout)rootView.FindViewById(Resource.Id.til_last_name);
            tilAltName = (TextInputLayout)rootView.FindViewById(Resource.Id.til_alt_name);

            tieFullName = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_full_name);
            tieLastName = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_last_name);
            tieAltName = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_alt_name);

            tilIC = (TextInputLayout)rootView.FindViewById(Resource.Id.til_ic_number);
            tieIC = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_ic_number);
            btnIC = (Button)rootView.FindViewById(Resource.Id.btn_ic);

            imgProfileOverlay.Visibility = ViewStates.Gone;
            progressBar.Visibility = ViewStates.Gone;

            btnSave.Click += BtnSave_Click;
            btnCancel.Click += BtnCancel_Click;
            btnCamera.Click += BtnCamera_Click;
            btnIC.Click += BtnIC_Click;

            return rootView;
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            await GetIntent();

            if (currentProfile != null)
            {
                tieFullName.Text = currentProfile.FirstName;
                tieLastName.Text = currentProfile.LastName;
                tieAltName.Text = currentProfile.AlternateName;

                /*
                UserProfileIC profile = await MySilatProfile.GetProfileICAsync(currentProfile.ProfileId);
                tieIC.Text = profile.Identification;
                */
            }

            tieFullName.TextChanged += ProfileTextChanged;
            tieLastName.TextChanged += ProfileTextChanged;
            tieAltName.TextChanged += ProfileTextChanged;
        }

        public override void OnPause()
        {
            base.OnPause();

            CleanUpTasks();
        }

        public override void OnResume()
        {
            base.OnResume();

            if (currentProfile != null)
                DownloadProfileImage(currentProfile);
            else
            {
                progressBar.Visibility = ViewStates.Gone;
                NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
            }
        }

        public override void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == CropImage.CropImageActivityRequestCode)
            {
                CropImage.ActivityResult result = CropImage.GetActivityResult(data);

                if (resultCode == (int)Result.Ok)
                {
                    resultUri = result.Uri;

                    imgProfileOverlay.Visibility = ViewStates.Visible;
                    btnCamera.Visibility = ViewStates.Gone;
                    btnCameraOverlay.Visibility = ViewStates.Gone;

                    UploadProfileImage();
                }

                else if (resultCode == CropImage.CropImageActivityResultErrorCode)
                {
                    System.Exception error = result.Error;
                }
            }
        }

        async Task<bool> GetIntent()
        {
            if (Activity.Intent.HasExtra(GetString(Resource.String.intent_user_type)))
            {
                userType = Activity.Intent.GetStringExtra(GetString(Resource.String.intent_user_type));

                if (string.Compare(userType, GetString(Resource.String.intent_user)) == 0)
                {
                    try
                    {
                        currentProfile = await MySilatProfile.GetProfileByIDAsync(MySilatProfile.UserProfile.ProfileId);

                        if (currentProfile != null)
                        {
                            MySilatProfile.UserProfile = currentProfile;
                            DownloadProfileImage(currentProfile);
                        }
                        else
                        {
                            NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                        NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                    }

                }

                else if (string.Compare(userType, GetString(Resource.String.intent_global)) == 0)
                {
                    /// TODO
                    ///
                    /// Editing someone else
                }
            }

            return true;
        }

        async void Save()
        {
            Snackbar savingSnackbar = Snackbar.Make(rootView, Resource.String.saving, Snackbar.LengthIndefinite);

            bool success = false;

            var finalResult = await MySilatProfile.UpdateProfileNamesAsync(currentProfile.ProfileId, tieFullName.Text, tieLastName.Text, tieAltName.Text);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
            {
                // Record exist
                if (genericObject.Data != null)
                {
                    currentProfile = await MySilatProfile.GetProfileByIDAsync(MySilatProfile.UserProfile.ProfileId);

                    if (currentProfile != null)
                    {
                        if (string.Compare(userType, GetString(Resource.String.intent_user)) == 0)
                        {
                            MySilatProfile.UserProfile = currentProfile;
                        }

                        success = true;
                    }
                }
            }

            savingSnackbar.Dismiss();

            if (success)
            {
                Snackbar.Make(rootView, Resource.String.saved, Snackbar.LengthShort)
                        .Show();
            }
            else
            {
                Snackbar snackbar = Snackbar.Make(rootView, Resource.String.saving_error, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, v => { });

                MySilatProfile.UserProfile = currentProfile;
            }
        }

        void UploadProfileImage()
        {
            snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
            snackbar.Show();

            imgProfile.Visibility = ViewStates.Gone;
            btnCamera.Visibility = ViewStates.Gone;
            progressBar.Visibility = ViewStates.Visible;

            uploadFileName = Guid.NewGuid().ToString() + "-" + resultUri.LastPathSegment;

            FirebaseMyStorage.Init();
            profileImageRef = FirebaseMyStorage.storageRef.Child("profile/" + currentProfile.ProfileId + "/images/" + uploadFileName);

            uploadTask = profileImageRef.PutFile(resultUri)
                .AddOnProgressListener(this)
                .AddOnSuccessListener(this)
                .AddOnFailureListener(this);
        }

        private void ProfileTextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            bool changed = false;

            if (string.Compare(tieFullName.Text.Trim(), currentProfile.FirstName) != 0)
                changed = true;
            if (string.Compare(tieLastName.Text.Trim(), currentProfile.LastName) != 0)
                changed = true;
            if (string.Compare(tieAltName.Text.Trim(), currentProfile.AlternateName) != 0)
                changed = true;

            if (changed)
                lytProfile.Visibility = ViewStates.Visible;
            else
                lytProfile.Visibility = ViewStates.Gone;
        }

        void DownloadProfileImage(UserProfileModel userProfileModel)
        {
            if (userProfileModel != null)
            {
                imgProfile.Visibility = ViewStates.Gone;
                progressBar.Visibility = ViewStates.Visible;

                int imgId = 0;
                int.TryParse(userProfileModel.ProfileImgId, out imgId);

                int userId = 0;
                int.TryParse(userProfileModel.ProfileId, out userId);

                // User uploaded image
                if (imgId > 0)
                {
                    FirebaseMyStorage.Init();
                    profileImageRef = FirebaseMyStorage.storageRef.Child("profile/" + userId + "/images/thumb@" + thumbSize + currentProfile.ProfileImgName);

                    downloadTask = profileImageRef.GetDownloadUrl()
                        .AddOnSuccessListener(this)
                        .AddOnFailureListener(this);
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(userProfileModel.FacebookId))
                {
                    try
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(userProfileModel.FacebookId, MySilatCommon.IMAGE_LARGE);
                        Picasso.With(imgProfile.Context).Load(uri).Resize(resizeDim, resizeDim)
                            .Into(imgProfile, new Action(PicassoComplete), new Action(PicassoComplete));

                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine(e);
                        NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                    }
                    finally
                    {
                        progressBar.Visibility = ViewStates.Gone;
                    }
                }
            }
        }

        void PicassoComplete()
        {
            if (snackbar != null)
                snackbar.Dismiss();

            progressBar.Visibility = ViewStates.Gone;

            imgProfile.Visibility = ViewStates.Visible;
            imgProfile.SetScaleType(ImageView.ScaleType.CenterCrop);
            imgProfileOverlay.Visibility = ViewStates.Gone;

            btnCamera.Visibility = ViewStates.Visible;
            btnCameraOverlay.Visibility = ViewStates.Visible;
        }

        void CleanUpTasks()
        {
            try
            {
                Picasso.With(imgProfile.Context).CancelRequest(imgProfile);
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e);
            }
        }

        void StartICEditActivity()
        {
            try
            {
                Intent intent = new Intent(Activity, typeof(UserICEditActivity));

                intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

                // pass data to next activity
                intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_edit));
                intent.PutExtra("user_id", currentProfile.ProfileId);
                StartActivity(intent);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
            }
        }

        void IOnProgressListener.snapshot(Java.Lang.Object p0)
        {
            if (uploadTask != null)
            {
                progressBar.Visibility = ViewStates.Visible;

                var taskSnapshot = (UploadTask.TaskSnapshot)p0;
                double progress = (100.0 * taskSnapshot.BytesTransferred / taskSnapshot.TotalByteCount);
                progressBar.Progress = (int)progress;
            }
        }

        public async void OnSuccess(Java.Lang.Object result)
        {
            if (downloadTask != null)
            {
                if (downloadTask.IsSuccessful)
                {
                    try
                    {
                        Android.Net.Uri uri = (Android.Net.Uri)downloadTask.Result;
                        Picasso.With(imgProfile.Context).Load(uri).Resize(resizeDim, resizeDim)
                            .Into(imgProfile, new Action(PicassoComplete), new Action(PicassoComplete));
                    }
                    catch (System.Exception e)
                    {
                        Console.WriteLine(e);
                        NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                    }
                    finally
                    {
                        downloadTask = null;
                    }
                }
            }

            if (uploadTask != null)
            {
                if (uploadTask.IsSuccessful)
                {
                    var taskSnapshot = (UploadTask.TaskSnapshot)result;

                    Android.Net.Uri uri = await profileImageRef.GetDownloadUrlAsync();

                    var finalResult = await MySilatProfile.AddProfileImageAsync(
                        currentProfile.ProfileId,
                        uploadFileName,
                        uri.ToString());

                    GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(finalResult);

                    if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) == 0)
                    {
                        try
                        {
                            Picasso.With(imgProfile.Context).Load(resultUri).Resize(resizeDim, resizeDim)
                                .Into(imgProfile, new Action(PicassoComplete), new Action(PicassoComplete));

                            currentProfile = await MySilatProfile.GetProfileByIDAsync(currentProfile.ProfileId);
                            MySilatProfile.UserProfile = currentProfile;
                        }
                        catch (System.Exception e)
                        {
                            NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                            Console.WriteLine(e);
                        }
                    }
                    else
                    {
                        snackbar = Snackbar.Make(rootView, Resource.String.server_error, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, v => { });
                    }

                    progressBar.Visibility = ViewStates.Gone;
                    imgProfileOverlay.Visibility = ViewStates.Gone;
                    btnCamera.Visibility = ViewStates.Visible;
                    btnCameraOverlay.Visibility = ViewStates.Visible;

                    uploadTask = null;
                }
            }
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            if (downloadTask != null)
            {
                if (!downloadTask.IsSuccessful)
                {
                    downloadTask = null;
                    progressBar.Visibility = ViewStates.Gone;
                    NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                }
            }

            if (uploadTask != null)
            {
                if (!uploadTask.IsSuccessful)
                {
                    progressBar.Visibility = ViewStates.Gone;
                    imgProfileOverlay.Visibility = ViewStates.Gone;
                    btnCamera.Visibility = ViewStates.Visible;
                    btnCameraOverlay.Visibility = ViewStates.Visible;

                    snackbar.Dismiss();

                    snackbar = Snackbar.Make(this.rootView, Resource.String.saving_error, Snackbar.LengthIndefinite)
                        .SetAction(Resource.String.ok, v => { });

                    uploadTask = null;

                    NetworkManager.IsOnline(this.Context, true, ToastLength.Long);
                }
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (NetworkManager.IsOnline(this.Context, true, ToastLength.Long) == false)
                return;

            InputMethodManager inputManager = (InputMethodManager)Context.GetSystemService(Context.InputMethodService);

            if (inputManager != null && View != null)
                inputManager.HideSoftInputFromWindow(View.WindowToken, HideSoftInputFlags.NotAlways);

            tieFullName.ClearFocus();
            tieLastName.ClearFocus();
            tieAltName.ClearFocus();

            tilFullName.Enabled = false;
            tilLastName.Enabled = false;
            tilAltName.Enabled = false;

            lytProfile.Visibility = ViewStates.Gone;

            Save();

            tilFullName.Enabled = true;
            tilLastName.Enabled = true;
            tilAltName.Enabled = true;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            InputMethodManager inputManager = (InputMethodManager)Context.GetSystemService(Context.InputMethodService);

            if (inputManager != null && View != null)
                inputManager.HideSoftInputFromWindow(View.WindowToken, HideSoftInputFlags.NotAlways);

            tieFullName.ClearFocus();
            tieLastName.ClearFocus();
            tieAltName.ClearFocus();

            if (currentProfile != null)
            {
                tieFullName.Text = currentProfile.FirstName;
                tieLastName.Text = currentProfile.LastName;
                tieAltName.Text = currentProfile.AlternateName;
            }
            else
            {
                tieFullName.Text = "";
                tieLastName.Text = "";
                tieAltName.Text = "";
            }
        }

        private void BtnCamera_Click(object sender, EventArgs e)
        {
            if (NetworkManager.IsOnline(this.Context, true, ToastLength.Long) == false)
                return;

            // for fragment (DO NOT use `getActivity()`)
            CropImage.Builder()
                .SetGuidelines(CropImageView.Guidelines.On)
                .SetAspectRatio(1, 1)
                .SetMinCropResultSize(512, 512)
                .SetFixAspectRatio(true)
                .Start(Context, this);
        }

        private void BtnIC_Click(object sender, EventArgs e)
        {
            StartICEditActivity();
        }
    }
}