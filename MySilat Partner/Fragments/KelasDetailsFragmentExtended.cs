﻿using Android.Content;
using Android.Gms.Tasks;
using Android.Graphics;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Charts;
using Firebase.Storage;
using MySilatPartner.API;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using v4App = Android.Support.V4.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Fragments
{
    partial class KelasDetailsFragment : v4App.Fragment, KelasLatihanAdapter.Callback, IOnSuccessListener, IOnFailureListener
    {
        #region Profile Image / Firebase Storage
        int thumbSize = 256;

        Dictionary<int, Task> downloadTaskDict = new Dictionary<int, Task>();
        Dictionary<int, TaskType> taskTypeDict = new Dictionary<int, TaskType>();

        public enum TaskType
        {
            TP,
            Penuntut
        }

        void GetImageThumbUri(TaskType taskType, string userId, string imgName, int thumbDim)
        {
            FirebaseMyStorage.Init();
            StorageReference profileImageRef = FirebaseMyStorage.storageRef.Child("profile/" + userId + "/images/thumb@" + thumbDim + imgName);

            int userIdInt = 0;
            int.TryParse(userId, out userIdInt);

            if (!downloadTaskDict.ContainsKey(userIdInt))
            {
                downloadTaskDict.Add(userIdInt, profileImageRef.GetDownloadUrl()
                            .AddOnSuccessListener(this)
                            .AddOnFailureListener(this));

                switch (taskType)
                {
                    case TaskType.TP:
                        taskTypeDict.Add(userIdInt, TaskType.TP);
                        break;

                    case TaskType.Penuntut:
                        taskTypeDict.Add(userIdInt, TaskType.Penuntut);
                        break;
                }
            }
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                int userId = downloadTaskDict.ElementAt(i).Key;
                Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (!task.IsSuccessful)
                    continue;

                Android.Net.Uri path = (task.Result as Android.Net.Uri);

                var taskType = taskTypeDict[downloadTaskDict.ElementAt(i).Key];

                switch (taskType)
                {
                    case TaskType.TP:
                        kelasTPAdapter.imgUri[userId] = path;
                        kelasTPAdapter.NotifyDataSetChanged();
                        break;
                    case TaskType.Penuntut:
                        kelasPenuntutAdapter.imgUri[userId] = path;
                        kelasPenuntutAdapter.NotifyDataSetChanged();
                        break;
                }

                taskTypeDict.Remove(downloadTaskDict.ElementAt(i).Key);
                downloadTaskDict[userId] = null;
            }

        }

        public void OnFailure(Java.Lang.Exception e)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (task.IsSuccessful)
                    continue;

                taskTypeDict.Remove(downloadTaskDict.ElementAt(i).Key);

                int key = downloadTaskDict.ElementAt(i).Key;
                downloadTaskDict[key] = null;
            }

            Console.WriteLine(e);
        }
        #endregion

        private async void OnKelasItemClickAsync(object sender, int position)
        {
            if (sender is TauliahRecyclerAdapter)
            {
                kelasId = tauliahList[position].KelasId;

                tauliahDialog.Dismiss();

                await GenerateData();
            }
        }

        private void BtnKelasEdit_Click(object sender, EventArgs e)
        {
            popupKelas.Show();
            popupKelas.MenuItemClick +=
                        new EventHandler<v7Widget.PopupMenu.MenuItemClickEventArgs>((s, args) =>
                        {
                            switch ((args as v7Widget.PopupMenu.MenuItemClickEventArgs).Item.ItemId)
                            {
                                case Resource.Id.details:
                                    StartEditKelasActivity();
                                    break;
                                case Resource.Id.address:
                                    StartEditAddressActivity();
                                    break;
                                case Resource.Id.contact:
                                    StartEditContactActivity();
                                    break;
                            }
                        });
        }

        private void BtnMap_Click(object sender, EventArgs e)
        {
            AlertDialog mapDialog;
            AlertDialog.Builder mapBuilder = new AlertDialog.Builder(this.Context);

            mapBuilder.SetTitle(GetString(Resource.String.address));

            string address = "";

            if (kelasAddressBook != null)
            {
                if (!string.IsNullOrWhiteSpace(kelasAddressBook.Line1))
                    address += kelasAddressBook.Line1;

                if (!string.IsNullOrWhiteSpace(kelasAddressBook.Line2))
                    address += ",\n" + kelasAddressBook.Line2;

                if (!string.IsNullOrWhiteSpace(kelasAddressBook.Postcode))
                    address += ",\n" + kelasAddressBook.Postcode;

                if (!string.IsNullOrWhiteSpace(kelasAddressBook.State))
                    address += ", " + kelasAddressBook.State;

                if (!string.IsNullOrWhiteSpace(address))
                    mapBuilder.SetMessage(address);
                else
                    mapBuilder.SetMessage(GetString(Resource.String.not_set));
            }
            else
                mapBuilder.SetMessage(GetString(Resource.String.not_set));

            mapBuilder.SetNegativeButton(GetString(Resource.String.ok), (s, args) => { });

            mapDialog = mapBuilder.Create();
            mapDialog.Show();

            Color backgroundColor = new Color(ContextCompat.GetColor(Context, Resource.Color.SecondaryHeader));
            Color buttonColor = new Color(ContextCompat.GetColor(Context, Resource.Color.LightGrey));
            mapDialog.Window.DecorView.Background.SetColorFilter(backgroundColor, PorterDuff.Mode.Src);
            mapDialog.GetButton((int)Android.Content.DialogButtonType.Negative).SetTextColor(buttonColor);
        }

        private void BtnChat_Click(object sender, EventArgs e)
        {
            if (kelasAddressBook != null)
            {
                if (!string.IsNullOrWhiteSpace(kelasAddressBook.Line1))
                    MySilatCommon.SendWhatsapp(kelasAddressBook.MobilePhone, "Test message", this.Context);
            }
            else
            {
                AlertDialog chatDialog;
                AlertDialog.Builder chatBuilder = new AlertDialog.Builder(this.Context);

                chatBuilder.SetTitle(GetString(Resource.String.address));
                chatBuilder.SetMessage(GetString(Resource.String.not_set));
                chatBuilder.SetNegativeButton(GetString(Resource.String.ok), (s, args) => { });

                chatDialog = chatBuilder.Create();
                chatDialog.Show();

                Color backgroundColor = new Color(ContextCompat.GetColor(Context, Resource.Color.SecondaryHeader));
                Color buttonColor = new Color(ContextCompat.GetColor(Context, Resource.Color.LightGrey));
                chatDialog.Window.DecorView.Background.SetColorFilter(backgroundColor, PorterDuff.Mode.Src);
                chatDialog.GetButton((int)Android.Content.DialogButtonType.Negative).SetTextColor(buttonColor);
            }
        }

        private void BtnCall_Click(object sender, EventArgs e)
        {
            AlertDialog callDialog;
            AlertDialog.Builder callBuilder = new AlertDialog.Builder(this.Context);

            callBuilder.SetTitle(GetString(Resource.String.contact_info));

            string contactNumber = "";

            if (kelasAddressBook == null)
            {
                contactNumber = GetString(Resource.String.not_set);
                callBuilder.SetMessage(contactNumber);
            }

            else if (string.IsNullOrWhiteSpace(kelasAddressBook.MobilePhone)
                && string.IsNullOrWhiteSpace(kelasAddressBook.HomePhone))
            {
                contactNumber = GetString(Resource.String.not_set);
                callBuilder.SetMessage(contactNumber);
            }

            else
            {
                View dialogView = this.Activity.LayoutInflater.Inflate(Resource.Layout.contact_phone, null);
                callBuilder.SetView(dialogView);

                LinearLayout lytMobilePhone = (LinearLayout)dialogView.FindViewById(Resource.Id.lyt_mobile);
                LinearLayout lytHomePhone = (LinearLayout)dialogView.FindViewById(Resource.Id.lyt_home);

                if (!string.IsNullOrWhiteSpace(kelasAddressBook.MobilePhone))
                {
                    Button btnCallMobile = (Button)dialogView.FindViewById(Resource.Id.btn_call_mobile);
                    btnCallMobile.Click += (s, args) => { MySilatCommon.DialPhone(kelasAddressBook.MobilePhone, Activity); };
                    btnCallMobile.Text = kelasAddressBook.MobilePhone;
                }
                else
                {
                    lytMobilePhone.Visibility = ViewStates.Gone;
                }

                if (!string.IsNullOrWhiteSpace(kelasAddressBook.HomePhone))
                {
                    Button btnCallHome = (Button)dialogView.FindViewById(Resource.Id.btn_call_home);
                    btnCallHome.Click += (s, args) => { MySilatCommon.DialPhone(kelasAddressBook.HomePhone, Activity); };
                    btnCallHome.Text = kelasAddressBook.HomePhone;
                }
                else
                {
                    lytHomePhone.Visibility = ViewStates.Gone;
                }
            }

            callBuilder.SetNegativeButton(GetString(Resource.String.ok), (s, args) => { });

            callDialog = callBuilder.Create();
            callDialog.Show();

            Color backgroundColor = new Color(ContextCompat.GetColor(Context, Resource.Color.SecondaryHeader));
            Color buttonColor = new Color(ContextCompat.GetColor(Context, Resource.Color.LightGrey));
            callDialog.Window.DecorView.Background.SetColorFilter(backgroundColor, PorterDuff.Mode.Src);
            callDialog.GetButton((int)Android.Content.DialogButtonType.Negative).SetTextColor(buttonColor);
        }

        private void BtnAddKelasLatihan_Click(object sender, EventArgs e)
        {
            StartAddKelasLatihan();
        }

        private void BtnPenuntutMore_Click(object sender, EventArgs e)
        {
            popupPenuntut.Show();
            popupPenuntut.MenuItemClick +=
                new EventHandler<v7Widget.PopupMenu.MenuItemClickEventArgs>((s, args) =>
                {
                    if (args.Item.ItemId == Resource.Id.view_all)
                        StartKelasMembersPenuntutActivity();
                    else if (args.Item.ItemId == Resource.Id.details)
                        StartKelasMembersDetailsPenuntutActivity();
                });
        }

        private void BtnTPMore_Click(object sender, EventArgs e)
        {
            popupTP.Show();
            popupTP.MenuItemClick +=
                new EventHandler<v7Widget.PopupMenu.MenuItemClickEventArgs>((s, args) =>
                {
                    if (args.Item.ItemId == Resource.Id.view_all)
                        StartKelasMembersTPActivity();
                    else if (args.Item.ItemId == Resource.Id.details)
                        StartKelasMembersDetailsTPActivity();
                });
        }

        private void KelasPenuntutAdapter_ItemClick(object sender, int e)
        {
            // throw new NotImplementedException();
        }

        private void KelasTPAdapter_ItemClick(object sender, int e)
        {
            // throw new NotImplementedException();
        }

        private void KelasAdapter_ItemClick(object sender, int e)
        {
            // throw new NotImplementedException();
        }

        public class CustomLinearLayoutManager : LinearLayoutManager
        {
            public CustomLinearLayoutManager(Context context) : base(context)
            {
            }

            public override bool CanScrollVertically()
            {
                return false;
            }
        }
    }
}