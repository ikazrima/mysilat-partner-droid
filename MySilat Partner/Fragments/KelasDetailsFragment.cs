﻿using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V4.Graphics.Drawable;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Charts;
using Com.Syncfusion.Sfbusyindicator;
using MySilatPartner.Activity;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using v4App = Android.Support.V4.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Fragments
{
    partial class KelasDetailsFragment : v4App.Fragment, KelasLatihanAdapter.Callback
    {
        string userType = "user";
        string kelasId = "0";

        KelasDetailsModel kelasDetails;
        AddressBookModel kelasAddressBook;

        KelasLatihanList kelasLatihanList;
        KelasLatihanAdapter kelasLatihanAdapter;

        KelasMemberList kelasTPList;
        KelasMemberRecyclerAdapter kelasTPAdapter;

        KelasMemberList kelasPenuntutList;
        KelasMemberRecyclerAdapter kelasPenuntutAdapter;

        #region UI

        IMenu menu;
        MenuInflater inflater;

        View rootView;
        LinearLayout lytContent;

        ImageView imgKelasType;

        TextView txtKelasCode;
        TextView txtNamaKelas;
        TextView txtJumlahTP;
        TextView txtJumlahPenuntut;

        RecyclerView kelasLatihanRecycler;
        RecyclerView.LayoutManager kelasLatihanLytMgr;

        RecyclerView kelasTPRecycler;
        GridLayoutManager kelasTPLytMgr;

        RecyclerView kelasPenuntutRecycler;
        GridLayoutManager kelasPenuntutLytMgr;

        ImageButton btnKelasEdit;
        ImageButton btnAddKelasLatihan;
        ImageButton btnTPMore;
        ImageButton btnPenuntutMore;

        ImageButton btnCall;
        ImageButton btnChat;
        ImageButton btnMap;

        v7Widget.PopupMenu popupKelas;
        v7Widget.PopupMenu popupTP;
        v7Widget.PopupMenu popupPenuntut;

        ProgressBar progressKelasLatihan;
        ProgressBar progressTP;
        ProgressBar progressPenuntut;

        LinearLayout lytNoKelasLatihan;
        LinearLayout lytNoTP;
        LinearLayout lytNoPenuntut;
        #endregion

        #region Tauliah
        TauliahList tauliahList;
        TauliahRecyclerAdapter tauliahAdapter;
        v7Widget.RecyclerView tauliahRecycler;
        v7Widget.RecyclerView.LayoutManager tauliahLytMgr;

        AlertDialog tauliahDialog;

        SfBusyIndicator sfBusyTauliah;
        #endregion

        const string TAG = "KelasDetailsFragment";

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RetainInstance = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            HasOptionsMenu = true;

            rootView = inflater.Inflate(Resource.Layout.kelas_details, container, false);

            lytContent = (LinearLayout)rootView.FindViewById(Resource.Id.lyt_content);

            imgKelasType = (ImageView)lytContent.FindViewById(Resource.Id.img_kelas_type);

            txtKelasCode = (TextView)lytContent.FindViewById(Resource.Id.txt_kelas_code);
            txtNamaKelas = (TextView)lytContent.FindViewById(Resource.Id.txt_nama_kelas);
            txtJumlahTP = (TextView)lytContent.FindViewById(Resource.Id.txt_tp_jumlah);
            txtJumlahPenuntut = (TextView)lytContent.FindViewById(Resource.Id.txt_penuntut_jumlah);

            kelasLatihanRecycler = (RecyclerView)lytContent.FindViewById(Resource.Id.kelas_latihan_recycler);
            kelasTPRecycler = (RecyclerView)lytContent.FindViewById(Resource.Id.tp_recycler);
            kelasPenuntutRecycler = (RecyclerView)lytContent.FindViewById(Resource.Id.penuntut_recycler);

            progressKelasLatihan = (ProgressBar)rootView.FindViewById(Resource.Id.pb_kelas_latihan);
            progressTP = (ProgressBar)rootView.FindViewById(Resource.Id.pb_tp);
            progressPenuntut = (ProgressBar)rootView.FindViewById(Resource.Id.pb_penuntut);

            lytNoKelasLatihan = (LinearLayout)rootView.FindViewById(Resource.Id.lyt_no_kelas_latihan);
            lytNoTP = (LinearLayout)rootView.FindViewById(Resource.Id.lyt_no_tp);
            lytNoPenuntut = (LinearLayout)rootView.FindViewById(Resource.Id.lyt_no_penuntut);

            btnKelasEdit = (ImageButton)rootView.FindViewById(Resource.Id.btn_kelas_edit);
            btnAddKelasLatihan = (ImageButton)rootView.FindViewById(Resource.Id.btn_kelas_more);
            btnTPMore = (ImageButton)rootView.FindViewById(Resource.Id.btn_tp_more);
            btnPenuntutMore = (ImageButton)rootView.FindViewById(Resource.Id.btn_penuntut_more);

            btnCall = (ImageButton)rootView.FindViewById(Resource.Id.btn_call);
            btnChat = (ImageButton)rootView.FindViewById(Resource.Id.btn_chat);
            btnMap = (ImageButton)rootView.FindViewById(Resource.Id.btn_map);

            ContextThemeWrapper ctw = new ContextThemeWrapper(Context, Resource.Style.PopupMenu);
            popupKelas = new v7Widget.PopupMenu(ctw, btnKelasEdit, (int)(GravityFlags.Top | GravityFlags.Right));
            popupTP = new v7Widget.PopupMenu(ctw, btnTPMore, (int)(GravityFlags.Top | GravityFlags.Right));
            popupPenuntut = new v7Widget.PopupMenu(ctw, btnPenuntutMore, (int)(GravityFlags.Top | GravityFlags.Right));

            MenuInflater popupKelasInflater = popupKelas.MenuInflater;
            popupKelasInflater.Inflate(Resource.Menu.kelas_details_menu, popupKelas.Menu);

            MenuInflater popupTPInfalter = popupTP.MenuInflater;
            popupTPInfalter.Inflate(Resource.Menu.kelas_members_more, popupTP.Menu);

            MenuInflater popupPenuntutInfalter = popupPenuntut.MenuInflater;
            popupPenuntutInfalter.Inflate(Resource.Menu.kelas_members_more, popupPenuntut.Menu);

            lytContent.Focusable = false;
            kelasLatihanRecycler.Focusable = false;
            kelasTPRecycler.Focusable = false;
            kelasPenuntutRecycler.Focusable = false;

            return rootView;
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            #region kelas latihan
            kelasLatihanList = new KelasLatihanList();
            kelasLatihanAdapter = new KelasLatihanAdapter(kelasLatihanList, this);
            kelasLatihanAdapter.ItemClick += KelasAdapter_ItemClick;
            kelasLatihanLytMgr = new CustomLinearLayoutManager(this.Context);
            kelasLatihanRecycler.SetLayoutManager(kelasLatihanLytMgr);
            kelasLatihanRecycler.SetAdapter(kelasLatihanAdapter);
            #endregion

            #region kelas TP
            kelasTPList = new KelasMemberList();
            kelasTPAdapter = new KelasMemberRecyclerAdapter(kelasTPList);
            kelasTPAdapter.ItemClick += KelasTPAdapter_ItemClick;
            kelasTPLytMgr = new GridLayoutManager(this.Context, 1, GridLayoutManager.Horizontal, false); // 1 row

            kelasTPRecycler.SetLayoutManager(kelasTPLytMgr);
            kelasTPRecycler.SetAdapter(kelasTPAdapter);
            #endregion

            #region kelas penuntut
            kelasPenuntutList = new KelasMemberList();
            kelasPenuntutAdapter = new KelasMemberRecyclerAdapter(kelasPenuntutList);
            kelasPenuntutAdapter.ItemClick += KelasPenuntutAdapter_ItemClick;
            kelasPenuntutLytMgr = new GridLayoutManager(this.Context, 1, GridLayoutManager.Horizontal, false); // 1 row
            kelasPenuntutRecycler.SetLayoutManager(kelasPenuntutLytMgr);
            kelasPenuntutRecycler.SetAdapter(kelasPenuntutAdapter);
            #endregion

            await GetIntent();
            await GenerateData();

            DisplayOptionsMenu(this.menu, this.inflater);
        }

        public override async void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            // Reload
            if (resultCode == (int)Android.App.Result.Ok)
            {
                string _activity = data.GetStringExtra(GetString(Resource.String.intent_activity_type));

                if (string.Compare(_activity, "kelas_edit") == 0)
                    await GenerateKelasDetails();

                else if (string.Compare(_activity, "kelas_latihan_edit") == 0)
                    await GenerateKelasLatihan();

                else if (string.Compare(_activity, "kelas_latihan_add") == 0)
                    await GenerateKelasLatihan();

                else if (string.Compare(_activity, "address_edit") == 0)
                    await GenerateKelasDetails();

                else if (string.Compare(_activity, "contact_edit") == 0)
                    await GenerateKelasDetails();
            }
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);

            this.menu = menu;
            this.inflater = inflater;

            menu.Clear();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_switch:
                    ChangeKelasAsync();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        void DisplayOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            menu.Clear();
            inflater.Inflate(Resource.Menu.toolbar_menu, menu);

            IMenuItem itemRefresh = menu.FindItem(Resource.Id.menu_refresh);
            IMenuItem itemEdit = menu.FindItem(Resource.Id.menu_edit);
            IMenuItem itemMore = menu.FindItem(Resource.Id.menu_more);

            itemRefresh.SetVisible(false);
            itemEdit.SetVisible(false);
            itemMore.SetVisible(false);
        }

        async Task GetIntent()
        {
            MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);

            if (Activity.Intent.HasExtra(GetString(Resource.String.intent_user_type)))
            {
                userType = Activity.Intent.GetStringExtra(GetString(Resource.String.intent_user_type));

                if (string.Compare(userType, GetString(Resource.String.intent_user)) == 0)
                {
                    kelasId = MySilatPersilatan.UserPersilatan.MainKelasId;
                }

                else if (string.Compare(userType, GetString(Resource.String.intent_global)) == 0)
                {
                    /// TODO
                    ///
                    /// View for public
                    kelasId = "0";
                }
            }
            else
            {
                kelasId = MySilatPersilatan.UserPersilatan.MainKelasId;
            }
        }

        async Task GenerateData()
        {
            progressKelasLatihan.Visibility = ViewStates.Visible;
            kelasLatihanRecycler.Visibility = ViewStates.Gone;
            lytNoKelasLatihan.Visibility = ViewStates.Gone;

            progressTP.Visibility = ViewStates.Visible;
            lytNoTP.Visibility = ViewStates.Gone;
            kelasTPRecycler.Visibility = ViewStates.Gone;

            progressPenuntut.Visibility = ViewStates.Visible;
            lytNoPenuntut.Visibility = ViewStates.Gone;
            kelasPenuntutRecycler.Visibility = ViewStates.Gone;

            txtJumlahTP.Text = "";
            txtJumlahPenuntut.Text = "";

            await GenerateKelasDetails();
            await GenerateKelasLatihan();
            await GenerateKelasTP();
            await GenerateKelasPenuntut();

            GenerateProfileImageUri();
            // Displaying views before generating uri will result in null exception
            DisplayTPContent();
            DisplayPenuntutContent();

            progressTP.Visibility = ViewStates.Gone;
            progressPenuntut.Visibility = ViewStates.Gone;
        }

        async Task GenerateKelasDetails()
        {
            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.loading), Snackbar.LengthIndefinite);
            snackbar.Show();

            btnMap.Visibility = ViewStates.Gone;
            btnCall.Visibility = ViewStates.Gone;
            btnChat.Visibility = ViewStates.Gone;
            btnKelasEdit.Visibility = ViewStates.Gone;

            btnMap.Click -= BtnMap_Click;
            btnCall.Click -= BtnCall_Click;
            btnChat.Click -= BtnChat_Click;
            btnKelasEdit.Click -= BtnKelasEdit_Click;

            kelasDetails = await MySilatKelas.GetDetailsById(kelasId);

            if (kelasDetails != null)
            {
                // Wrapping in try / catch to prevent null exception when switching fragments during loading
                try
                {
                    switch (kelasDetails.Type)
                    {
                        // IPT
                        case "1":
                            imgKelasType.SetImageDrawable(ContextCompat.GetDrawable(imgKelasType.Context, Resource.Drawable.ic_graduate_cap_white_24dp));
                            break;
                        // Sekolah
                        case "2":
                            imgKelasType.SetImageDrawable(ContextCompat.GetDrawable(imgKelasType.Context, Resource.Drawable.ic_classroom_white_24dp));
                            break;
                        // Awam
                        case "3":
                            imgKelasType.SetImageDrawable(ContextCompat.GetDrawable(imgKelasType.Context, Resource.Drawable.ic_team_white_24dp));
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                txtKelasCode.Text = kelasDetails.Code;
                txtNamaKelas.Text = kelasDetails.Name;
            }

            kelasAddressBook = await MySilatAddressBook.GetAddressBookByOwner(kelasId, "2"); // type 2 = kelas

            if (kelasAddressBook == null)
            {
                await MySilatAddressBook.AddAddressBook(kelasId, "2"); // type 2 = kelas
                kelasAddressBook = await MySilatAddressBook.GetAddressBookByOwner(kelasId, "2"); // type 2 = kelas
            }

            btnMap.Click += BtnMap_Click;
            btnCall.Click += BtnCall_Click;
            btnChat.Click += BtnChat_Click;
            btnKelasEdit.Click += BtnKelasEdit_Click;

            btnMap.Visibility = ViewStates.Visible;
            btnCall.Visibility = ViewStates.Visible;
            btnChat.Visibility = ViewStates.Visible;
            btnKelasEdit.Visibility = ViewStates.Visible;

            snackbar.Dismiss();
        }

        async Task GenerateKelasLatihan()
        {
            progressKelasLatihan.Visibility = ViewStates.Visible;
            kelasLatihanRecycler.Visibility = ViewStates.Gone;
            lytNoKelasLatihan.Visibility = ViewStates.Gone;
            btnAddKelasLatihan.Click -= BtnAddKelasLatihan_Click;
            btnAddKelasLatihan.Visibility = ViewStates.Gone;

            await kelasLatihanList.GenerateKelasLatihanAsync(kelasId, DateTime.Today);

            if (kelasLatihanAdapter.ItemCount > 0)
            {
                lytNoKelasLatihan.Visibility = ViewStates.Gone;
                kelasLatihanRecycler.Visibility = ViewStates.Visible;
            }
            else
            {
                lytNoKelasLatihan.Visibility = ViewStates.Visible;
                kelasLatihanRecycler.Visibility = ViewStates.Gone;
            }

            kelasLatihanAdapter.NotifyDataSetChanged();

            btnAddKelasLatihan.Click += BtnAddKelasLatihan_Click;
            progressKelasLatihan.Visibility = ViewStates.Gone;
            btnAddKelasLatihan.Visibility = ViewStates.Visible;
        }

        async Task GenerateKelasTP()
        {
            progressTP.Visibility = ViewStates.Visible;
            lytNoTP.Visibility = ViewStates.Gone;
            kelasTPRecycler.Visibility = ViewStates.Gone;
            btnTPMore.Click -= BtnTPMore_Click;

            int[] tpTauliahId = new int[] { 2, 3, 4, 5, 6, 7 };
            string tpTauliahIdJson = JsonConvert.SerializeObject(tpTauliahId, Formatting.Indented);

            await kelasTPList.GenerateMemberByKelasIdYearTauliahIdAsync(kelasId, DateTime.Now, tpTauliahIdJson);
            kelasTPAdapter.NotifyDataSetChanged();

            btnTPMore.Click += BtnTPMore_Click;
        }

        async Task GenerateKelasPenuntut()
        {
            progressPenuntut.Visibility = ViewStates.Visible;
            lytNoPenuntut.Visibility = ViewStates.Gone;
            kelasPenuntutRecycler.Visibility = ViewStates.Gone;
            btnPenuntutMore.Click -= BtnPenuntutMore_Click;

            int[] penuntutTauliahId = new int[] { 1 };
            string penuntutTauliahIdJson = JsonConvert.SerializeObject(penuntutTauliahId, Formatting.Indented);

            await kelasPenuntutList.GenerateMemberByKelasIdYearTauliahIdAsync(kelasId, DateTime.Now, penuntutTauliahIdJson);
            kelasPenuntutAdapter.NotifyDataSetChanged();

            btnPenuntutMore.Click += BtnPenuntutMore_Click;
        }

        void GenerateProfileImageUri()
        {
            if (downloadTaskDict != null)
                downloadTaskDict.Clear();

            if (kelasTPAdapter.imgUri.Count > 0)
                kelasTPAdapter.imgUri.Clear();

            if (kelasPenuntutAdapter.imgUri.Count > 0)
                kelasPenuntutAdapter.imgUri.Clear();

            for (int index = 0; index < kelasTPList.mUserProfile.Count; index++)
            {
                var item = kelasTPList.mUserProfile[index];

                int imgId = 0;
                int.TryParse(item.ProfileImageId, out imgId);

                int userId = 0;
                int.TryParse(item.ProfileId, out userId);

                // User uploaded image
                if (imgId > 0)
                {
                    if (!kelasTPAdapter.imgUri.ContainsKey(userId))
                    {
                        kelasTPAdapter.imgUri.Add(userId, Android.Net.Uri.Empty);
                        GetImageThumbUri(TaskType.TP, item.ProfileId, item.ProfileImageName, thumbSize);
                    }
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(item.FacebookId))
                {
                    if (!kelasTPAdapter.imgUri.ContainsKey(userId))
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(item.FacebookId, MySilatCommon.IMAGE_LARGE);
                        kelasTPAdapter.imgUri.Add(userId, uri);
                    }

                }
            }

            for (int index = 0; index < kelasPenuntutList.mUserProfile.Count; index++)
            {
                var item = kelasPenuntutList.mUserProfile[index];

                int imgId = 0;
                int.TryParse(item.ProfileImageId, out imgId);

                int userId = 0;
                int.TryParse(item.ProfileId, out userId);

                // User uploaded image
                if (imgId > 0)
                {
                    if (!kelasPenuntutAdapter.imgUri.ContainsKey(userId))
                    {
                        kelasPenuntutAdapter.imgUri.Add(userId, Android.Net.Uri.Empty);
                        GetImageThumbUri(TaskType.Penuntut, item.ProfileId, item.ProfileImageName, thumbSize);
                    }
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(item.FacebookId))
                {
                    if (!kelasPenuntutAdapter.imgUri.ContainsKey(userId))
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(item.FacebookId, MySilatCommon.IMAGE_LARGE);
                        kelasPenuntutAdapter.imgUri.Add(userId, uri);
                    }
                }
            }
        }

        void DisplayTPContent()
        {
            if (kelasTPAdapter.ItemCount > 0)
            {
                btnTPMore.Visibility = ViewStates.Visible;

                lytNoTP.Visibility = ViewStates.Gone;
                kelasTPRecycler.Visibility = ViewStates.Visible;
                txtJumlahTP.Visibility = ViewStates.Visible;
                txtJumlahTP.Text = kelasTPAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower();
            }
            else
            {
                btnTPMore.Visibility = ViewStates.Gone;

                lytNoTP.Visibility = ViewStates.Visible;
                kelasTPRecycler.Visibility = ViewStates.Gone;
                txtJumlahTP.Visibility = ViewStates.Gone;
            }
        }

        void DisplayPenuntutContent()
        {
            if (kelasPenuntutAdapter.ItemCount > 0)
            {
                btnPenuntutMore.Visibility = ViewStates.Visible;

                lytNoPenuntut.Visibility = ViewStates.Gone;
                kelasPenuntutRecycler.Visibility = ViewStates.Visible;
                txtJumlahPenuntut.Visibility = ViewStates.Visible;
                txtJumlahPenuntut.Text = kelasPenuntutAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower();
            }
            else
            {
                btnPenuntutMore.Visibility = ViewStates.Gone;

                lytNoPenuntut.Visibility = ViewStates.Visible;
                kelasPenuntutRecycler.Visibility = ViewStates.Gone;
                txtJumlahPenuntut.Visibility = ViewStates.Gone;
            }
        }

        public void StartAddKelasLatihan()
        {
            Intent intent = new Intent(Activity, typeof(KelasLatihanEditActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_new));
            intent.PutExtra("app_bar_title", GetString(Resource.String.kelas_latihan) + " - " + GetString(Resource.String.add));
            intent.PutExtra("kelas_id", kelasDetails.Id);

            StartActivityForResult(intent, 1);
        }

        public void StartEditKelasLatihan(int position)
        {
            Intent intent = new Intent(Activity, typeof(KelasLatihanEditActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_edit));
            intent.PutExtra("app_bar_title", GetString(Resource.String.kelas_latihan) + " - " + GetString(Resource.String.edit));

            intent.PutExtra("kelas_latihan_id", kelasLatihanList[position].Id);
            intent.PutExtra("description", kelasLatihanList[position].KelasDescription);
            intent.PutExtra("start_time", kelasLatihanList[position].StartTime.Value.ToString("HH:mm"));
            intent.PutExtra("end_time", kelasLatihanList[position].EndTime.Value.ToString("HH:mm"));
            intent.PutExtra("days", kelasLatihanList[position].Days);

            StartActivityForResult(intent, 1);
        }

        void StartEditKelasActivity()
        {
            Intent intent = new Intent(Activity, typeof(KelasEditActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_edit));
            intent.PutExtra("app_bar_title", GetString(Resource.String.kelas) + " - " + GetString(Resource.String.edit));

            intent.PutExtra("kelas_id", kelasDetails.Id);
            intent.PutExtra("kelas_code", kelasDetails.Code);
            intent.PutExtra("kelas_name", kelasDetails.Name);
            intent.PutExtra("kelas_type", kelasDetails.Type);
            intent.PutExtra("address_state", kelasDetails.StateId);

            StartActivityForResult(intent, 1);
        }

        void StartEditAddressActivity()
        {
            Intent intent = new Intent(Activity, typeof(AddressEditActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity

            if (kelasAddressBook != null)
            {
                intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_edit));
                intent.PutExtra("app_bar_title", GetString(Resource.String.address) + " - " + GetString(Resource.String.edit));

                intent.PutExtra("id", kelasAddressBook.Id);
                intent.PutExtra("line1", kelasAddressBook.Line1);
                intent.PutExtra("line2", kelasAddressBook.Line2);
                intent.PutExtra("postcode", kelasAddressBook.Postcode);
                intent.PutExtra("address_state", kelasAddressBook.StateId);
            }
            else
            {
                intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_new));
                intent.PutExtra("app_bar_title", GetString(Resource.String.address) + " - " + GetString(Resource.String.add));
            }

            StartActivityForResult(intent, 1);
        }

        void StartEditContactActivity()
        {
            Intent intent = new Intent(Activity, typeof(ContactEditActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity

            if (kelasAddressBook != null)
            {
                intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_edit));
                intent.PutExtra("app_bar_title", GetString(Resource.String.contact_info) + " - " + GetString(Resource.String.edit));

                intent.PutExtra("id", kelasAddressBook.Id);
                intent.PutExtra("fixed", kelasAddressBook.HomePhone);
                intent.PutExtra("mobile", kelasAddressBook.MobilePhone);
            }
            else
            {
                intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_new));
                intent.PutExtra("app_bar_title", GetString(Resource.String.contact_info) + " - " + GetString(Resource.String.add));
            }

            StartActivityForResult(intent, 1);
        }

        void StartKelasMembersTPActivity()
        {
            Intent intent = new Intent(Activity, typeof(KelasMembersActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_search));
            intent.PutExtra("app_bar_title", kelasDetails.Code);

            intent.PutExtra("title", GetString(Resource.String.tp));
            intent.PutExtra("sub_title", kelasTPAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower());
            intent.PutExtra("kelas_members", JsonConvert.SerializeObject(kelasTPList));

            Dictionary<int, string> tmpUri = new Dictionary<int, string>();

            foreach (var item in kelasTPAdapter.imgUri)
            {
                tmpUri.Add(item.Key, item.Value.ToString());
            }

            intent.PutExtra("img_uri", JsonConvert.SerializeObject(tmpUri));

            StartActivity(intent);
        }

        void StartKelasMembersPenuntutActivity()
        {
            Intent intent = new Intent(Activity, typeof(KelasMembersActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_search));
            intent.PutExtra("app_bar_title", kelasDetails.Code);

            intent.PutExtra("title", GetString(Resource.String.penuntut));
            intent.PutExtra("sub_title", kelasPenuntutAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower());
            intent.PutExtra("kelas_members", JsonConvert.SerializeObject(kelasPenuntutList));

            Dictionary<int, string> tmpUri = new Dictionary<int, string>();

            foreach (var item in kelasPenuntutAdapter.imgUri)
            {
                tmpUri.Add(item.Key, item.Value.ToString());
            }

            intent.PutExtra("img_uri", JsonConvert.SerializeObject(tmpUri));

            StartActivity(intent);
        }

        void StartKelasMembersDetailsTPActivity()
        {
            Intent intent = new Intent(Activity, typeof(KelasMembersDetailsActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_search));
            intent.PutExtra("app_bar_title", GetString(Resource.String.tp));
            intent.PutExtra("sub_title", kelasTPAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower());
            intent.PutExtra("kelas_members", JsonConvert.SerializeObject(kelasTPList));
            intent.PutExtra("type", GetString(Resource.String.tp));

            StartActivity(intent);
        }

        void StartKelasMembersDetailsPenuntutActivity()
        {
            Intent intent = new Intent(Activity, typeof(KelasMembersDetailsActivity));

            intent.AddFlags(ActivityFlags.ClearTop); // prevent multiple same activities

            // pass data to next activity
            intent.PutExtra(GetString(Resource.String.intent_activity_type), GetString(Resource.String.intent_search));
            intent.PutExtra("app_bar_title", GetString(Resource.String.penuntut));
            intent.PutExtra("sub_title", kelasTPAdapter.ItemCount + " " + GetString(Resource.String.orang).ToLower());
            intent.PutExtra("kelas_members", JsonConvert.SerializeObject(kelasPenuntutList));
            intent.PutExtra("type", GetString(Resource.String.penuntut));

            StartActivity(intent);
        }

        private async void ChangeKelasAsync()
        {
            View dialogView = this.Activity.LayoutInflater.Inflate(Resource.Layout.recycler_generic_dialog, null);

            ConstraintLayout lytContent = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_content);
            lytContent.Visibility = ViewStates.Gone;

            v7Widget.SearchView tauliahSearchView = (v7Widget.SearchView)dialogView.FindViewById(Resource.Id.search);

            TextView tauliah_txtTitle = (TextView)dialogView.FindViewById(Resource.Id.txt_title);
            TextView tauliah_txtSubtitle = (TextView)dialogView.FindViewById(Resource.Id.txt_sub_title);

            tauliahSearchView.QueryHint = GetString(Resource.String.hint_kelas);

            tauliahSearchView.QueryTextChange += new EventHandler<v7Widget.SearchView.QueryTextChangeEventArgs>((s, args) =>
            {
                tauliahAdapter.Filter((s as v7Widget.SearchView).Query);
                tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();
            });

            tauliahSearchView.QueryTextSubmit += new EventHandler<v7Widget.SearchView.QueryTextSubmitEventArgs>((s, args) =>
            {
                tauliahAdapter.Filter((s as v7Widget.SearchView).Query);
                tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();
            });

            tauliahSearchView.Visibility = ViewStates.Gone;

            TextView txtWarning = (TextView)dialogView.FindViewById(Resource.Id.txt_warning);

            ImageView searchIcon = (ImageView)tauliahSearchView.FindViewById(Resource.Id.search_button);
            searchIcon.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.ic_search_white_24dp));

            DrawableCompat.SetTint(
                DrawableCompat.Wrap(searchIcon.Drawable),
                ContextCompat.GetColor(this.Context, Resource.Color.White)
            );

            v7Widget.SearchView.SearchAutoComplete searchAutoComplete = (v7Widget.SearchView.SearchAutoComplete)tauliahSearchView.FindViewById(Resource.Id.search_src_text);
            searchAutoComplete.SetHint(Resource.String.hint_kelas);
            searchAutoComplete.SetHintTextColor(Color.White);
            searchAutoComplete.SetTextColor(Color.White);

            sfBusyTauliah = (SfBusyIndicator)dialogView.FindViewById(Resource.Id.busyIndicator);
            sfBusyTauliah.SetBackgroundColor(Color.Transparent);
            sfBusyTauliah.TextColor = new Color(ContextCompat.GetColor(this.Activity, Resource.Color.Accent));

            Button btnSave = (Button)dialogView.FindViewById(Resource.Id.btn_save);
            btnSave.Visibility = ViewStates.Gone;

            Button btnCancel = (Button)dialogView.FindViewById(Resource.Id.btn_cancel);
            btnCancel.Click += new EventHandler((s, args) => { tauliahDialog.Dismiss(); });

            AlertDialog.Builder tauliahBuilder = new AlertDialog.Builder(this.Context);
            tauliahBuilder.SetView(dialogView);
            tauliahDialog = tauliahBuilder.Create();
            tauliahDialog.Show();

            tauliahList = new TauliahList();
            await tauliahList.GenerateMemberByKelasIdYearTauliahIdAsync(MySilatProfile.UserProfile.ProfileId, DateTime.Now.Year);

            lytContent.Visibility = ViewStates.Visible;

            tauliahLytMgr = new v7Widget.LinearLayoutManager(this.Context, (int)Orientation.Vertical, false);

            tauliahAdapter = new TauliahRecyclerAdapter(tauliahList);
            tauliahAdapter.ItemClick += OnKelasItemClickAsync;
            tauliahAdapter.NotifyDataSetChanged();
            tauliahAdapter.displayJawatanCard = false;

            tauliahRecycler = (v7Widget.RecyclerView)dialogView.FindViewById(Resource.Id.content_recycler);
            tauliahRecycler.SetAdapter(tauliahAdapter);
            tauliahRecycler.HasFixedSize = true;
            tauliahRecycler.SetItemAnimator(new DefaultItemAnimator());
            tauliahRecycler.SetLayoutManager(tauliahLytMgr);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(Context, DividerItemDecoration.Vertical);
            tauliahRecycler.AddItemDecoration(dividerItemDecoration);

            tauliah_txtTitle.Text = GetString(Resource.String.kelas);
            tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();

            sfBusyTauliah.Visibility = ViewStates.Gone;

            if (tauliahAdapter.ItemCount > 0)
            {
                tauliahSearchView.Visibility = ViewStates.Visible;
                tauliahRecycler.Visibility = ViewStates.Visible;
                txtWarning.Visibility = ViewStates.Gone;
            }
            else
            {
                tauliahSearchView.Visibility = ViewStates.Gone;
                tauliahRecycler.Visibility = ViewStates.Gone;
                txtWarning.Visibility = ViewStates.Visible;
            }
        }
    }
}