﻿using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using v4App = Android.Support.V4.App;

namespace MySilatPartner.Fragments
{
    public class UserPersilatanFragment : v4App.Fragment
    {
        bool init = false;

        View rootView;

        Spinner peringkatSpinner;
        ArrayAdapter peringkatAdapter;
        Dictionary<int, string> peringkatDict = new Dictionary<int, string>();

        TextInputLayout tilAsas;
        TextInputLayout tilPotong;
        TextInputLayout tilTamat;
        TextInputLayout tilSRP;
        TextInputLayout tilSMP;

        TextInputEditText tieAsas;
        TextInputEditText tiePotong;
        TextInputEditText tieTamat;
        TextInputEditText tieSRP;
        TextInputEditText tieSMP;

        ConstraintLayout lytSijilHeader;

        DateTime date = new DateTime();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            rootView = inflater.Inflate(Resource.Layout.user_persilatan_edit, container, false);

            peringkatSpinner = (Spinner)rootView.FindViewById(Resource.Id.peringkat_spinner);

            tilAsas = (TextInputLayout)rootView.FindViewById(Resource.Id.til_asas);
            tilPotong = (TextInputLayout)rootView.FindViewById(Resource.Id.til_potong);
            tilTamat = (TextInputLayout)rootView.FindViewById(Resource.Id.til_tamat);

            tilSRP = (TextInputLayout)rootView.FindViewById(Resource.Id.til_srp);
            tilSMP = (TextInputLayout)rootView.FindViewById(Resource.Id.til_smp);

            tieAsas = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_asas);
            tiePotong = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_potong);
            tieTamat = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_tamat);

            tieSRP = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_srp);
            tieSMP = (TextInputEditText)rootView.FindViewById(Resource.Id.tie_smp);

            lytSijilHeader = (ConstraintLayout)rootView.FindViewById(Resource.Id.lyt_sijil_header);

            return rootView;
        }

        public override async void OnResume()
        {
            base.OnResume();

            if (!init)
            {
                Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.loading), Snackbar.LengthIndefinite);
                snackbar.Show();

                MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);

                InitPeringkatSpinner();
                InitFields();

                snackbar.Dismiss();

                init = true;
            }
        }

        void InitFields()
        {
            if (MySilatPersilatan.UserPersilatan.IjazahAsas != null)
                tieAsas.Text = MySilatPersilatan.UserPersilatan.IjazahAsas.ToString("dd MMM yyyy");

            if (MySilatPersilatan.UserPersilatan.IjazahPotong != null)
                tiePotong.Text = MySilatPersilatan.UserPersilatan.IjazahPotong.ToString("dd MMM yyyy");

            if (MySilatPersilatan.UserPersilatan.IjazahTamat != null)
                tieTamat.Text = MySilatPersilatan.UserPersilatan.IjazahTamat.ToString("dd MMM yyyy");

            if (MySilatPersilatan.UserPersilatan.SRP != null)
                tieSRP.Text = MySilatPersilatan.UserPersilatan.SRP.Year.ToString();

            if (MySilatPersilatan.UserPersilatan.SMP != null)
                tieSMP.Text = MySilatPersilatan.UserPersilatan.SMP.Year.ToString();

            tilAsas.Click += TieAsas_Click;
            tilPotong.Click += TiePotong_Click;
            tilTamat.Click += TieTamat_Click;

            tieAsas.Click += TieAsas_Click;
            tiePotong.Click += TiePotong_Click;
            tieTamat.Click += TieTamat_Click;

            tilSRP.Click += TieSRP_Click;
            tieSRP.Click += TieSRP_Click;

            tilSMP.Click += TieSMP_Click;
            tieSMP.Click += TieSMP_Click;
        }

        void InitPeringkatSpinner()
        {
            peringkatSpinner.Enabled = false;

            peringkatDict.Add(1, GetString(Resource.String.asas));
            peringkatDict.Add(2, GetString(Resource.String.jatuh));
            peringkatDict.Add(3, GetString(Resource.String.potong));
            peringkatDict.Add(4, GetString(Resource.String.tamat));

            List<string> peringkatName = new List<string>();

            foreach (var item in peringkatDict)
            {
                peringkatName.Add(item.Value);
            }

            peringkatAdapter = new ArrayAdapter<string>(Context, Resource.Layout.my_spinner_left, peringkatName);
            peringkatAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item_left);
            peringkatSpinner.Adapter = peringkatAdapter;
            peringkatSpinner.ItemSelected += PeringkatSpinner_ItemSelected;

            peringkatSpinner.SetSelection(MySilatPersilatan.UserPersilatan.PeringkatId - 1);

            peringkatSpinner.Enabled = true;
        }

        async void UpdateCurrentPeringkat()
        {
            peringkatSpinner.Enabled = false;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
            snackbar.Show();

            string peringkatId = "" + peringkatDict.ElementAt(peringkatSpinner.SelectedItemPosition).Key;

            var result = await MySilatPersilatan.UpdateUserPeringkat(MySilatProfile.UserProfile.ProfileId, peringkatId);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) != 0)
            {
                snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort);
                snackbar.Show();
            }

            peringkatSpinner.Enabled = true;
        }

        async void UpdateIjazah(string peringkat, DateTime date)
        {
            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
            snackbar.Show();

            string result = "";

            if (string.Compare(peringkat, GetString(Resource.String.asas)) == 0)
                result = await MySilatPersilatan.UpdateIjazahAsas(MySilatProfile.UserProfile.ProfileId, date);
            else if (string.Compare(peringkat, GetString(Resource.String.potong)) == 0)
                result = await MySilatPersilatan.UpdateIjazahPotong(MySilatProfile.UserProfile.ProfileId, date);
            else if (string.Compare(peringkat, GetString(Resource.String.tamat)) == 0)
                result = await MySilatPersilatan.UpdateIjazahTamat(MySilatProfile.UserProfile.ProfileId, date);

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) != 0)
            {
                snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort);
                snackbar.Show();
            }
            else
            {
                if (string.Compare(peringkat, GetString(Resource.String.asas)) == 0)
                    MySilatPersilatan.UserPersilatan.IjazahAsas = date;
                else if (string.Compare(peringkat, GetString(Resource.String.potong)) == 0)
                    MySilatPersilatan.UserPersilatan.IjazahPotong = date;
                else if (string.Compare(peringkat, GetString(Resource.String.tamat)) == 0)
                    MySilatPersilatan.UserPersilatan.IjazahTamat = date;
            }
        }

        async void UpdateSijil(string sijil, DateTime date)
        {
            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
            snackbar.Show();

            string result = "";

            if (string.Compare(sijil, GetString(Resource.String.srp)) == 0)
                result = await MySilatPersilatan.UpdateSRP(MySilatProfile.UserProfile.ProfileId, date);
            else if (string.Compare(sijil, GetString(Resource.String.smp)) == 0)
                result = await MySilatPersilatan.UpdateSMP(MySilatProfile.UserProfile.ProfileId, date);

            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) != 0)
            {
                snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort);
                snackbar.Show();
            }
            else
            {
                if (string.Compare(sijil, GetString(Resource.String.srp)) == 0)
                    MySilatPersilatan.UserPersilatan.SRP = date;
                else if (string.Compare(sijil, GetString(Resource.String.smp)) == 0)
                    MySilatPersilatan.UserPersilatan.SMP = date;
            }
        }

        private void SetReceivedDate(TextInputEditText field, int type, bool onlyYear)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime date)
            {
                if (date > DateTime.Today)
                    date = DateTime.Now;

                if (!onlyYear)
                    field.Text = date.ToString("dd MMMM yyyy");
                else
                    field.Text = date.Year.ToString();

                this.date = date;

                switch (type)
                {
                    case 0:
                        UpdateIjazah(GetString(Resource.String.asas), date);
                        break;
                    case 1:
                        UpdateIjazah(GetString(Resource.String.potong), date);
                        break;
                    case 2:
                        UpdateIjazah(GetString(Resource.String.tamat), date);
                        break;
                    case 3:
                        UpdateSijil(GetString(Resource.String.srp), date);
                        break;
                    case 4:
                        UpdateSijil(GetString(Resource.String.smp), date);
                        break;
                }
            });

            switch (type)
            {
                case 0:
                    frag.date = MySilatPersilatan.UserPersilatan.IjazahAsas;
                    break;
                case 1:
                    frag.date = MySilatPersilatan.UserPersilatan.IjazahPotong;
                    break;
                case 2:
                    frag.date = MySilatPersilatan.UserPersilatan.IjazahTamat;
                    break;
                case 3:
                    frag.date = MySilatPersilatan.UserPersilatan.SRP;
                    break;
                case 4:
                    frag.date = MySilatPersilatan.UserPersilatan.SMP;
                    break;
            }

            frag.Show(Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private void PeringkatSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            if (init)
                UpdateCurrentPeringkat();

            if (peringkatSpinner.SelectedItemPosition < 2)
            {
                tilPotong.Visibility = ViewStates.Gone;
                lytSijilHeader.Visibility = ViewStates.Gone;
                tilSRP.Visibility = ViewStates.Gone;
                tilSMP.Visibility = ViewStates.Gone;
            }

            else
                tilPotong.Visibility = ViewStates.Visible;

            if (peringkatSpinner.SelectedItemPosition < 3)
                tilTamat.Visibility = ViewStates.Gone;

            else
            {
                tilTamat.Visibility = ViewStates.Visible;
                lytSijilHeader.Visibility = ViewStates.Visible;
                tilSRP.Visibility = ViewStates.Visible;
                tilSMP.Visibility = ViewStates.Visible;
            }

        }

        private void TieAsas_Click(object sender, EventArgs e)
        {
            if (init)
                SetReceivedDate(tieAsas, 0, false);
        }

        private void TiePotong_Click(object sender, EventArgs e)
        {
            if (init)
                SetReceivedDate(tiePotong, 1, false);
        }

        private void TieTamat_Click(object sender, EventArgs e)
        {
            if (init)
                SetReceivedDate(tieTamat, 2, false);
        }

        private void TieSRP_Click(object sender, EventArgs e)
        {
            if (init)
                SetReceivedDate(tieSRP, 3, true);
        }

        private void TieSMP_Click(object sender, EventArgs e)
        {
            if (init)
                SetReceivedDate(tieSMP, 4, true);
        }
    }
}