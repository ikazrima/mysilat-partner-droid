﻿using Android.App;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Locations;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Com.Google.Maps.Android;
using Com.Google.Maps.Android.UI;
using MySilatPartner.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using v4App = Android.Support.V4.App;

namespace MySilatPartner.Fragments
{
    public class LocationFragment : v4App.Fragment, IOnMapReadyCallback
    {
        const string TAG = "MapFragment";

        // UI
        private FloatingActionButton fabSOS;
        private FloatingActionButton fabUserLocation;

        // Map
        private SupportMapFragment _mapFragment;
        private GoogleMap _map;

        private List<IconGenerator> iconList = new List<IconGenerator>();
        private List<Marker> markerList = new List<Marker>();
        private List<Polyline> polylineList = new List<Polyline>();

        private Marker userMarker;
        private Circle sosCircle;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
            
            /*
            _mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
            */
            if (_mapFragment == null)
            {
                GoogleMapOptions mapOptions = new GoogleMapOptions()
                    .InvokeMapType(GoogleMap.MapTypeNormal)
                    .InvokeZoomControlsEnabled(false)
                    .InvokeCompassEnabled(true);

                v4App.FragmentTransaction fragTx = FragmentManager.BeginTransaction();
                _mapFragment = SupportMapFragment.NewInstance(mapOptions);
                fragTx.Add(Resource.Id.map, _mapFragment, "map");
                fragTx.Commit();
            }

            _mapFragment.GetMapAsync(this);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            View rootView = inflater.Inflate(Resource.Layout.Maps, container, false);
            return rootView;

            //return base.OnCreateView(inflater, container, savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            InitControls();
        }

        void InitControls()
        {
            fabSOS = this.Activity.FindViewById<FloatingActionButton>(Resource.Id.fab_sos);
            fabSOS.Click += FabSOS_ClickAsync;

            fabUserLocation = this.Activity.FindViewById<FloatingActionButton>(Resource.Id.fab_user_location);
            fabUserLocation.Click += FabUserLocation_ClickAsync;
        }

        private async void FabSOS_ClickAsync(object sender, EventArgs e)
        {
            /*
            Snackbar
                    .Make(((MainActivity)this.Activity).lytCoordinator, "Works for me+!", Snackbar.LengthShort)
                    .Show();
            */
            /*
            bool onPath = await PointOnPath();

            if (onPath)
            {
                Snackbar
                    .Make(((MainActivity)this.Activity).lytCoordinator, "Point on path!", Snackbar.LengthShort)
                    .Show();
            }
            else
            {
                Snackbar
                    .Make(((MainActivity)this.Activity).lytCoordinator, "Area out of coverage!", Snackbar.LengthLong)
                    .Show();
            }*/
        }

        private async void FabUserLocation_ClickAsync(object sender, EventArgs e)
        {
            var location = await ((MainActivity)this.Activity).fusedLocationProviderClient.GetLastLocationAsync();
            LatLng latlng = new LatLng(location.Latitude, location.Longitude);

            GoToLocation(latlng);
        }

        public async void OnMapReady(GoogleMap map)
        {
            _map = map;
            _map.MapType = GoogleMap.MapTypeNormal;

            // We are disabling movement in maps
            //_map.UiSettings.SetAllGesturesEnabled(false);

            var location = await ((MainActivity)this.Activity).fusedLocationProviderClient.GetLastLocationAsync();
            LatLng latlng = new LatLng(location.Latitude, location.Longitude);

            userMarker = CreateUserMarker(location);
            GoToLocation(latlng);
        }

        public void GoToLocation(LatLng latlng)
        {
            if (_map != null)
            {
                CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
                builder.Target(latlng);
                builder.Zoom(18);
                builder.Bearing(155);
                builder.Tilt(65);
                CameraPosition cameraPosition = builder.Build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

                _map.MoveCamera(cameraUpdate);
            }
        }

        public Marker GetUserMarker()
        {
            return userMarker;
        }

        public Marker CreateUserMarker(Location location)
        {
            DateTime dt = MySilatCommon.FromUnixLocalTime(location.Time);

            TextView utilText = new TextView(this.Activity);
            utilText.Text = dt.ToString();

            IconGenerator iconGenerator = new IconGenerator(this.Activity);
            iconGenerator.SetStyle(4);
            iconGenerator.SetContentView(utilText);

            iconList.Add(iconGenerator);

            Bitmap icon = iconGenerator.MakeIcon();

            MarkerOptions mO = new MarkerOptions();

            LatLng latlng = new LatLng(location.Latitude, location.Longitude);

            mO.SetPosition(latlng);
            mO.SetIcon(BitmapDescriptorFactory.FromBitmap(icon)).SetTitle(dt.ToString()).SetAlpha(1);

            Marker marker = _map.AddMarker(mO);
            markerList.Add(marker);

            return marker;
        }

        public Marker SetUserMarkerLocation(Marker marker, Location location)
        {
            LatLng latlng = new LatLng(location.Latitude, location.Longitude);

            MarkerOptions mO = new MarkerOptions();
            mO.SetPosition(latlng);

            for (int i = 0; i < markerList.Count; i++)
            {
                if (markerList[i] == marker)
                {
                    markerList[i].Remove();
                    markerList.Remove(marker);
                    iconList.RemoveAt(i);
                    break;
                }
            }

            return userMarker = CreateUserMarker(location);
        }

        public Polyline DrawPolyline(List<LatLng> latlng, Color color)
        {
            PolylineOptions pO = new PolylineOptions();

            pO.InvokeWidth(20);
            pO.InvokeColor(color);

            foreach (LatLng ll in latlng)
            {
                pO.Add(ll);
            }

            return _map.AddPolyline(pO);
        }

        public void RemovePolyline(Polyline polyline)
        {
            foreach (Polyline p in polylineList)
            {
                if (p == polyline)
                {
                    polyline.Remove();
                    polylineList.Remove(p);
                    break;
                }
            }
        }

        public async Task<bool> PointOnPath()
        {
            fabSOS.Visibility = ViewStates.Gone;

            double lat = _map.CameraPosition.Target.Latitude;
            double lng = _map.CameraPosition.Target.Longitude;

            bool geodesic = false;
            double tolerance = 25; // meters

            CircleOptions cO = new CircleOptions();
            cO.InvokeRadius(tolerance / 2);
            cO.InvokeCenter(new LatLng(lat, lng));
            cO.InvokeFillColor(Color.Red);

            if (sosCircle != null)
            {
                sosCircle.Remove();
            }

            sosCircle = _map.AddCircle(cO);

            bool onPath = false;

            for (int i = 0; i < polylineList.Count; i++)
            {
                if (PolyUtil.IsLocationOnPath(new LatLng(lat, lng), polylineList[i].Points, geodesic, tolerance))
                {
                    onPath = true;
                    break;
                }
            }

            await Task.Delay(100);

            fabSOS.Visibility = ViewStates.Visible;

            return onPath;
        }

        // Address
        async void DisplayAddress(Location location)
        {
            Address address = await GoogleLocationsCommon.ReverseGeocodeLocation(location, this.Activity);

            if (address != null)
            {
                StringBuilder deviceAddress = new StringBuilder();

                for (int i = 0; i < address.MaxAddressLineIndex; i++)
                {
                    deviceAddress.AppendLine(address.GetAddressLine(i));
                }
                // Remove the last comma from the end of the address.
                Console.WriteLine(deviceAddress.ToString());
            }
            else
            {
                Console.WriteLine("Unable to determine the address. Try again in a few minutes.");
            }
        }
    }
}