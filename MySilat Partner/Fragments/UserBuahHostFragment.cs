﻿using Android.OS;
using Android.Views;
using MySilatPartner.API;
using v4App = Android.Support.V4.App;

namespace MySilatPartner.Fragments
{
    public class UserBuahHostFragment : v4App.Fragment
    {
        private v4App.FragmentTabHost mTabHost;
        const string TAG = "BuahChecklistFragment";

        public override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View rootView = inflater.Inflate(Resource.Layout.buah_checklist, container, false);

            mTabHost = rootView.FindViewById<v4App.FragmentTabHost>(Resource.Id.tabhost);
            mTabHost.Setup(Activity, ChildFragmentManager, Resource.Id.tabcontent);

            mTabHost.AddTab(mTabHost.NewTabSpec(GetString(Resource.String.asas)).SetIndicator(GetString(Resource.String.asas)),
                Java.Lang.Class.FromType(typeof(UserBuahTabFragment)), null);

            mTabHost.AddTab(mTabHost.NewTabSpec(GetString(Resource.String.jatuh)).SetIndicator(GetString(Resource.String.jatuh)),
                    Java.Lang.Class.FromType(typeof(UserBuahTabFragment)), null);

            mTabHost.AddTab(mTabHost.NewTabSpec(GetString(Resource.String.potong)).SetIndicator(GetString(Resource.String.potong)),
                Java.Lang.Class.FromType(typeof(UserBuahTabFragment)), null);

            mTabHost.AddTab(mTabHost.NewTabSpec(GetString(Resource.String.tamat)).SetIndicator(GetString(Resource.String.tamat)),
                Java.Lang.Class.FromType(typeof(UserBuahTabFragment)), null);
            return rootView;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);

            menu.Clear();
        }
    }
}