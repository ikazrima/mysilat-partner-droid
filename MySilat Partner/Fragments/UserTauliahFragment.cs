﻿using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using v4App = Android.Support.V4.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Fragments
{
    public class UserTauliahFragment : v4App.Fragment
    {
        bool init = false;

        View rootView;

        Spinner kelasSpinner;
        ArrayAdapter kelasAdapter;
        List<int> kelasId = new List<int>();
        List<string> kelasName = new List<string>();

        TauliahList tauliahList;
        TauliahRecyclerAdapter tauliahAdapter;
        v7Widget.RecyclerView tauliahRecycler;
        v7Widget.RecyclerView.LayoutManager tauliahLytMgr;

        ImageButton btnHelpKelas;

        v7Widget.PopupMenu popupTauliah;
        ImageButton btnTauliahMore;

        ProgressBar progressBarTauliah;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            rootView = inflater.Inflate(Resource.Layout.user_tauliah_edit, container, false);

            kelasSpinner = (Spinner)rootView.FindViewById(Resource.Id.kelas_spinner);
            tauliahRecycler = (v7Widget.RecyclerView)rootView.FindViewById(Resource.Id.tauliah_recycler);
            progressBarTauliah = (ProgressBar)rootView.FindViewById(Resource.Id.progressBarTauliah);

            btnHelpKelas = (ImageButton)rootView.FindViewById(Resource.Id.btn_help_kelas);
            btnTauliahMore = (ImageButton)rootView.FindViewById(Resource.Id.btn_tauliah_more);

            ContextThemeWrapper ctw = new ContextThemeWrapper(Context, Resource.Style.PopupMenu);
            popupTauliah = new v7Widget.PopupMenu(ctw, btnTauliahMore, (int)(GravityFlags.Top | GravityFlags.Right));

            MenuInflater popupTauliahInfalter = popupTauliah.MenuInflater;
            popupTauliahInfalter.Inflate(Resource.Menu.others, popupTauliah.Menu);

            return rootView;
        }

        public override async void OnResume()
        {
            base.OnResume();

            if (!init)
            {
                btnHelpKelas.Click -= BtnHelpKelas_Click;
                btnTauliahMore.Click -= BtnTauliahMore_Click;

                Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.loading), Snackbar.LengthIndefinite);
                snackbar.Show();

                progressBarTauliah.Visibility = ViewStates.Visible;
                await InitKelasSpinnerAsync();
                await SetKelasAsync();
                progressBarTauliah.Visibility = ViewStates.Gone;

                snackbar.Dismiss();

                btnHelpKelas.Click += BtnHelpKelas_Click;
                btnTauliahMore.Click += BtnTauliahMore_Click;

                init = true;
            }
        }

        async Task InitKelasSpinnerAsync()
        {
            kelasSpinner.Enabled = false;

            MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);
            List<TauliahModel> result = await MySilatTauliah.GetTauliahUserYear(MySilatProfile.UserProfile.ProfileId, "" + DateTime.Now.Year);

            foreach (var item in result)
            {
                int id;
                int.TryParse(item.KelasId, out id);

                if (!kelasName.Contains(item.KelasCode))
                {
                    kelasId.Add(id);
                    kelasName.Add(item.KelasCode);
                }
            }

            kelasAdapter = new ArrayAdapter<string>(Context, Resource.Layout.my_spinner_left, kelasName);
            kelasAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item_left);
            kelasSpinner.Adapter = kelasAdapter;
            kelasSpinner.ItemSelected += KelasSpinner_ItemSelected;

            int.TryParse(MySilatPersilatan.UserPersilatan.MainKelasId, out int tempId);
            int idPosition = kelasId.FindIndex(x => x == tempId);
            kelasSpinner.SetSelection(idPosition);

            kelasSpinner.Enabled = true;
        }

        async Task SetKelasAsync()
        {
            tauliahList = new TauliahList();
            await tauliahList.GenerateMemberByKelasIdYearTauliahIdAsync(MySilatProfile.UserProfile.ProfileId, DateTime.Now.Year);

            tauliahAdapter = new TauliahRecyclerAdapter(tauliahList);
            tauliahAdapter.NotifyDataSetChanged();
            tauliahAdapter.displayJawatanCard = true;

            tauliahRecycler.SetAdapter(tauliahAdapter);
            tauliahRecycler.HasFixedSize = true;
            tauliahRecycler.SetItemAnimator(new v7Widget.DefaultItemAnimator());

            tauliahLytMgr = new v7Widget.LinearLayoutManager(this.Context, (int)Orientation.Vertical, false);
            tauliahRecycler.SetLayoutManager(tauliahLytMgr);

            v7Widget.DividerItemDecoration dividerItemDecoration = new v7Widget.DividerItemDecoration(Context, v7Widget.DividerItemDecoration.Vertical);
            tauliahRecycler.AddItemDecoration(dividerItemDecoration);

            if (tauliahAdapter.ItemCount > 0)
            {
                tauliahRecycler.Visibility = ViewStates.Visible;
            }
            else
            {
                tauliahRecycler.Visibility = ViewStates.Gone;
            }
        }

        async void UpdateKelasUtama()
        {
            kelasSpinner.Enabled = false;

            Snackbar snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
            snackbar.Show();

            string kelasIdStr = "" + kelasId[kelasSpinner.SelectedItemPosition];

            var result = await MySilatPersilatan.UpdateUserKelasUtama(MySilatProfile.UserProfile.ProfileId, kelasIdStr);
            GenericModel genericObject = JsonConvert.DeserializeObject<GenericModel>(result);

            snackbar.Dismiss();

            if (string.Compare(genericObject.Status, MySilatCommon.SUCCESS) != 0)
            {
                snackbar = Snackbar.Make(rootView, GetString(Resource.String.saving_error), Snackbar.LengthShort);
                snackbar.Show();
            }

            kelasSpinner.Enabled = true;
        }

        private void KelasSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            if (init)
                UpdateKelasUtama();
        }

        private void BtnTauliahMore_Click(object sender, EventArgs e)
        {
            popupTauliah.Show();

            popupTauliah.MenuItemClick +=
                new EventHandler<v7Widget.PopupMenu.MenuItemClickEventArgs>((s, args) =>
                {
                    /// TODO
                });
        }

        private void BtnHelpKelas_Click(object sender, EventArgs e)
        {
            AlertDialog dialog;
            AlertDialog.Builder builder = new AlertDialog.Builder(this.Context);

            builder.SetTitle(GetString(Resource.String.kelas_utama));
            builder.SetMessage(GetString(Resource.String.kelas_utama_notice));
            builder.SetNegativeButton(GetString(Resource.String.ok), (s, args) => { });

            dialog = builder.Create();
            dialog.Show();

            Color backgroundColor = new Color(ContextCompat.GetColor(Context, Resource.Color.SecondaryHeader));
            Color buttonColor = new Color(ContextCompat.GetColor(Context, Resource.Color.LightGrey));
            dialog.Window.DecorView.Background.SetColorFilter(backgroundColor, PorterDuff.Mode.Src);
            dialog.GetButton((int)Android.Content.DialogButtonType.Negative).SetTextColor(buttonColor);
        }
    }
}