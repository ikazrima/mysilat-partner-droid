﻿using Android.Gms.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Firebase.Storage;
using Java.Lang;
using MySilatPartner.API;
using MySilatPartner.JsonModel;
using MySilatPartner.MyCallbacks;
using Square.Picasso;
using System;
using System.Collections.Generic;
using v4App = Android.Support.V4.App;

namespace MySilatPartner.Fragments
{
    public class HomeFragment : v4App.Fragment, IOnSuccessListener, IOnFailureListener
    {
        int thumbDim = 256;
        int resizeDim = 256;

        #region UI
        View rootView;

        ImageView profileImage;
        ImageView profileImagePlaceHolder;

        LinearLayout statusHeader;
        TextView status;

        TextView txtAltName;
        TextView txtFullName;

        TextView txtUserStatus;
        TextView txtUserMainKelas;

        TextView txtTodayDate;

        RecyclerView recyclerKelas;
        RecyclerView.LayoutManager recyclerKelasLytMgr;

        KelasLatihanList kelasList;
        KelasLatihanAdapter kelasAdapter;
        #endregion

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            HasOptionsMenu = false;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            rootView = inflater.Inflate(Resource.Layout.home_page, container, false);

            profileImage = (ImageView)rootView.FindViewById(Resource.Id.profile_image);
            profileImagePlaceHolder = (ImageView)rootView.FindViewById(Resource.Id.profile_image_placeholder);

            statusHeader = (LinearLayout)rootView.FindViewById(Resource.Id.status_header);
            status = (TextView)rootView.FindViewById(Resource.Id.status_text);

            txtAltName = (TextView)rootView.FindViewById(Resource.Id.txtAltName);
            txtFullName = (TextView)rootView.FindViewById(Resource.Id.txtFullName);

            txtUserStatus = (TextView)rootView.FindViewById(Resource.Id.status_text);
            txtUserMainKelas = (TextView)rootView.FindViewById(Resource.Id.main_kelas);

            txtTodayDate = (TextView)rootView.FindViewById(Resource.Id.txtDate);
            txtTodayDate.Text = DateTime.Now.ToString("dddd, dd MMMM");

            recyclerKelas = (RecyclerView)rootView.FindViewById(Resource.Id.kelas_recycler);

            return rootView;
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            LoadProfileImage();

            /*
            if (MySilatProfile.UserProfile != null)
            {
                txtAltName.Text = MySilatProfile.UserProfile.AlternateName;
                txtFullName.Text = MySilatProfile.UserProfile.FirstName + " | " + MySilatProfile.UserProfile.LastName;

                MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);
                List<TauliahModel> userTauliah = await MySilatTauliah.GetTauliahUser(MySilatProfile.UserProfile.ProfileId);

                if (userTauliah != null)
                {
                    foreach (var tauliah in userTauliah)
                    {
                        if (tauliah.Year.Equals(DateTime.Today.Year.ToString()))
                        {
                            txtUserStatus.Visibility = ViewStates.Visible;
                            txtUserStatus.Text = tauliah.Jawatan;
                            txtUserMainKelas.Visibility = ViewStates.Visible;
                            txtUserMainKelas.Text = tauliah.KelasName;

                            switch (tauliah.Code)
                            {
                                default:
                                case "AB":
                                case "PK":
                                    status.SetTextColor(Android.Graphics.Color.Black);
                                    statusHeader.SetBackgroundColor(Android.Graphics.Color.White);
                                    status.Text = tauliah.Jawatan;
                                    break;
                                case "PJP":
                                    status.SetTextColor(Android.Graphics.Color.Black);
                                    statusHeader.SetBackgroundColor(Android.Graphics.Color.White);
                                    status.Text = tauliah.Jawatan;
                                    break;
                                case "PJ":
                                case "J":
                                case "PP":
                                case "P":
                                    status.SetTextColor(Android.Graphics.Color.White);
                                    statusHeader.SetBackgroundColor(Android.Graphics.Color.Red);
                                    status.Text = tauliah.Jawatan;
                                    break;
                                case "GU":
                                    status.SetTextColor(Android.Graphics.Color.Black);
                                    statusHeader.SetBackgroundColor(Android.Graphics.Color.Yellow);
                                    status.Text = tauliah.Jawatan;
                                    break;
                            }

                            break;
                        }
                    }
                }
                else
                {
                    txtUserStatus.Visibility = ViewStates.Gone;
                    txtUserMainKelas.Visibility = ViewStates.Gone;
                }

                kelasList = new KelasLatihanList();
                await kelasList.GenerateUserKelasTodayAsync(MySilatProfile.UserProfile.ProfileId, DateTime.Today);

                kelasAdapter = new KelasLatihanAdapter(kelasList, null);
                kelasAdapter.showDays = false;
                kelasAdapter.showKelasCode = true;
                kelasAdapter.showAttendanceSwitch = true;

                recyclerKelas = (RecyclerView)view.FindViewById(Resource.Id.kelas_recycler);
                recyclerKelasLytMgr = new LinearLayoutManager(this.Context);
                recyclerKelas.SetLayoutManager(recyclerKelasLytMgr);
                recyclerKelas.SetAdapter(kelasAdapter);
            }
            */
        }

        /*
        public override void OnPause()
        {
            base.OnPause();

            try
            {
                Picasso.With(profileImage.Context).CancelRequest(profileImage);
            }
            catch(System.Exception e)
            {
                Console.WriteLine(e);
            }
        }*/

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);

            menu.Clear();
        }

        Android.Gms.Tasks.Task downloadTask;

        void LoadProfileImage()
        {
            if (MySilatProfile.UserProfile != null)
            {
                var item = MySilatProfile.UserProfile;

                int imgId = 0;
                int.TryParse(item.ProfileImgId, out imgId);

                int userId = 0;
                int.TryParse(item.ProfileId, out userId);

                // User uploaded image
                if (imgId > 0)
                {
                    FirebaseMyStorage.Init();
                    StorageReference profileImageRef = FirebaseMyStorage.storageRef.Child("profile/"
                        + MySilatProfile.UserProfile.ProfileId + "/images/thumb@" + thumbDim + MySilatProfile.UserProfile.ProfileImgName);

                    downloadTask = profileImageRef.GetDownloadUrl()
                            .AddOnSuccessListener(this)
                            .AddOnFailureListener(this);
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(item.FacebookId))
                {
                    if (profileImage != null)
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(item.FacebookId, MySilatCommon.IMAGE_LARGE);

                        try
                        {
                            Picasso.With(profileImage.Context).Load(uri).Resize(resizeDim, resizeDim).Into(profileImage);
                        }
                        catch (System.Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        finally
                        {
                            profileImagePlaceHolder.Visibility = ViewStates.Gone;
                            profileImage.Visibility = ViewStates.Visible;
                        }
                    }
                }
                else
                {
                    profileImagePlaceHolder.Visibility = ViewStates.Visible;
                    profileImage.Visibility = ViewStates.Gone;
                }
            }
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            if (downloadTask != null)
            {
                try
                {
                    Android.Net.Uri uri = (downloadTask.Result as Android.Net.Uri);
                    Picasso.With(profileImage.Context).Load(uri).Resize(resizeDim, resizeDim).Into(profileImage);
                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    downloadTask = null;
                }
            }

        }

        public void OnFailure(Java.Lang.Exception e)
        {
            if (downloadTask != null)
            {
                profileImagePlaceHolder.Visibility = ViewStates.Visible;
                profileImage.Visibility = ViewStates.Gone;
                downloadTask = null;
            }
        }
    }
}