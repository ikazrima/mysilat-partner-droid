﻿using Android.Gms.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V4.Graphics.Drawable;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Sfbusyindicator;
using Firebase.Storage;
using MySilatPartner.API;
using MySilatPartner.Models;
using MySilatPartner.MyCallbacks;
using Newtonsoft.Json;
using Syncfusion.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using v4App = Android.Support.V4.App;
using v7Widget = Android.Support.V7.Widget;

namespace MySilatPartner.Fragments
{
    public class UserBuahTabFragment : v4App.Fragment, UserBuahCallback.IAdapterCallback, IOnSuccessListener, IOnFailureListener
    {
        #region Var
        int peringkatId = 1;

        bool editReceived = false;
        DateTime? editReceivedDate = null;
        int editKelasId;
        string editKelasCode;
        int editTpId;
        string editTpName;
        #endregion

        #region UI
        CoordinatorLayout lytCoordinator;
        ConstraintLayout lytConstraint;
        SfBusyIndicator sfBusyBuahList;
        ListView buahListView;
        ListView tpListView;
        #endregion

        #region List/Recycler
        DataSource buahDataSource;
        DataSource tpDataSource;

        BuahList buahList;
        UserProfileList tpList;

        BuahAdapter buahAdapter;
        UserProfileAdapter tpAdapter;

        TauliahList tauliahList;
        TauliahRecyclerAdapter tauliahAdapter;
        #endregion

        #region Buah edit UI
        ConstraintLayout lytReceivedDate;
        ConstraintLayout lytKelas;
        ConstraintLayout lytTenagaPengajar;

        TextView txtReceivedDate;
        TextView txtKelas;
        TextView txtTenagaPengajar;
        Button btnClearReceivedDate;
        Button btnClearKelas;
        Button btnClearTP;
        Button btnEditCancel;
        Button btnEditSave;
        FloatingActionButton fabEdit;
        #endregion

        #region Dialogs
        AlertDialog editDialog;
        AlertDialog tauliahDialog;
        AlertDialog tpDialog;

        SfBusyIndicator sfBusyTauliah;
        v7Widget.RecyclerView tauliahRecycler;
        v7Widget.RecyclerView.LayoutManager tauliahLytMgr;

        TextView tp_txtTitle;
        TextView tp_txtSubtitle;
        #endregion

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            if (string.Compare(this.Tag, GetString(Resource.String.asas)) == 0)
            {
                peringkatId = 1;
            }

            else if (string.Compare(this.Tag, GetString(Resource.String.jatuh)) == 0)
            {
                peringkatId = 2;
            }

            else if (string.Compare(this.Tag, GetString(Resource.String.potong)) == 0)
            {
                peringkatId = 3;
            }

            else if (string.Compare(this.Tag, GetString(Resource.String.tamat)) == 0)
            {
                peringkatId = 4;
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.Inflate(Resource.Layout.user_buah_checklist, container, false);

            lytCoordinator = (CoordinatorLayout)rootView.FindViewById(Resource.Id.lyt_coordinator);
            lytConstraint = (ConstraintLayout)rootView.FindViewById(Resource.Id.lyt_user_buah);

            sfBusyBuahList = (SfBusyIndicator)lytCoordinator.FindViewById(Resource.Id.busyIndicator);
            sfBusyBuahList.SetBackgroundColor(Color.Transparent);
            sfBusyBuahList.TextColor = new Color(ContextCompat.GetColor(this.Activity, Resource.Color.Accent));

            fabEdit = (FloatingActionButton)lytCoordinator.FindViewById(Resource.Id.fab_edit);
            fabEdit.Click += FabEdit_Click;

            return rootView;
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            buahList = new BuahList();

            buahDataSource = new DataSource();
            buahDataSource.Source = buahList;
            buahDataSource.SortDescriptors.Add(new SortDescriptor("BuahId"));

            buahAdapter = new BuahAdapter(buahDataSource, this.Activity, this);

            buahListView = (ListView)lytConstraint.FindViewById(Resource.Id.buah_list_view);
            buahListView.Adapter = buahAdapter;
            buahListView.ScrollingCacheEnabled = false;

            await buahList.GenerateUserBuahAsync(peringkatId);

            sfBusyBuahList.Visibility = ViewStates.Gone;
            buahListView.Visibility = ViewStates.Visible;

            for (int i = 0; i < buahAdapter.Count; i++)
            {
                buahAdapter.selected.Add(false);
            }

            buahAdapter.peringkatId = peringkatId;
            buahAdapter.NotifyDataSetChanged();

            if (MySilatPersilatan.UserPersilatan != null)
            {
                // int.TryParse(MySilatPersilatan.UserPersilatan.MainKelasId, out editKelasId);

                if (MySilatPersilatan.UserPersilatan.PeringkatId < peringkatId)
                {
                    Snackbar
                        .Make(lytCoordinator, GetString(Resource.String.level_not_reached), Snackbar.LengthIndefinite)
                        .Show();
                }
            }
        }

        private void FabEdit_Click(object sender, EventArgs e)
        {
            AlertDialog.Builder editBuilder = new AlertDialog.Builder(this.Context);

            View dialogView = this.Activity.LayoutInflater.Inflate(Resource.Layout.user_buah_edit, null);
            editBuilder.SetView(dialogView);

            TextView edit_txtTitle = (TextView)dialogView.FindViewById(Resource.Id.txt_title);
            TextView edit_txtSubtitle = (TextView)dialogView.FindViewById(Resource.Id.txt_sub_title);

            Switch switchReceived = (Switch)dialogView.FindViewById(Resource.Id.switch_terima);
            switchReceived.Checked = editReceived;

            lytReceivedDate = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_tarikh_terima);
            txtReceivedDate = (TextView)dialogView.FindViewById(Resource.Id.txt_tarikh_terima);

            lytKelas = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_kelas);
            txtKelas = (TextView)dialogView.FindViewById(Resource.Id.txt_kelas);

            lytTenagaPengajar = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_tenaga_pengajar);
            txtTenagaPengajar = (TextView)dialogView.FindViewById(Resource.Id.txt_tenaga_pengajar);

            btnClearReceivedDate = (Button)lytReceivedDate.FindViewById(Resource.Id.btn_clear_tarikh_terima);
            btnClearKelas = (Button)lytKelas.FindViewById(Resource.Id.btn_clear_kelas);
            btnClearTP = (Button)lytTenagaPengajar.FindViewById(Resource.Id.btn_clear_tp);

            btnEditCancel = (Button)dialogView.FindViewById(Resource.Id.btn_cancel);
            btnEditSave = (Button)dialogView.FindViewById(Resource.Id.btn_save);

            if (editReceivedDate.HasValue)
                txtReceivedDate.Text = editReceivedDate.Value.ToString("dd MMMM yyyy");
            else
                txtReceivedDate.Text = "";

            txtKelas.Text = editKelasCode;
            txtTenagaPengajar.Text = editTpName;

            for (int i = buahAdapter.Count - 1; i >= 0; i--)
            {
                if (buahAdapter.selected[i] == true)
                {
                    editReceived = (buahAdapter[i] as Buah).Received;
                    switchReceived.Checked = editReceived;

                    if (editReceived)
                    {
                        editReceivedDate = (buahAdapter[i] as Buah).ReceivedDt;

                        if (editReceivedDate.HasValue)
                            txtReceivedDate.Text = editReceivedDate.Value.ToString("dd MMMM yyyy");
                        else
                            txtReceivedDate.Text = "";

                        editKelasId = (buahAdapter[i] as Buah).KelasId;
                        editKelasCode = (buahAdapter[i] as Buah).KelasCode;
                        txtKelas.Text = editKelasCode;

                        editTpId = (buahAdapter[i] as Buah).JurulatihId;
                        editTpName = (buahAdapter[i] as Buah).JurulatihName;
                        txtTenagaPengajar.Text = editTpName;
                    }

                    break;
                }
            }

            if (!editReceived)
            {
                lytReceivedDate.Enabled = false;
                lytKelas.Enabled = false;
                lytTenagaPengajar.Enabled = false;

                txtReceivedDate.Visibility = ViewStates.Gone;
                txtKelas.Visibility = ViewStates.Gone;
                txtTenagaPengajar.Visibility = ViewStates.Gone;

                btnClearReceivedDate.Visibility = ViewStates.Gone;
                btnClearKelas.Visibility = ViewStates.Gone;
                btnClearTP.Visibility = ViewStates.Gone;
            }

            else
            {
                lytReceivedDate.Enabled = true;
                lytKelas.Enabled = true;
                lytTenagaPengajar.Enabled = true;

                txtReceivedDate.Visibility = ViewStates.Visible;
                txtKelas.Visibility = ViewStates.Visible;
                txtTenagaPengajar.Visibility = ViewStates.Visible;

                if (editReceivedDate.HasValue)
                    btnClearReceivedDate.Visibility = ViewStates.Visible;
                else
                    btnClearReceivedDate.Visibility = ViewStates.Gone;

                if (editKelasId > 0)
                    btnClearKelas.Visibility = ViewStates.Visible;
                else
                {
                    lytTenagaPengajar.Enabled = false;
                    txtTenagaPengajar.Text = "";

                    btnClearKelas.Visibility = ViewStates.Gone;
                    btnClearTP.Visibility = ViewStates.Gone;
                }

                if (editTpId > 0 && editKelasId > 0)
                    btnClearTP.Visibility = ViewStates.Visible;
                else
                {
                    lytTenagaPengajar.Enabled = false;
                    txtTenagaPengajar.Text = "";

                    btnClearTP.Visibility = ViewStates.Gone;
                }
            }

            switchReceived.Click += SwitchReceived_Click;

            if (!lytReceivedDate.HasOnClickListeners)
            {
                lytReceivedDate.Click += SetReceivedDate;
                lytKelas.Click += new EventHandler((s, args) => { SetKelasAsync(); });
                lytTenagaPengajar.Click += SetTenagaPengajarAsync;

                btnClearReceivedDate.Click += ClearReceivedDateEditField;
                btnClearKelas.Click += ClearKelasEditField;
                btnClearTP.Click += ClearTPEditField;

                btnEditCancel.Click += BtnEditCancel_Click;
                btnEditSave.Click += BtnEditSave_Click;
            }

            edit_txtTitle.Text = this.Tag;
            edit_txtSubtitle.Text = buahAdapter.SelectedCount() + " " + GetString(Resource.String.buah_selected);

            editDialog = editBuilder.Create();
            editDialog.Show();
        }

        private void BtnEditSave_Click(object sender, EventArgs e)
        {
            editDialog.Dismiss();
            SaveBuahEditAsync();
        }

        private void BtnEditCancel_Click(object sender, EventArgs e)
        {
            editDialog.Dismiss();
        }

        private void SwitchReceived_Click(object sender, EventArgs e)
        {
            editReceived = (sender as Switch).Checked;

            if (!editReceived)
            {
                lytReceivedDate.Enabled = false;
                lytKelas.Enabled = false;
                lytTenagaPengajar.Enabled = false;

                txtReceivedDate.Visibility = ViewStates.Gone;
                txtKelas.Visibility = ViewStates.Gone;
                txtTenagaPengajar.Visibility = ViewStates.Gone;

                btnClearReceivedDate.Visibility = ViewStates.Gone;
                btnClearKelas.Visibility = ViewStates.Gone;
                btnClearTP.Visibility = ViewStates.Gone;
            }
            else
            {
                lytReceivedDate.Enabled = true;
                lytKelas.Enabled = true;
                lytTenagaPengajar.Enabled = true;

                txtReceivedDate.Visibility = ViewStates.Visible;
                txtKelas.Visibility = ViewStates.Visible;
                txtTenagaPengajar.Visibility = ViewStates.Visible;

                if (editReceivedDate.HasValue)
                    btnClearReceivedDate.Visibility = ViewStates.Visible;
                else
                    btnClearReceivedDate.Visibility = ViewStates.Gone;

                if (editKelasId > 0)
                    btnClearKelas.Visibility = ViewStates.Visible;
                else
                {
                    lytTenagaPengajar.Enabled = false;
                    txtTenagaPengajar.Text = "";
                    txtTenagaPengajar.Visibility = ViewStates.Gone;

                    btnClearKelas.Visibility = ViewStates.Gone;
                    btnClearTP.Visibility = ViewStates.Gone;
                }

                if (editTpId > 0 && editKelasId > 0)
                    btnClearTP.Visibility = ViewStates.Visible;
                else
                {
                    lytTenagaPengajar.Enabled = false;
                    txtTenagaPengajar.Text = "";
                    txtTenagaPengajar.Visibility = ViewStates.Gone;

                    btnClearTP.Visibility = ViewStates.Gone;
                }
            }
        }

        private void ClearReceivedDateEditField(object sender, EventArgs e)
        {
            btnClearReceivedDate.Visibility = ViewStates.Gone;

            txtReceivedDate.Text = "";
            editReceivedDate = null;
        }

        private void ClearKelasEditField(object sender, EventArgs e)
        {
            btnClearKelas.Visibility = ViewStates.Gone;
            btnClearTP.Visibility = ViewStates.Gone;

            txtTenagaPengajar.Visibility = ViewStates.Gone;
            lytTenagaPengajar.Enabled = false;

            txtKelas.Text = "";
            editKelasId = 0;

            txtTenagaPengajar.Text = "";
            editTpName = "";
            editTpId = 0;
        }

        private void ClearTPEditField(object sender, EventArgs e)
        {
            btnClearTP.Visibility = ViewStates.Gone;

            txtTenagaPengajar.Text = "";
            editTpName = "";
            editTpId = 0;
        }

        private async void SetKelasAsync()
        {
            View dialogView = this.Activity.LayoutInflater.Inflate(Resource.Layout.recycler_generic_dialog, null);

            ConstraintLayout lytContent = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_content);
            lytContent.Visibility = ViewStates.Gone;

            v7Widget.SearchView tauliahSearchView = (v7Widget.SearchView)dialogView.FindViewById(Resource.Id.search);

            TextView tauliah_txtTitle = (TextView)dialogView.FindViewById(Resource.Id.txt_title);
            TextView tauliah_txtSubtitle = (TextView)dialogView.FindViewById(Resource.Id.txt_sub_title);

            tauliahSearchView.QueryHint = GetString(Resource.String.hint_kelas);

            tauliahSearchView.QueryTextChange += new EventHandler<v7Widget.SearchView.QueryTextChangeEventArgs>((s, args) =>
            {
                tauliahAdapter.Filter((s as v7Widget.SearchView).Query);
                tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();
            });

            tauliahSearchView.QueryTextSubmit += new EventHandler<v7Widget.SearchView.QueryTextSubmitEventArgs>((s, args) =>
            {
                tauliahAdapter.Filter((s as v7Widget.SearchView).Query);
                tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();
            });

            tauliahSearchView.Visibility = ViewStates.Gone;

            TextView txtWarning = (TextView)dialogView.FindViewById(Resource.Id.txt_warning);

            ImageView searchIcon = (ImageView)tauliahSearchView.FindViewById(Resource.Id.search_button);
            searchIcon.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.ic_search_white_24dp));

            DrawableCompat.SetTint(
                DrawableCompat.Wrap(searchIcon.Drawable),
                ContextCompat.GetColor(this.Context, Resource.Color.White)
            );

            v7Widget.SearchView.SearchAutoComplete searchAutoComplete = (v7Widget.SearchView.SearchAutoComplete)tauliahSearchView.FindViewById(Resource.Id.search_src_text);
            searchAutoComplete.SetHint(Resource.String.hint_kelas);
            searchAutoComplete.SetHintTextColor(Color.White);
            searchAutoComplete.SetTextColor(Color.White);

            sfBusyTauliah = (SfBusyIndicator)dialogView.FindViewById(Resource.Id.busyIndicator);
            sfBusyTauliah.SetBackgroundColor(Color.Transparent);
            sfBusyTauliah.TextColor = new Color(ContextCompat.GetColor(this.Activity, Resource.Color.Accent));

            Button btnSave = (Button)dialogView.FindViewById(Resource.Id.btn_save);
            btnSave.Visibility = ViewStates.Gone;

            Button btnCancel = (Button)dialogView.FindViewById(Resource.Id.btn_cancel);
            btnCancel.Click += new EventHandler((s, args) => { tauliahDialog.Dismiss(); });

            AlertDialog.Builder tauliahBuilder = new AlertDialog.Builder(this.Context);
            tauliahBuilder.SetView(dialogView);
            tauliahDialog = tauliahBuilder.Create();
            tauliahDialog.Show();

            tauliahList = new TauliahList();
            await tauliahList.GenerateMemberByKelasIdYearTauliahIdAsync(MySilatProfile.UserProfile.ProfileId, DateTime.Now.Year);

            lytContent.Visibility = ViewStates.Visible;
            lytContent.Visibility = ViewStates.Visible;

            tauliahLytMgr = new v7Widget.LinearLayoutManager(this.Context, (int)Orientation.Vertical, false);

            tauliahAdapter = new TauliahRecyclerAdapter(tauliahList);
            tauliahAdapter.ItemClick += OnKelasItemClickAsync;
            tauliahAdapter.NotifyDataSetChanged();

            tauliahRecycler = (v7Widget.RecyclerView)dialogView.FindViewById(Resource.Id.content_recycler);
            tauliahRecycler.SetAdapter(tauliahAdapter);
            tauliahRecycler.HasFixedSize = true;
            tauliahRecycler.SetItemAnimator(new DefaultItemAnimator());
            tauliahRecycler.SetLayoutManager(tauliahLytMgr);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(Context, DividerItemDecoration.Vertical);
            tauliahRecycler.AddItemDecoration(dividerItemDecoration);

            tauliah_txtTitle.Text = GetString(Resource.String.kelas);
            tauliah_txtSubtitle.Text = tauliahAdapter.ItemCount + " " + GetString(Resource.String.kelas).ToLower();

            sfBusyTauliah.Visibility = ViewStates.Gone;

            if (tauliahAdapter.ItemCount > 0)
            {
                tauliahSearchView.Visibility = ViewStates.Visible;
                tauliahRecycler.Visibility = ViewStates.Visible;
                txtWarning.Visibility = ViewStates.Gone;
            }
            else
            {
                tauliahSearchView.Visibility = ViewStates.Gone;
                tauliahRecycler.Visibility = ViewStates.Gone;
                txtWarning.Visibility = ViewStates.Visible;
            }
        }

        private void OnKelasItemClickAsync(object sender, int position)
        {
            if (sender is TauliahRecyclerAdapter)
            {
                txtKelas.Text = tauliahList[position].KelasCode;
                editKelasCode = tauliahList[position].KelasCode;
                int.TryParse(tauliahList[position].KelasId, out editKelasId);

                txtTenagaPengajar.Visibility = ViewStates.Visible;
                lytTenagaPengajar.Enabled = true;
                btnClearKelas.Visibility = ViewStates.Visible;

                editTpId = 0;
                editTpName = "";
                txtTenagaPengajar.Text = "";

                tauliahDialog.Dismiss();
            }
        }

        private async void SetTenagaPengajarAsync(object sender, EventArgs e)
        {
            View dialogView = this.Activity.LayoutInflater.Inflate(Resource.Layout.listview_generic_dialog, null);

            v7Widget.SearchView tpSearchView = (v7Widget.SearchView)dialogView.FindViewById(Resource.Id.search);
            tpSearchView.QueryTextChange += TPSearchView_QueryTextChange;
            tpSearchView.QueryTextSubmit += TPSearchView_QueryTextSubmit;
            tpSearchView.Visibility = ViewStates.Gone;

            ImageView searchIcon = (ImageView)tpSearchView.FindViewById(Resource.Id.search_button);
            searchIcon.SetImageDrawable(ContextCompat.GetDrawable(this.Context, Resource.Drawable.ic_search_white_24dp));

            DrawableCompat.SetTint(
                DrawableCompat.Wrap(searchIcon.Drawable),
                ContextCompat.GetColor(this.Context, Resource.Color.White)
            );

            v7Widget.SearchView.SearchAutoComplete searchAutoComplete = (v7Widget.SearchView.SearchAutoComplete)tpSearchView.FindViewById(Resource.Id.search_src_text);
            searchAutoComplete.SetHint(Resource.String.search_hint);
            searchAutoComplete.SetHintTextColor(Color.White);
            searchAutoComplete.SetTextColor(Color.White);

            ConstraintLayout lytContent = (ConstraintLayout)dialogView.FindViewById(Resource.Id.lyt_content);
            lytContent.Visibility = ViewStates.Gone;

            tp_txtTitle = (TextView)dialogView.FindViewById(Resource.Id.txt_title);
            tp_txtSubtitle = (TextView)dialogView.FindViewById(Resource.Id.txt_sub_title);

            if (tpList == null)
            {
                tpList = new UserProfileList();

                tpDataSource = new DataSource();
                tpDataSource.Source = tpList;

                tpAdapter = new UserProfileAdapter(tpDataSource, this.Activity, this);
            }

            if (tpList.Count > 0)
                tpList.Clear();

            tpListView = (ListView)dialogView.FindViewById(Resource.Id.content_listview);
            TextView tpWarning = (TextView)dialogView.FindViewById(Resource.Id.txt_warning);

            SfBusyIndicator sfBusyTP = (SfBusyIndicator)dialogView.FindViewById(Resource.Id.busyIndicator);
            sfBusyTP.SetBackgroundColor(Color.Transparent);
            sfBusyTP.TextColor = new Color(ContextCompat.GetColor(this.Activity, Resource.Color.Accent));

            tpListView.Adapter = tpAdapter;

            AlertDialog.Builder tpBuilder = new AlertDialog.Builder(this.Context);
            tpBuilder.SetView(dialogView);
            tpDialog = tpBuilder.Create();
            tpDialog.Show();

            Button btnSave = (Button)dialogView.FindViewById(Resource.Id.btn_save);
            btnSave.Visibility = ViewStates.Gone;

            Button btnCancel = (Button)dialogView.FindViewById(Resource.Id.btn_cancel);
            btnCancel.Click += new EventHandler((s, args) => { tpDialog.Dismiss(); });

            int[] tpTauliahId = new int[] { 2, 3, 4, 5, 6, 7 };
            string tpTauliahIdJson = JsonConvert.SerializeObject(tpTauliahId, Formatting.Indented);

            await tpList.GenerateMemberByKelasIdYearTauliahIdAsync(editKelasId + "", DateTime.Now, tpTauliahIdJson);

            #region Remove user from list
            int userIndex = -1;

            for (int i = 0; i < tpList.Count; i++)
            {
                if (tpList[i].ProfileId == MySilatProfile.UserProfile.ProfileId)
                {
                    userIndex = i;
                    break;
                }
            }

            if (userIndex != -1)
            {
                tpList.RemoveAt(userIndex);
            }
            #endregion

            tpAdapter.NotifyDataSetChanged();

            sfBusyTP.Visibility = ViewStates.Gone;
            tpSearchView.Visibility = ViewStates.Visible;

            tp_txtTitle.Text = GetString(Resource.String.tp);
            tp_txtSubtitle.Text = tpAdapter.Count + " " + GetString(Resource.String.orang).ToLower();

            lytContent.Visibility = ViewStates.Visible;

            if (tpAdapter.Count > 0)
            {
                tpListView.Visibility = ViewStates.Visible;
                tpWarning.Visibility = ViewStates.Gone;
            }
            else
            {
                tpListView.Visibility = ViewStates.Gone;
                tpWarning.Visibility = ViewStates.Visible;
            }

            LoadProfileImageUri();
        }

        private void TPSearchView_QueryTextSubmit(object sender, v7Widget.SearchView.QueryTextSubmitEventArgs e)
        {
            tpAdapter.Filter((sender as v7Widget.SearchView).Query);
            tp_txtSubtitle.Text = tpAdapter.Count + " " + GetString(Resource.String.orang).ToLower();
        }

        private void TPSearchView_QueryTextChange(object sender, v7Widget.SearchView.QueryTextChangeEventArgs e)
        {
            tpAdapter.Filter((sender as v7Widget.SearchView).Query);
            tp_txtSubtitle.Text = tpAdapter.Count + " " + GetString(Resource.String.orang).ToLower();
        }

        private void SetReceivedDate(object sender, EventArgs e)
        {
            DatePickerFragment frag = DatePickerFragment.NewInstance(delegate (DateTime date)
            {
                if (date > DateTime.Today)
                    date = DateTime.Today;

                editReceivedDate = date;
                txtReceivedDate.Text = editReceivedDate.Value.ToString("dd MMMM yyyy");
                btnClearReceivedDate.Visibility = ViewStates.Visible;
            });

            frag.Show(Activity.FragmentManager, DatePickerFragment.TAG);
        }

        private async void SaveBuahEditAsync()
        {
            if (MySilatPersilatan.UserPersilatan != null)
            {
                if (MySilatPersilatan.UserPersilatan.PeringkatId >= peringkatId)
                {
                    try
                    {
                        for (int i = 0; i < buahAdapter.selected.Count; i++)
                        {
                            if (buahAdapter.selected[i] == true)
                            {
                                (buahAdapter[i] as Buah).Received = editReceived;
                                (buahAdapter[i] as Buah).ReceivedDt = editReceivedDate;
                                (buahAdapter[i] as Buah).KelasId = editKelasId;
                                (buahAdapter[i] as Buah).KelasCode = editKelasCode;
                                (buahAdapter[i] as Buah).JurulatihId = editTpId;
                                (buahAdapter[i] as Buah).JurulatihName = editTpName;
                            }
                        }

                        buahListView.Visibility = ViewStates.Gone;
                        sfBusyBuahList.Visibility = ViewStates.Visible;

                        await MySilatBuah.SaveUserBuah(buahAdapter, peringkatId);

                        buahList.Clear();
                        await buahList.GenerateUserBuahAsync(peringkatId);

                        for (int i = 0; i < buahAdapter.selected.Count; i++)
                        {
                            buahAdapter.selected[i] = false;
                        }

                        HideFabEdit(buahAdapter.SelectedCount());
                        buahAdapter.NotifyDataSetChanged();

                        buahListView.Visibility = ViewStates.Visible;
                        sfBusyBuahList.Visibility = ViewStates.Gone;

                        Snackbar
                                .Make(lytCoordinator, GetString(Resource.String.saved), Snackbar.LengthShort)
                                .Show();

                    }
                    catch (Exception ex)
                    {
                        Snackbar
                            .Make(lytCoordinator, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                            .Show();

                        Console.WriteLine(GetString(Resource.String.error) + " : " + ex.ToString());
                    }
                }
            }
        }

        public void OnUserBuahSelectedChange(int count)
        {
            HideFabEdit(count);
        }

        public void OnTenagaPengajarClick(int position)
        {
            int.TryParse((tpAdapter[position] as UserProfile).ProfileId, out editTpId);
            editTpName = (tpAdapter[position] as UserProfile).AlternateName;

            txtTenagaPengajar.Text = editTpName;

            tpDialog.Dismiss();

            btnClearTP.Visibility = ViewStates.Visible;
        }

        private void HideFabEdit(int count)
        {
            if (count > 0)
                fabEdit.Visibility = ViewStates.Visible;
            else
                fabEdit.Visibility = ViewStates.Gone;

            lytConstraint.RefreshDrawableState();
        }

        #region Profile Image / Firebase Storage
        void LoadProfileImageUri()
        {
            if (downloadTaskDict != null)
                downloadTaskDict.Clear();

            if (tpAdapter.imgUri.Count > 0)
                tpAdapter.imgUri.Clear();

            for (int index = 0; index < tpList.Count; index++)
            {
                var item = tpList[index];

                int imgId = 0;
                int.TryParse(item.ProfileImageId, out imgId);

                int userId = 0;
                int.TryParse(item.ProfileId, out userId);

                if (tpAdapter.imgUri.ContainsKey(userId))
                    continue;

                // User uploaded image
                if (imgId > 0)
                {
                    if (!tpAdapter.imgUri.ContainsKey(userId))
                    {
                        tpAdapter.imgUri.Add(userId, Android.Net.Uri.Empty);
                        GetImageThumbUri(item.ProfileId, item.ProfileImageName, thumbSize);
                    }
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(item.FacebookId))
                {
                    if (!tpAdapter.imgUri.ContainsKey(userId))
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(item.FacebookId, MySilatCommon.IMAGE_NORMAL);
                        tpAdapter.imgUri.Add(userId, uri);
                    }
                }
            }
        }

        int thumbSize = 128;

        Dictionary<int, Android.Gms.Tasks.Task> downloadTaskDict = new Dictionary<int, Android.Gms.Tasks.Task>();
        List<int> uriUserId = new List<int>();

        void GetImageThumbUri(string userId, string imgName, int thumbDim)
        {
            FirebaseMyStorage.Init();
            StorageReference profileImageRef = FirebaseMyStorage.storageRef.Child("profile/" + userId + "/images/thumb@" + thumbDim + imgName);

            int userIdInt = 0;
            int.TryParse(userId, out userIdInt);

            if (!downloadTaskDict.ContainsKey(userIdInt))
            {
                downloadTaskDict.Add(userIdInt, profileImageRef.GetDownloadUrl()
                            .AddOnSuccessListener(this)
                            .AddOnFailureListener(this));
            }
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                int userId = downloadTaskDict.ElementAt(i).Key;
                Android.Gms.Tasks.Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (!task.IsSuccessful)
                    continue;

                Android.Net.Uri path = (task.Result as Android.Net.Uri);

                tpAdapter.imgUri[userId] = path;
                tpAdapter.NotifyDataSetChanged();

                downloadTaskDict[userId] = null;
                break;
            }
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                Android.Gms.Tasks.Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (task.IsSuccessful)
                    continue;

                int key = downloadTaskDict.ElementAt(i).Key;
                downloadTaskDict[key] = null;
            }

            Console.WriteLine(e);
        }
    }
    #endregion
}