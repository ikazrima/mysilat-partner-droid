﻿using Android.Gms.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Support.Constraints;
using Android.Support.Design.Widget;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Syncfusion.Sfbusyindicator;
using Firebase.Storage;
using MySilatPartner.API;
using MySilatPartner.MyCallbacks;
using Syncfusion.DataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using v4App = Android.Support.V4.App;
using v7App = Android.Support.V7.App;

namespace MySilatPartner.Fragments
{
    public class JurulatihApprovalFragment : v4App.Fragment, JurulatihApprovalCallback.IAdapterCallback, IOnSuccessListener, IOnFailureListener
    {
        DataSource approvalDataSource;
        JurulatihApprovalList approvalList;
        JurulatihApprovalRecyclerAdapter approvalAdapter;
        RecyclerView.LayoutManager approvalLytMgr;
        RecyclerView approvalRecycler;
        ArrayAdapter kelasCodeAdapter;

        #region UI
        CoordinatorLayout lytCoordinator;
        ConstraintLayout lytHeader;

        SfBusyIndicator sfBusy;
        LinearLayout lytNoContent;

        TextView txtTitle;
        TextView txtSubtitle;
        Spinner kelasCodeSpinner;

        LinearLayout fabLyt;
        FloatingActionButton fabDismiss;
        FloatingActionButton fabApprove;
        #endregion

        const string TAG = "JurulatihApprovalFragment";

        public override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            RetainInstance = true;

            MySilatPersilatan.UserPersilatan = await MySilatPersilatan.GetPersilatanByUserId(MySilatProfile.UserProfile.ProfileId);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            HasOptionsMenu = true;

            View rootView = inflater.Inflate(Resource.Layout.jurulatih_approval_checklist, container, false);

            lytCoordinator = (CoordinatorLayout)rootView.FindViewById(Resource.Id.lyt_coordinator);
            //lytApproval = (ConstraintLayout)rootView.FindViewById(Resource.Id.lyt_approval);
            lytHeader = (ConstraintLayout)rootView.FindViewById(Resource.Id.header);

            txtTitle = (TextView)lytCoordinator.FindViewById(Resource.Id.txt_title);
            txtSubtitle = (TextView)lytCoordinator.FindViewById(Resource.Id.txt_sub_title);
            kelasCodeSpinner = (Spinner)lytCoordinator.FindViewById(Resource.Id.kelas_spinner);

            sfBusy = (SfBusyIndicator)lytCoordinator.FindViewById(Resource.Id.busyIndicator);
            sfBusy.SetBackgroundColor(Color.Transparent);
            sfBusy.TextColor = new Color(ContextCompat.GetColor(this.Activity, Resource.Color.Accent));

            lytNoContent = (LinearLayout)rootView.FindViewById(Resource.Id.lyt_no_content);

            fabLyt = (LinearLayout)lytCoordinator.FindViewById(Resource.Id.fab_lyt);
            fabDismiss = (FloatingActionButton)lytCoordinator.FindViewById(Resource.Id.fab_dismiss);
            fabDismiss.Click += BtnDismiss_Click;
            fabApprove = (FloatingActionButton)lytCoordinator.FindViewById(Resource.Id.fab_approve);
            fabApprove.Click += BtnVerify_Click;

            lytHeader.Visibility = ViewStates.Gone;
            fabLyt.Visibility = ViewStates.Gone;
            sfBusy.Visibility = ViewStates.Visible;
            lytNoContent.Visibility = ViewStates.Gone;

            return rootView;
        }

        public override async void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            txtTitle.Text = GetString(Resource.String.application);

            approvalList = new JurulatihApprovalList();

            approvalDataSource = new DataSource();
            approvalDataSource.Source = approvalList;
            approvalDataSource.SortDescriptors.Add(new SortDescriptor("ReceivedDt"));

            approvalAdapter = new JurulatihApprovalRecyclerAdapter(approvalList, this);
            await approvalList.GenerateApprovalListAsync();
            approvalAdapter.NotifyDataSetChanged();

            approvalRecycler = (RecyclerView)lytCoordinator.FindViewById(Resource.Id.content_recycler);
            approvalRecycler.SetAdapter(approvalAdapter);

            approvalLytMgr = new LinearLayoutManager(this.Context, (int)Orientation.Vertical, false);
            approvalRecycler.SetLayoutManager(approvalLytMgr);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(Context, DividerItemDecoration.Vertical);
            approvalRecycler.AddItemDecoration(dividerItemDecoration);

            LoadProfileImageUri();

            kelasCodeAdapter = new ArrayAdapter<string>(this.Context, Resource.Layout.my_spinner, approvalList.kelasCodeList);
            kelasCodeAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item);
            kelasCodeSpinner.Adapter = kelasCodeAdapter;
            kelasCodeSpinner.ItemSelected += KelasCodeSpinner_ItemSelected;

            lytHeader.Visibility = ViewStates.Visible;
            sfBusy.Visibility = ViewStates.Gone;
            approvalRecycler.Visibility = ViewStates.Visible;

            PopulateApprovalAdapterSelected();

            CheckRequestDisplay();
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            base.OnCreateOptionsMenu(menu, inflater);

            menu.Clear();
            inflater.Inflate(Resource.Menu.toolbar_menu, menu);

            IMenuItem itemSwitch = menu.FindItem(Resource.Id.menu_switch);
            IMenuItem itemEdit = menu.FindItem(Resource.Id.menu_edit);
            IMenuItem itemMore = menu.FindItem(Resource.Id.menu_more);

            itemSwitch.SetVisible(false);
            itemEdit.SetVisible(false);
            itemMore.SetVisible(false);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_refresh:
                    var task = RefreshAsync();
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

        private async System.Threading.Tasks.Task RefreshAsync()
        {
            fabLyt.Visibility = ViewStates.Gone;
            lytHeader.Visibility = ViewStates.Gone;
            sfBusy.Visibility = ViewStates.Visible;
            kelasCodeSpinner.Visibility = ViewStates.Gone;
            approvalRecycler.Visibility = ViewStates.Gone;

            await approvalList.GenerateApprovalListAsync();
            LoadProfileImageUri();

            lytHeader.Visibility = ViewStates.Visible;
            sfBusy.Visibility = ViewStates.Gone;
            approvalRecycler.Visibility = ViewStates.Visible;
            PopulateApprovalAdapterSelected();
            CheckRequestDisplay();
        }

        private void KelasCodeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            // Filter approval adapter

            if (e.Position == 0)
            {
                approvalList.GenerateUnfilteredApprovalList(false);
            }
            else
            {
                approvalList.GenerateFilteredApprovalList(kelasCodeSpinner.SelectedItem.ToString());
            }

            PopulateApprovalAdapterSelected();
            RefreshTopCard(0);
            approvalAdapter.DeselectAll();
        }

        private void BtnDismiss_Click(object sender, EventArgs e)
        {
            Color backgroundColor = new Color(ContextCompat.GetColor(Context, Resource.Color.SecondaryHeader));
            Color warningColor = new Color(ContextCompat.GetColor(Context, Resource.Color.Warning));

            v7App.AlertDialog.Builder dismissBuilder = new v7App.AlertDialog.Builder(this.Context);
            dismissBuilder.SetNegativeButton(GetString(Resource.String.cancel), (s, args) => { });
            dismissBuilder.SetPositiveButton(GetString(Resource.String.ok), (s, args) =>
            {
                Approve(0);
            });

            v7App.AlertDialog dismissDialog;
            dismissDialog = dismissBuilder.Create();
            dismissDialog.SetMessage(GetString(Resource.String.resubmit_buah_application));
            dismissDialog.SetTitle(GetString(Resource.String.cancel_application));
            dismissDialog.Show();
            dismissDialog.Window.DecorView.Background.SetColorFilter(backgroundColor, PorterDuff.Mode.Src);
            //dismissDialog.GetButton((int)v7App.AlertDialog.InterfaceConsts.ButtonNegative).SetTextColor(warningColor);
            dismissDialog.GetButton((int)Android.Content.DialogButtonType.Negative).SetTextColor(warningColor);
        }

        private void BtnVerify_Click(object sender, EventArgs e)
        {
            Approve(1);
        }

        private async void Approve(int approvalStatus)
        {
            if (approvalAdapter.SelectedCount() > 0)
            {
                try
                {
                    Snackbar snackbar = Snackbar.Make(lytCoordinator, GetString(Resource.String.saving), Snackbar.LengthIndefinite);
                    snackbar.Show();

                    sfBusy.Visibility = ViewStates.Visible;
                    kelasCodeSpinner.Visibility = ViewStates.Gone;
                    approvalRecycler.Visibility = ViewStates.Gone;

                    List<int> user_buah_id = new List<int>();
                    List<string> buah_id = new List<string>();

                    for (int i = 0; i < approvalAdapter.selected.Count; i++)
                    {
                        if (approvalAdapter.selected[i] == true)
                        {
                            user_buah_id.Add(approvalList[i].UserBuahId);
                            buah_id.Add(approvalList[i].BuahId);
                        }
                    }

                    await MySilatBuah.SaveJurulatihApproval(user_buah_id, buah_id, MySilatProfile.UserProfile.ProfileId, approvalStatus);
                    await approvalList.GenerateApprovalListAsync();
                    LoadProfileImageUri();

                    PopulateApprovalAdapterSelected();
                    approvalAdapter.DeselectAll();

                    PopulateKelasCodeSpinner();

                    snackbar.Dismiss();

                    Snackbar
                    .Make(lytCoordinator, GetString(Resource.String.saved), Snackbar.LengthShort)
                    .Show();

                    sfBusy.Visibility = ViewStates.Gone;
                    approvalRecycler.Visibility = ViewStates.Visible;

                    CheckRequestDisplay();
                }
                catch (Exception ex)
                {
                    sfBusy.Visibility = ViewStates.Gone;
                    approvalRecycler.Visibility = ViewStates.Visible;

                    Snackbar
                        .Make(lytCoordinator, GetString(Resource.String.saving_error), Snackbar.LengthShort)
                        .Show();

                    Console.WriteLine(GetString(Resource.String.error) + " : " + ex.ToString());
                }
            }
        }

        private void CheckRequestDisplay()
        {
            if (approvalAdapter.ItemCount == 0)
            {
                lytNoContent.Visibility = ViewStates.Visible;
            }
            else
            {
                lytNoContent.Visibility = ViewStates.Gone;
                kelasCodeSpinner.Visibility = ViewStates.Visible;
                RefreshTopCard(0);
            }
        }

        private void PopulateApprovalAdapterSelected()
        {
            approvalAdapter.selected.Clear();

            for (int i = 0; i < approvalAdapter.ItemCount; i++)
            {
                approvalAdapter.selected.Add(false);
            }

            approvalAdapter.NotifyDataSetChanged();
        }

        private void PopulateKelasCodeSpinner()
        {
            kelasCodeAdapter = new ArrayAdapter<string>(this.Context, Resource.Layout.my_spinner, approvalList.kelasCodeList);
            kelasCodeAdapter.SetDropDownViewResource(Resource.Layout.my_spinner_dropdown_item);
            kelasCodeSpinner.Adapter = kelasCodeAdapter;
        }

        private void RefreshTopCard(int selectedCount)
        {
            if (selectedCount > 0)
            {
                txtSubtitle.Text = selectedCount + " daripada " + approvalAdapter.ItemCount + " permohonan dipilih.";
                fabLyt.Visibility = ViewStates.Visible;
            }
            else
            {
                txtSubtitle.Text = approvalAdapter.ItemCount + " permohonan untuk semakan.";
                fabLyt.Visibility = ViewStates.Gone;
            }

            // lytApproval.RefreshDrawableState();
        }

        public void OnApprovalSelectedChange(int count)
        {
            RefreshTopCard(count);
        }

        #region Profile Image / Firebase Storage
        void LoadProfileImageUri()
        {
            if (downloadTaskDict != null)
                downloadTaskDict.Clear();

            if (approvalAdapter.imgUri.Count > 0)
                approvalAdapter.imgUri.Clear();

            for (int index = 0; index < approvalList.Count; index++)
            {
                var item = approvalList[index];

                int imgId = 0;
                int.TryParse(item.ProfileImageId, out imgId);

                int userId = 0;
                int.TryParse(item.ProfileId, out userId);

                if (approvalAdapter.imgUri.ContainsKey(userId))
                    continue;

                // User uploaded image
                if (imgId > 0)
                {
                    if (!approvalAdapter.imgUri.ContainsKey(userId))
                    {
                        approvalAdapter.imgUri.Add(userId, Android.Net.Uri.Empty);
                        GetImageThumbUri(item.ProfileId, item.ProfileImageName, thumbSize);
                    }
                }
                // Facebook
                else if (!string.IsNullOrWhiteSpace(item.FacebookId))
                {
                    if (!approvalAdapter.imgUri.ContainsKey(userId))
                    {
                        Android.Net.Uri uri = MySilatProfile.GetUserProfileImageUri(item.FacebookId, MySilatCommon.IMAGE_NORMAL);
                        approvalAdapter.imgUri.Add(userId, uri);
                    }
                }
            }
        }

        int thumbSize = 128;

        Dictionary<int, Android.Gms.Tasks.Task> downloadTaskDict = new Dictionary<int, Android.Gms.Tasks.Task>();
        List<int> uriUserId = new List<int>();

        void GetImageThumbUri(string userId, string imgName, int thumbDim)
        {
            FirebaseMyStorage.Init();
            StorageReference profileImageRef = FirebaseMyStorage.storageRef.Child("profile/" + userId + "/images/thumb@" + thumbDim + imgName);

            int userIdInt = 0;
            int.TryParse(userId, out userIdInt);

            if (!downloadTaskDict.ContainsKey(userIdInt))
            {
                downloadTaskDict.Add(userIdInt, profileImageRef.GetDownloadUrl()
                            .AddOnSuccessListener(this)
                            .AddOnFailureListener(this));
            }
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                int userId = downloadTaskDict.ElementAt(i).Key;
                Android.Gms.Tasks.Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (!task.IsSuccessful)
                    continue;

                Android.Net.Uri path = (task.Result as Android.Net.Uri);

                approvalAdapter.imgUri[userId] = path;
                approvalAdapter.NotifyDataSetChanged();

                downloadTaskDict[userId] = null;
                break;
            }
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            for (int i = 0; i < downloadTaskDict.Count; i++)
            {
                Android.Gms.Tasks.Task task = downloadTaskDict.ElementAt(i).Value;

                if (task == null)
                    continue;
                if (task.IsSuccessful)
                    continue;

                int key = downloadTaskDict.ElementAt(i).Key;
                downloadTaskDict[key] = null;
            }

            Console.WriteLine(e);
        }
    }
    #endregion
}