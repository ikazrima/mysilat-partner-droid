﻿using System.ComponentModel;

namespace MySilatPartner.Models
{
    class Tauliah : INotifyPropertyChanged
    {
        private string tauliahId;
        private string code;
        private string jawatan;
        private string kelasId;
        private string kelasCode;
        private string kelasName;
        private string kelasType;
        private string year;

        public Tauliah(string tauliahId, string code, string jawatan, string kelasId, string kelasCode, string kelasName, string kelasType, string year)
        {
            this.TauliahId = tauliahId;
            this.Code = code;
            this.Jawatan = jawatan;
            this.KelasId = kelasId;
            this.KelasCode = kelasCode;
            this.KelasName = kelasName;
            this.KelasType = kelasType;
            this.Year = year;
        }

        public string TauliahId
        {
            get { return tauliahId; }

            set
            {
                if (tauliahId != value)
                {
                    tauliahId = value;
                    this.RaisedOnPropertyChanged("TauliahId");
                }
            }
        }

        public string Code
        {
            get { return code; }

            set
            {
                if (code != value)
                {
                    code = value;
                    this.RaisedOnPropertyChanged("Code");
                }
            }
        }

        public string Jawatan
        {
            get { return jawatan; }

            set
            {
                if (jawatan != value)
                {
                    jawatan = value;
                    this.RaisedOnPropertyChanged("Jawatan");
                }
            }
        }

        public string KelasId
        {
            get { return kelasId; }

            set
            {
                if (kelasId != value)
                {
                    kelasId = value;
                    this.RaisedOnPropertyChanged("KelasId");
                }
            }
        }

        public string KelasCode
        {
            get { return kelasCode; }

            set
            {
                if (kelasCode != value)
                {
                    kelasCode = value;
                    this.RaisedOnPropertyChanged("KelasCode");
                }
            }
        }

        public string KelasName
        {
            get { return kelasName; }

            set
            {
                if (kelasName != value)
                {
                    kelasName = value;
                    this.RaisedOnPropertyChanged("KelasName");
                }
            }
        }

        public string KelasType
        {
            get { return kelasType; }

            set
            {
                if (kelasType != value)
                {
                    kelasType = value;
                    this.RaisedOnPropertyChanged("KelasType");
                }
            }
        }

        public string Year
        {
            get { return year; }

            set
            {
                if (year != value)
                {
                    year = value;
                    this.RaisedOnPropertyChanged("Year");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}