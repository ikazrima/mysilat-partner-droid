﻿using System.ComponentModel;


namespace MySilatPartner.Models
{
    class MyImage : INotifyPropertyChanged
    {
        private string id;
        private string ownerId;
        private string ownerType;
        private string imgName;
        private string imgPath;

        public MyImage(string id, string ownerId, string ownerType, string imgName, string imgPath)
        {
            this.Id = id;
            this.OwnerId = ownerId;
            this.OwnerType = ownerType;
            this.ImgName = imgName;
            this.ImgPath = imgPath;

        }

        public string Id
        {
            get { return id; }

            set
            {
                if (id != value)
                {
                    id = value;
                    this.RaisedOnPropertyChanged("Id");
                }
            }
        }

        public string OwnerId
        {
            get { return ownerId; }

            set
            {
                if (ownerId != value)
                {
                    ownerId = value;
                    this.RaisedOnPropertyChanged("OwnerId");
                }
            }
        }

        public string OwnerType
        {
            get { return ownerType; }

            set
            {
                if (ownerType != value)
                {
                    ownerType = value;
                    this.RaisedOnPropertyChanged("OwnerType");
                }
            }
        }

        public string ImgName
        {
            get { return imgName; }

            set
            {
                if (imgName != value)
                {
                    imgName = value;
                    this.RaisedOnPropertyChanged("ImgName");
                }
            }
        }

        public string ImgPath
        {
            get { return ImgPath; }

            set
            {
                if (ImgPath != value)
                {
                    ImgPath = value;
                    this.RaisedOnPropertyChanged("ImgPath");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}