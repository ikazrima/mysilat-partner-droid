﻿using System.ComponentModel;

namespace MySilatPartner.Models
{
    public class UserProfile : INotifyPropertyChanged
    {
        private string profileId;
        private string sex;
        private string firstName;
        private string lastName;
        private string alternateName;
        private int peringkatId;
        private string peringkat;
        private string tauliahId;
        private string jawatanCode;
        private string jawatan;
        private string facebookId;
        private string profileImageId;
        private string profileImageName;
        private string profileImageUri;
        private int year;

        public UserProfile(string profileId, string sex, string firstName, string lastName, string alternateName,
            int peringkatId, string peringkat, string tauliahId, string jawatanCode, string jawatan, string facebookId,
            string profileImageId, string profileImageName, string profileImageUri,
            int year)
        {
            this.ProfileId = profileId;
            this.Sex = sex;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.AlternateName = alternateName;
            this.PeringkatId = peringkatId;
            this.Peringkat = peringkat;
            this.TauliahId = tauliahId;
            this.JawatanCode = jawatanCode;
            this.Jawatan = jawatan;
            this.FacebookId = facebookId;
            this.ProfileImageId = profileImageId;
            this.ProfileImageName = profileImageName;
            this.ProfileImageUri = profileImageUri;
            this.Year = year;
        }

        public string ProfileId
        {
            get { return profileId; }

            set
            {
                if (profileId != value)
                {
                    profileId = value;
                    this.RaisedOnPropertyChanged("ProfileId");
                }
            }
        }

        public string Sex
        {
            get { return sex; }

            set
            {
                if (sex != value)
                {
                    sex = value;
                    this.RaisedOnPropertyChanged("Sex");
                }
            }
        }

        public string FirstName
        {
            get { return firstName; }

            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    this.RaisedOnPropertyChanged("FirstName");
                }
            }
        }

        public string LastName
        {
            get { return lastName; }

            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    this.RaisedOnPropertyChanged("LastName");
                }
            }
        }

        public string AlternateName
        {
            get { return alternateName; }

            set
            {
                if (alternateName != value)
                {
                    alternateName = value;
                    this.RaisedOnPropertyChanged("AlternateName");
                }
            }
        }

        public int PeringkatId
        {
            get { return peringkatId; }

            set
            {
                if (peringkatId != value)
                {
                    peringkatId = value;
                    this.RaisedOnPropertyChanged("PeringkatId");
                }
            }
        }

        public string Peringkat
        {
            get { return peringkat; }

            set
            {
                if (peringkat != value)
                {
                    peringkat = value;
                    this.RaisedOnPropertyChanged("Peringkat");
                }
            }
        }

        public string TauliahId
        {
            get { return tauliahId; }

            set
            {
                if (tauliahId != value)
                {
                    tauliahId = value;
                    this.RaisedOnPropertyChanged("TauliahId");
                }
            }
        }

        public string JawatanCode
        {
            get { return jawatanCode; }

            set
            {
                if (jawatanCode != value)
                {
                    jawatanCode = value;
                    this.RaisedOnPropertyChanged("JawatanCode");
                }
            }
        }

        public string Jawatan
        {
            get { return jawatan; }

            set
            {
                if (jawatan != value)
                {
                    jawatan = value;
                    this.RaisedOnPropertyChanged("Jawatan");
                }
            }
        }

        public string FacebookId
        {
            get { return facebookId; }

            set
            {
                if (facebookId != value)
                {
                    facebookId = value;
                    this.RaisedOnPropertyChanged("FacebookId");
                }
            }
        }

        public string ProfileImageId
        {
            get { return profileImageId; }

            set
            {
                if (profileImageId != value)
                {
                    profileImageId = value;
                    this.RaisedOnPropertyChanged("ProfileImageId");
                }
            }
        }

        public string ProfileImageName
        {
            get { return profileImageName; }

            set
            {
                if (profileImageName != value)
                {
                    profileImageName = value;
                    this.RaisedOnPropertyChanged("ProfileImageName");
                }
            }
        }

        public string ProfileImageUri
        {
            get { return profileImageUri; }

            set
            {
                if (profileImageUri != value)
                {
                    profileImageUri = value;
                    this.RaisedOnPropertyChanged("ProfileImageUri");
                }
            }
        }

        public int Year
        {
            get { return year; }

            set
            {
                if (year != value)
                {
                    year = value;
                    this.RaisedOnPropertyChanged("Year");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}