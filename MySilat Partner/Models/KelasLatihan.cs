﻿using System;

namespace MySilatPartner.Models
{
    public class KelasLatihan
    {
        private string id { get; set; }
        private string kelasId { get; set; }
        private string kelasCode { get; set; }
        private string kelasName { get; set; }
        private string kelasDescription { get; set; }
        private string days { get; set; }
        private DateTime? startTime { get; set; }
        private DateTime? endTime { get; set; }
        private double latitude { get; set; }
        private double longitude { get; set; }

        public string Id
        {
            get { return id; }

            set
            {
                if (id != value)
                {
                    id = value;
                }
            }
        }

        public string KelasId
        {
            get { return kelasId; }

            set
            {
                if (kelasId != value)
                {
                    kelasId = value;
                }
            }
        }

        public string KelasCode
        {
            get { return kelasCode; }

            set
            {
                if (kelasCode != value)
                {
                    kelasCode = value;
                }
            }
        }

        public string KelasName
        {
            get { return kelasName; }

            set
            {
                if (kelasName != value)
                {
                    kelasName = value;
                }
            }
        }

        public string KelasDescription
        {
            get { return kelasDescription; }

            set
            {
                if (kelasDescription != value)
                {
                    kelasDescription = value;
                }
            }
        }

        public string Days
        {
            get { return this.days; }

            set
            {
                if (days != value)
                {
                    days = value;
                }
            }
        }

        public DateTime? StartTime
        {
            get { return startTime; }

            set
            {
                if (startTime != value)
                {
                    startTime = value;
                }
            }
        }

        public DateTime? EndTime
        {
            get { return endTime; }

            set
            {
                if (endTime != value)
                {
                    endTime = value;
                }
            }
        }

        public double Latitude
        {
            get { return latitude; }

            set
            {
                if (latitude != value)
                {
                    latitude = value;
                }
            }
        }

        public double Longitude
        {
            get { return longitude; }

            set
            {
                if (longitude != value)
                {
                    longitude = value;
                }
            }
        }
    }
}
