﻿using System;

namespace MySilatPartner.Models
{
    class TauliahList
    {
        private string id { get; set; }
        private string code { get; set; }
        private string name { get; set; }
        private string type { get; set; }
        private int stateId { get; set; }
        private int zoneId { get; set; }
        private int active { get; set; }
        private DateTime createdDt { get; set; }
        private DateTime modifiedDt { get; set; }

        public string Id
        {
            get { return id; }

            set
            {
                if (id != value)
                {
                    id = value;
                }
            }
        }

        public string Code
        {
            get { return code; }

            set
            {
                if (code != value)
                {
                    code = value;
                }
            }
        }

        public string Name
        {
            get { return name; }

            set
            {
                if (name != value)
                {
                    name = value;
                }
            }
        }

        public string Type
        {
            get { return type; }

            set
            {
                if (type != value)
                {
                    type = value;
                }
            }
        }

        public int StateId
        {
            get { return stateId; }

            set
            {
                if (stateId != value)
                {
                    stateId = value;
                }
            }
        }

        public int ZoneId
        {
            get { return zoneId; }

            set
            {
                if (zoneId != value)
                {
                    zoneId = value;
                }
            }
        }

        public int Active
        {
            get { return active; }

            set
            {
                if (zoneId != value)
                {
                    zoneId = value;
                }
            }
        }

        public DateTime CreatedDt
        {
            get { return createdDt; }

            set
            {
                if (createdDt != value)
                {
                    createdDt = value;
                }
            }
        }

        public DateTime ModifiedDt
        {
            get { return modifiedDt; }

            set
            {
                if (modifiedDt != value)
                {
                    modifiedDt = value;
                }
            }
        }
    }
}