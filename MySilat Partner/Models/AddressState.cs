﻿using System.ComponentModel;

namespace MySilatPartner.Models
{
    class AddressState : INotifyPropertyChanged
    {
        private string id;
        private string state;
        private string abbreviation;

        public AddressState(string id, string state, string abbreviation)
        {
            this.Id = id;
            this.State = state;
            this.Abbreviation = abbreviation;

        }

        public string Id
        {
            get { return id; }

            set
            {
                if (id != value)
                {
                    id = value;
                    this.RaisedOnPropertyChanged("Id");
                }
            }
        }

        public string State
        {
            get { return state; }

            set
            {
                if (state != value)
                {
                    state = value;
                    this.RaisedOnPropertyChanged("State");
                }
            }
        }

        public string Abbreviation
        {
            get { return abbreviation; }

            set
            {
                if (abbreviation != value)
                {
                    abbreviation = value;
                    this.RaisedOnPropertyChanged("Abbreviation");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}