﻿using System;
using System.ComponentModel;

namespace MySilatPartner.Models
{
    public class JurulatihApproval : INotifyPropertyChanged
    {
        private string buahId;
        private string buahName;
        private DateTime? receivedDt;
        private int userBuahId;
        private string profileId;
        private string userAltName;
        private string facebookId;
        private string profileImageId;
        private string profileImageName;
        private string profileImageUri;
        private int peringkatId;
        private string peringkat;
        private int kelasId;
        private string kelasCode;

        public JurulatihApproval(string buahId, string buahName, DateTime? receivedDt,
            int userBuahId, string profileId, string userAltName, string facebookId,
            string profileImageId, string profileImageName, string profileImageUri,
            int peringkatId, string peringkat, int kelasId,
            string kelasCode)
        {
            this.BuahId = buahId;
            this.BuahName = buahName;
            this.ReceivedDt = receivedDt;
            this.UserBuahId = userBuahId;
            this.ProfileId = profileId;
            this.UserAltName = userAltName;
            this.FacebookId = facebookId;
            this.ProfileImageId = profileImageId;
            this.ProfileImageName = profileImageName;
            this.ProfileImageUri = profileImageUri;
            this.PeringkatId = peringkatId;
            this.Peringkat = peringkat;
            this.KelasId = kelasId;
            this.KelasCode = kelasCode;
        }

        public string BuahId
        {
            get { return buahId; }

            set
            {
                if (buahId != value)
                {
                    buahId = value;
                    this.RaisedOnPropertyChanged("BuahId");
                }
            }
        }

        public string BuahName
        {
            get { return buahName; }

            set
            {
                if (buahName != value)
                {
                    buahName = value;
                    this.RaisedOnPropertyChanged("BuahName");
                }
            }
        }

        public DateTime? ReceivedDt
        {
            get { return receivedDt; }

            set
            {
                if (receivedDt != value)
                {
                    receivedDt = value;
                    this.RaisedOnPropertyChanged("ReceivedDt");
                }
            }
        }

        public int UserBuahId
        {
            get { return userBuahId; }

            set
            {
                if (userBuahId != value)
                {
                    userBuahId = value;
                    this.RaisedOnPropertyChanged("UserBuahid");
                }
            }
        }

        public string ProfileId
        {
            get { return profileId; }

            set
            {
                if (profileId != value)
                {
                    profileId = value;
                    this.RaisedOnPropertyChanged("ProfileId");
                }
            }
        }

        public string UserAltName
        {
            get { return userAltName; }

            set
            {
                if (userAltName != value)
                {
                    userAltName = value;
                    this.RaisedOnPropertyChanged("UserAltName");
                }
            }
        }

        public string ProfileImageId
        {
            get { return profileImageId; }

            set
            {
                if (profileImageId != value)
                {
                    profileImageId = value;
                    this.RaisedOnPropertyChanged("ProfileImageId");
                }
            }
        }

        public string ProfileImageName
        {
            get { return profileImageName; }

            set
            {
                if (profileImageName != value)
                {
                    profileImageName = value;
                    this.RaisedOnPropertyChanged("ProfileImageName");
                }
            }
        }

        public string ProfileImageUri
        {
            get { return profileImageUri; }

            set
            {
                if (profileImageUri != value)
                {
                    profileImageUri = value;
                    this.RaisedOnPropertyChanged("ProfileImageUri");
                }
            }
        }

        public string FacebookId
        {
            get { return facebookId; }

            set
            {
                if (facebookId != value)
                {
                    facebookId = value;
                    this.RaisedOnPropertyChanged("FacebookId");
                }
            }
        }

        public int PeringkatId
        {
            get { return peringkatId; }

            set
            {
                if (peringkatId != value)
                {
                    peringkatId = value;
                    this.RaisedOnPropertyChanged("PeringkatId");
                }
            }
        }

        public string Peringkat
        {
            get { return peringkat; }

            set
            {
                if (peringkat != value)
                {
                    peringkat = value;
                    this.RaisedOnPropertyChanged("Peringkat");
                }
            }
        }

        public int KelasId
        {
            get { return kelasId; }

            set
            {
                if (kelasId != value)
                {
                    kelasId = value;
                    this.RaisedOnPropertyChanged("KelasId");
                }
            }
        }

        public string KelasCode
        {
            get { return kelasCode; }

            set
            {
                if (kelasCode != value)
                {
                    kelasCode = value;
                    this.RaisedOnPropertyChanged("KelasCode");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}