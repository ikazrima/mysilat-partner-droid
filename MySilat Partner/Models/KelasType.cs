﻿using System.ComponentModel;

namespace MySilatPartner.Models
{
    class KelasType : INotifyPropertyChanged
    {
        private string id;
        private string type;


        public KelasType(string id, string type)
        {
            this.Id = id;
            this.Type = type;

        }

        public string Id
        {
            get { return id; }

            set
            {
                if (id != value)
                {
                    id = value;
                    this.RaisedOnPropertyChanged("Id");
                }
            }
        }

        public string Type
        {
            get { return type; }

            set
            {
                if (type != value)
                {
                    type = value;
                    this.RaisedOnPropertyChanged("Type");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}