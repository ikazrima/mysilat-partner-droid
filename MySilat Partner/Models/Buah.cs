﻿using System;
using System.ComponentModel;

namespace MySilatPartner.Models
{
    public class Buah : INotifyPropertyChanged
    {
        private string buahId;
        private string buahName;
        private int kelasId;
        private string kelasCode;
        private int jurulatihId;
        private string jurulatihName;
        private bool received;
        private DateTime? receivedDt;
        private int verifiedBy;
        private DateTime verifiedDt;

        public Buah(string buahId, string buahName, int kelasId, string kelasCode, int jurulatihId, string jurulatihName,
            bool received, DateTime? receivedDt, int verifiedBy, DateTime verifiedDt)
        {
            this.BuahId = buahId;
            this.BuahName = buahName;
            this.KelasId = kelasId;
            this.KelasCode = kelasCode;
            this.JurulatihId = jurulatihId;
            this.JurulatihName = jurulatihName;
            this.Received = received;
            this.ReceivedDt = receivedDt;
            this.VerifiedBy = verifiedBy;
            this.VerifiedDt = verifiedDt;
        }

        public string BuahId
        {
            get { return buahId; }

            set
            {
                if (buahId != value)
                {
                    buahId = value;
                    this.RaisedOnPropertyChanged("BuahId");
                }
            }
        }

        public string BuahName
        {
            get { return buahName; }

            set
            {
                if (buahName != value)
                {
                    buahName = value;
                    this.RaisedOnPropertyChanged("BuahName");
                }
            }
        }

        public int KelasId
        {
            get { return kelasId; }

            set
            {
                if (kelasId != value)
                {
                    kelasId = value;
                    this.RaisedOnPropertyChanged("KelasId");
                }
            }
        }

        public string KelasCode
        {
            get { return kelasCode; }

            set
            {
                if (kelasCode != value)
                {
                    kelasCode = value;
                    this.RaisedOnPropertyChanged("KelasCode");
                }
            }
        }

        public int JurulatihId
        {
            get { return jurulatihId; }

            set
            {
                if (jurulatihId != value)
                {
                    jurulatihId = value;
                    this.RaisedOnPropertyChanged("JurulatihId");
                }
            }
        }

        public string JurulatihName
        {
            get { return jurulatihName; }

            set
            {
                if (jurulatihName != value)
                {
                    jurulatihName = value;
                    this.RaisedOnPropertyChanged("JurulatihName");
                }
            }
        }

        public bool Received
        {
            get { return this.received; }

            set
            {
                if (received != value)
                {
                    received = value;
                    this.RaisedOnPropertyChanged("Received");
                }
            }
        }

        public DateTime? ReceivedDt
        {
            get { return receivedDt; }

            set
            {
                if (receivedDt != value)
                {
                    receivedDt = value;
                    this.RaisedOnPropertyChanged("ReceivedDt");
                }
            }
        }

        public int VerifiedBy
        {
            get { return verifiedBy; }

            set
            {
                if (verifiedBy != value)
                {
                    verifiedBy = value;
                    this.RaisedOnPropertyChanged("VerifiedBy");
                }
            }
        }

        public DateTime VerifiedDt
        {
            get { return verifiedDt; }

            set
            {
                if (verifiedDt != value)
                {
                    verifiedDt = value;
                    this.RaisedOnPropertyChanged("VerifiedDt");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisedOnPropertyChanged(string _PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(_PropertyName));
            }
        }
    }
}